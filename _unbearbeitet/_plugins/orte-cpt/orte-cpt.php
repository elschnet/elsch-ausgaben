<?php defined('ABSPATH') or die("No script kiddies please!");
 
// Plugin Name: CPT Orte
// Description: liefert den Custom Post Type "Ort" 
// Author:      Michael Schaefer
// Author URI:  https://elsch.net
// Version:     1.0 (2018-12)
// Text Domain:	elschnet_td
// *******************************


// startwert = am pelz
// wenn aktuelle position nicht ermittelbar
$GLOBALS['start_lat'] 	= '49.8508';
$GLOBALS['start_lon'] 	= '8.6342'; 
$GLOBALS['pos_text']	= 'Am Pelz';

// maps api + aussehen
#$GLOBALS['google_api'] = 'AIzaSyCYGNVFuO4t_yvGZuy7Eyf21HDNm8BWIVE'; // mapelsch
$GLOBALS['google_api'] 	= 'AIzaSyD_KgeKz3pYgwyNGvwG_hZ5kxyuJ0p9LrA'; // Orte ab 241118
$GLOBALS['map_type'] 	= 'roadmap';
$GLOBALS['map_zoom'] 	= 12;
$GLOBALS['map_center'] 	= '{lat:'. $GLOBALS['start_lat'] .',lng:'. $GLOBALS['start_lon'] .'}'; 

// tabelle mit lat lon
$GLOBALS['orte_latlon'] = $wpdb->prefix . "posts_latlon";


 
// **********************************************
//  INHALT
//
//  custom post type: ort
//  orte in "Auf einen Blick" anzeigen
//  custom taxonomy: adr (tags)
//  custom taxonomy: pin (kategorie)
//  custom taxonomy: special (kategorie)
//  custom taxonomy: besuchen (kategorie)
//  umkreissuche
//	luftlinie
//  archiv + suchergebnis sortiert nach kategorie
//  lat_lon in separater tabelle speichern
//	lat_lon wieder speichern (zurück aus dem papierkorb)
//	acf felder auf disable setzen
//
// **********************************************


// **********************************************
//  custom post type: ort
// **********************************************
function elschnet_ort_posttype() {

	$labels = array(
		'name'                  => _x( 'Orte', 'Post Type General Name', 'elschnet_textdomain' ),
		'singular_name'         => _x( 'Ort', 'Post Type Singular Name', 'elschnet_textdomain' ),
		'menu_name'             => __( 'Orte', 'elschnet_textdomain' ),
		'name_admin_bar'        => __( 'Orte', 'elschnet_textdomain' ),
		'archives'              => __( 'Archiv: Orte', 'elschnet_textdomain' ),
		'all_items'             => __( 'Alle Orte', 'elschnet_textdomain' ),
		'add_new_item'          => __( 'Neuen Ort hinzufügen', 'elschnet_textdomain' ),
		'add_new'               => __( 'Erstellen', 'elschnet_textdomain' ),
		'edit_item'             => __( 'Ort bearbeiten', 'elschnet_textdomain' ),
		'search_items'          => __( 'Ort suchen', 'elschnet_textdomain' ),
		'not_found'             => __( 'keine Ort gefunden', 'elschnet_textdomain' ),
		'not_found_in_trash'    => __( 'keine Ort im Papierkorb', 'elschnet_textdomain' ),
		'back_to_items'			=> __( '← Zurück zu Orte', 'elschnet_textdomain' ),
	);
	$args = array(
		'label'                 => __( 'Orte', 'elschnet_textdomain' ),
		'description'           => __( 'Alle Orte', 'elschnet_textdomain' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'thumbnail', 'author', 'comments' ),
		'taxonomies' 			=> array( 'adr', 'pin' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 4,
		'menu_icon'             => 'dashicons-admin-site',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => false,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
		'show_in_rest'          => false,
	);
	register_post_type( 'ort', $args );

}
add_action( 'init', 'elschnet_ort_posttype', 0 );



// **********************************************
//  orte in "Auf einen Blick" anzeigen
// **********************************************
function elschnet_cpt_to_at_a_glance() {
	// Add custom taxonomies and custom post types counts to dashboard
	// https://wordpress.stackexchange.com/a/241665
	$post_types = get_post_types( array( '_builtin' => false ), 'objects' );
	$anzeigen = array('ort'); //welche sollen angezeigt werden
	foreach ( $post_types as $post_type ) {
		if( !in_array( $post_type->name, $anzeigen ) ) { continue; }
		$num_posts = wp_count_posts( $post_type->name );
		$num = number_format_i18n( $num_posts->publish );
		$text = _n( $post_type->labels->singular_name, $post_type->labels->name, $num_posts->publish );
		
		if ( current_user_can( 'edit_posts' ) ) {
			$num = '<li class="post-count"><a href="edit.php?post_type=' . $post_type->name . '">' . $num . ' ' . $text . '</a></li>';
		}
		
		echo $num;
	}
}
add_action( 'dashboard_glance_items', 'elschnet_cpt_to_at_a_glance' );




// **********************************************
//  custom taxonomy: adr (tags)
// **********************************************
function elschnet_adr_taxonomy() {
	// https://generatewp.com/snippet/bqdon3j/

	$labels = array(
		'name'                       => _x( 'Adressen', 'Taxonomy General Name', 'elschnet_td' ),
		'singular_name'              => _x( 'Adresse', 'Taxonomy Singular Name', 'elschnet_td' ),
		'menu_name'                  => __( 'Adresse', 'elschnet_td' ),
		'all_items'                  => __( 'Alle Adressen', 'elschnet_td' ),
		'new_item_name'              => __( 'Adresse', 'elschnet_td' ),
		'add_new_item'               => __( 'Neue Adresse speichern', 'elschnet_td' ),
		'edit_item'                  => __( 'Adresse bearbeiten', 'elschnet_td' ),
		'update_item'                => __( 'Adresse aktualisieren', 'elschnet_td' ),
		'search_items'               => __( 'Adresse suchen', 'elschnet_td' ),
		'not_found'                  => __( 'keine Adresse gefunden', 'elschnet_td' ),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => false,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => false,
		'show_tagcloud'              => true,
		'show_in_rest'               => false,
	);
	register_taxonomy( 'adr', array( 'ort' ), $args );

}
add_action( 'init', 'elschnet_adr_taxonomy', 0 );



// **********************************************
//  custom taxonomy: pin (kategorie)
// **********************************************
function elschnet_pin_taxonomy() {
	https://generatewp.com/snippet/KDJ3Rbn/

	$labels = array(
		'name'                       => _x( 'Kategorien', 'Taxonomy General Name', 'elschnet_td' ),
		'singular_name'              => _x( 'Kategorie', 'Taxonomy Singular Name', 'elschnet_td' ),
		'menu_name'                  => __( 'Kategorien', 'elschnet_td' ),
		'all_items'                  => __( 'Alle Kategorien', 'elschnet_td' ),
		'parent_item'                => __( 'Übergeordnete Kategorie', 'elschnet_td' ),
		'new_item_name'              => __( 'Kategorie', 'elschnet_td' ),
		'add_new_item'               => __( 'Neue Kategorie speichern', 'elschnet_td' ),
		'edit_item'                  => __( 'Kategorie bearbeiten', 'elschnet_td' ),
		'update_item'                => __( 'Kategorie aktualisieren', 'elschnet_td' ),
		'search_items'               => __( 'Kategorie suchen', 'elschnet_td' ),
		'not_found'                  => __( 'keine Kategorie gefunden', 'elschnet_td' ),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => true,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => false,
		'show_tagcloud'              => true,
		'show_in_rest'               => false,
	);
	register_taxonomy( 'pin', array( 'ort' ), $args );

}
add_action( 'init', 'elschnet_pin_taxonomy', 0 );



// **********************************************
//  custom taxonomy: special (kategorie)
// **********************************************
function elschnet_special_taxonomy() {
	// https://generatewp.com/snippet/KDJ3Rbn/

	$labels = array(
		'name'                       => _x( 'Special', 'Taxonomy General Name', 'elschnet_td' ),
		'singular_name'              => _x( 'Special', 'Taxonomy Singular Name', 'elschnet_td' ),
		'menu_name'                  => __( 'Specials', 'elschnet_td' ),
		'all_items'                  => __( 'Alle Specials', 'elschnet_td' ),
		'parent_item'                => __( 'Übergeordnetes Special', 'elschnet_td' ),
		'new_item_name'              => __( 'Special', 'elschnet_td' ),
		'add_new_item'               => __( 'Neues Special speichern', 'elschnet_td' ),
		'edit_item'                  => __( 'Special bearbeiten', 'elschnet_td' ),
		'update_item'                => __( 'Special aktualisieren', 'elschnet_td' ),
		'search_items'               => __( 'Special suchen', 'elschnet_td' ),
		'not_found'                  => __( 'keine Special gefunden', 'elschnet_td' ),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => true,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => false,
		'show_tagcloud'              => false,
		'show_in_rest'               => false,
	);
	register_taxonomy( 'special', array( 'ort' ), $args );

}
add_action( 'init', 'elschnet_special_taxonomy', 0 );



// **********************************************
//  custom taxonomy: besuchen (kategorie)
// **********************************************
function elschnet_besuchen_taxonomy() {

	$labels = array(
		'name'                       => _x( 'besuchen', 'Taxonomy General Name', 'elschnet_td' ),
		'singular_name'              => _x( 'Besuchen', 'Taxonomy Singular Name', 'elschnet_td' ),
		'menu_name'                  => __( 'Besuchen', 'elschnet_td' ),
		'all_items'                  => __( 'Alle Besuchen', 'elschnet_td' ),
		'parent_item'                => __( 'Übergeordnetes Besuchen', 'elschnet_td' ),
		'new_item_name'              => __( 'Besuchen', 'elschnet_td' ),
		'add_new_item'               => __( 'Neues Besuchen anlegen', 'elschnet_td' ),
		'edit_item'                  => __( 'Besuchen bearbeiten', 'elschnet_td' ),
		'update_item'                => __( 'Besuchen aktualisieren', 'elschnet_td' ),
		'search_items'               => __( 'Besuchen suchen', 'elschnet_td' ),
		'not_found'                  => __( 'kein Besuchen gefunden', 'elschnet_td' ),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => true,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => false,
		'show_tagcloud'              => false,
		'show_in_rest'               => false,
	);
	register_taxonomy( 'besuchen', array( 'ort' ), $args );

}
add_action( 'init', 'elschnet_besuchen_taxonomy', 0 );



// **********************************************
//  umkreissuche
// **********************************************
function elschnet_get_nearby( $lat, $lng, $distance ) {
	// liefert die max und min koordinaten für den umkreis einer position
	// https://stackoverflow.com/a/27455891
	// https://stackoverflow.com/a/5045027

	$lat_range = $distance/69.000;  // 111.000 km ist in etwa der radius der erde
	$lon_range = abs($distance/(cos($lat) * 69.000));  
	$minLat = number_format($lat - $lat_range, "6", ".", "");  
	$maxLat = number_format($lat + $lat_range, "6", ".", "");  
	$minLng = number_format($lng - $lon_range, "6", ".", "");  
	$maxLng = number_format($lng + $lon_range, "6", ".", "");  

	$max_min_values = array(
		'start_lat' => $lat,
		'start_lon' => $lng,
		'max_lat' => $maxLat,
		'min_lat' => $minLat,
		'max_lon' => $maxLng,
		'min_lon' => $minLng
	);
	 
	return $max_min_values;
}



// **********************************************
//  luftlinie
// **********************************************
function luftlinie( $lat1, $lon1, $lat2, $lon2 ) {
	
	if ( !is_numeric($lat1) || !is_numeric($lon1) || !is_numeric($lat2) || !is_numeric($lon2) ) {		
		return false; // mindestens ein wert ist keine zahl oder leer
	}
	
	if ( $lat1 == $lat2 && $lon1 == $lon2 ) {		
		return false; // koordinaten sind gleich
	}
	
	$f = 1/298.257223563; //abplattung der erde
	$a = 6378.137; // erdradius am äquator

	$F = ($lat1 + $lat2) / 2;
	$G = ($lat1 - $lat2) / 2;
	$l = ($lon1 - $lon2) / 2;
	
	$sF = pow( sin( deg2rad($F) ),2 );
	$cF = pow( cos( deg2rad($F) ),2 );
	
	$sG = pow( sin( deg2rad($G) ),2 );
	$cG = pow( cos( deg2rad($G) ),2 );
	
	$sl = pow( sin( deg2rad($l) ),2 );
	$cl = pow( cos( deg2rad($l) ),2 );
	
	$S = $sG * $cl + $cF * $sl;
	$C = $cG * $cl + $sF * $sl;
	
	$w = atan( sqrt( $S / $C ) );
	$T = sqrt( $S * $C ) / $w;
	$D = 2 * $w * $a; // grober abstand der punkte

	$H1 = (3 * $T - 1) / (2 * $C);
	$H2 = (3 * $T + 1) / (2 * $S);

	// distanz in kilometern berechnen
	$distanz = $D * (1 + $f * $H1 * $sF * $cG - $f * $H2 * $cF * $sG );
	
	return $distanz;
}


// **********************************************
//  archiv + suchergebnis: alle anzeigen
// **********************************************
function custom_post_type_archive( $query ) {

	if( $query->is_main_query() && !is_admin() && ( is_archive() || is_search() ) ) {
		$query->set( 'posts_per_page', '-1' );
		//$query->set( 'orderby', 'title' );
		//$query->set( 'order', 'ASC' );
	}
}
add_action( 'pre_get_posts', 'custom_post_type_archive' );



// **********************************************
//  lat_lon in separater tabelle speichern
// **********************************************
// @http://biostall.com/performing-a-radial-search-with-wp_query-in-wordpress/
// @https://codex.wordpress.org/Plugin_API/Action_Reference/save_post
function save_lat_lon( $post_id ) {
	global $wpdb;
	
	$slug = 'ort';
	$extern = $GLOBALS['orte_latlon'];
	
	/*
	diese tabelle muss in db verhanden sein
	CREATE TABLE `oelschde_G4Tx_posts_latlon` (
		`post_id` BIGINT(20) UNSIGNED NOT NULL ,
		`lat` FLOAT NOT NULL , 
		`lon` FLOAT NOT NULL ) ENGINE = MyISAM;
	*/
			

	// nur bei meinem post_type arbeiten
	$post_type = get_post_type( $post_id );
    if ( $slug !== $post_type ) {
        return;
    }
	
	// mal sicherheitshalber
    if ( !current_user_can( 'edit_post', $post_id ) ) {
        return;
    }
	
	// prüfen, ob es den eintrag in meiner tabelle schon gibt 
	$check_link = $wpdb->get_row( "SELECT * FROM $extern WHERE post_id = '" . $post_id . "'");
	
	
	// eintrag bereits vorhanden = update
	if ( $check_link != null ) {  
		
		// beitrag wird in den papierkorb gelegt
		if ( isset ($_GET['action']) && $_GET['action'] == 'trash' && is_numeric( $_GET['post'] ) ) {
			$wpdb->delete( $GLOBALS['orte_latlon'], array( 'post_id' => $_GET['post'] ) );		
			// dies können wir ohne schmerzen tun. der beitrag ist ja noch im wp papierkorb
			// sollte er wieder publiziert werden, werden die daten wieder in die externe
			// tabelle geschrieben. siehe function restore_lat_lon()
		}
		
		// externe tabelle wird aktualisiert
		else {
		
			$o_name = get_the_title( $post_id );
			$lat = get_post_meta( $post_id, 'lat', TRUE ); 
			$lon = get_post_meta( $post_id, 'lon', TRUE );
			$pin = get_the_terms( $post_id, 'pin' );
			$pin = $pin[0]->term_id;
			
			$special = get_the_terms( $post_id, 'special' );
			if ( isset($special[0]) ) { $special = $special[0]->term_id; }
			else { $special = 0; }
			
			$besuchen = get_the_terms( $post_id, 'besuchen' );
			if ( isset($besuchen[0]) ) { $besuchen = $besuchen[0]->term_id; }
			else { $besuchen = 0; }
			
			$wpdb->update(
				$extern,
				array(
					'lat' => $lat,
					'lon' => $lon,
					'post_title' => $o_name,
					'pin' => $pin,
					'special' => $special,
					'besuchen' => $besuchen
					
				),   
				array( 'post_id' => $post_id ),   
				array(
					'%s',
					'%s',
					'%s',
					'%d',
					'%d',
					'%d'
				)
			);
		}		
	}  
	
	else  {
		//brauchen wir nicht. neue orte werden über neu.php eingetragen
	}

}  
add_action( 'save_post', 'save_lat_lon', 100 ); 



// **********************************************
//	lat_lon wieder speichern (zurück aus dem papierkorb)
// **********************************************
// https://wordpress.stackexchange.com/q/272937
function restore_lat_lon( $post_id ) {
	global $wpdb;
	
	// nur bei meinem post_type
	if ( get_post_type( $post_id ) == 'ort' ) {

		// daten aus dem wiederhergestellten post holen
		$o_name = get_the_title( $post_id );
		$lat = get_post_meta( $post_id, 'lat', TRUE ); 
		$lon = get_post_meta( $post_id, 'lon', TRUE );
		$pin = get_the_terms( $post_id, 'pin' );
		$pin = $pin[0]->term_id;
		
		$special = get_the_terms( $post_id, 'special' );
		if ( isset($special[0]) ) { $special = $special[0]->term_id; }
		else { $special = 0; }
		
		$besuchen = get_the_terms( $post_id, 'besuchen' );
		if ( isset($besuchen[0]) ) { $besuchen = $besuchen[0]->term_id; }
		else { $besuchen = 0; }
		
		// daten in externe tabelle schreiben
		$wpdb->insert(
			$GLOBALS['orte_latlon'],
			array(
				'post_id' => $post_id,
				'lat' => $lat,
				'lon' => $lon,
				'post_title' => $o_name,
				'pin' => $pin,
				'special' => $special,
				'besuchen' => $besuchen
			),
			array(
				'%d',
				'%s',
				'%s',
				'%s',
				'%d',
				'%d'
			)
		);
	}
}
add_action('untrash_post' , 'restore_lat_lon');




// **********************************************
//	acf felder auf disable setzen
// **********************************************
// @ https://support.advancedcustomfields.com/forums/topic/read-only-field-2/
// @ traviskelleher May 14, 2016 at 6:40 am
function disable_acf_load_field( $field ) {
	
	// bei administratoren bleiben die felder unverändert
	if ( !current_user_can( 'update_core' ) ) {
		$field['disabled'] = 1;
	}	
	
	return $field;
}
add_filter('acf/load_field/name=lat', 'disable_acf_load_field');
add_filter('acf/load_field/name=lon', 'disable_acf_load_field');
add_filter('acf/load_field/name=osm_id', 'disable_acf_load_field');
add_filter('acf/load_field/name=place_id', 'disable_acf_load_field');
add_filter('acf/load_field/name=display_name', 'disable_acf_load_field');


