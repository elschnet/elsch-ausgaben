<?php defined('ABSPATH') or die("No script kiddies please!");
 
// Theme Name:  elsch-ausgaben
// Author:      Michael Schaefer
// Author URI:  https://elsch.net
// Version:     1.0 (2018-12)
// Text Domain:	elschnet_td
// Template Name: wetter
// *******************************


// *******************************
//  xml einlesen 
// *******************************
function elschnet_get_xml_data( $xml, $useragent=FALSE ) {

	if ( !$useragent ) { $useragent = "Mozilla/4.0"; }

	$ch = curl_init($xml);
	curl_setopt ($ch, CURLOPT_URL, $xml);
	curl_setopt ($ch, CURLOPT_HEADER, 0);
	curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt ($ch, CURLOPT_USERAGENT, $useragent);
	$result = curl_exec ($ch);
	curl_close ($ch);
	
	if ($result) { return $result; }
	else { return FALSE; }
}



// *******************************
//  svg icon
// *******************************
$svg_pfeil = ' <svg width="25px" height="25px" viewBox="0 0 512 512" style="vertical-align: sub;"><polygon points="256,382.4 66.8,193.2 109.2,150.8 256,297.6 402.8,150.8 445.2,193.2" style="fill:white"/></svg>';



// *******************************
//  koordinaten übernehmen und prüfen
// *******************************
$koordinaten = $_GET['k'];
list($lat, $lon) = explode(',', $koordinaten);

if ( !is_numeric($lat) || !is_numeric($lon) ) {		
	wp_die('koordinate fehlen'); // mindestens ein wert ist keine zahl oder leer
}


// *******************************
//  xml auslesen
// *******************************
else {
	
	$xmlFile = 'https://api.met.no/weatherapi/locationforecast/1.9/?lat='. $lat .'&lon='. $lon;
	
	#$xmlFile = 'http://www.zals.de/wetter/testdata.xml';	
	#echo '<style>body{background:#333;color:#fff}</style>';

	$xml = simplexml_load_string( elschnet_get_xml_data($xmlFile) );
	
	// leere variablen erstellen
	$lastdate = '';
	$detail = '';


	//  title übernehmen, prüfen ggf. ersatztitle
	$title 	= filter_input( INPUT_GET, "t", FILTER_SANITIZE_STRING );
	if ( empty( $title ) ) {
		$title 	= $lat .', '. $lon;
	}	
	
	// tabelle geht los :)
	echo '<table id="wetter">';
	
	echo '<thead><tr>';
	echo '<th colspan="4">Wetter für '. $title .'</th>';
	echo '</tr></thead>';
	
	$vorhersagetag = 0;
	foreach ($xml->product->time AS $forcast) {
		
		$from = $forcast->attributes()->from; //bsp: [0] => 2018-11-26T07:00:00Z
		$to = $forcast->attributes()->to;
		
		$date = substr($from[0], 0, 10); //bsp: 2018-11-26
		$from = substr($from[0], 11, -7); //bsp: 07
		$to = substr($to[0], 11, -7); 
							
		// datum hübsch machen
		$da = explode('-',$date);
		$tag = date("N", mktime(0, 0, 0, $da[1], $da[2], $da[0]));
		$tage = array( '', 'Mo', 'Di', 'Mi', 'Do', 'Fr', 'Sa', 'So' );
		$datum = date("d.m.", mktime(0, 0, 0, $da[1], $da[2], $da[0]));		

				
		// wir brauchen nur 00-06, 06-12, 12-18, 18-00
		if (( $from=='00' && $to=='06' ) || ( $from=='06' && $to=='12' ) || 
			( $from=='12' && $to=='18' ) || ( $from=='18' && $to=='00' ) ) {
			
			// hilfskonstrukt um letzte zeile immer auszugeben
			$lastrow = false;
			
			// *******************************
			// tabellenzeile starten
			// feststellen, welches der allererste wert ist, die passt
			if ( !$lastdate ) {
				
				// übersichtszeile, allererster(aktueller) tag
				// tabellenzeile öffnen + datum ausgeben
				echo '<tr><td>Heute<br/>'. $datum .'</td>';
			}
			
			// neuer tag = übersichtszeile
			// tabellenzeile öffnen + datum ausgeben
			elseif ( $from=='00' ) { echo '<tr><td>'. $tage[$tag] .'<br/>'. $datum .'</td>'; }	
				
				
			// *******************************
			// inhalte zusammenstellen
			// symbol
			$symbol_nr = floatval( $forcast->location->symbol->attributes()->number ); // bsp: 4
			$symbol_alt = $forcast->location->symbol->attributes()->id; // bsp: Cloud
			$symbol = '<img alt="'. $symbol_alt .'" src="'. get_template_directory_uri() .'/assets/wetter/'. $symbol_nr .'.svg" />';
			
			// niederschlag
			$regen = floatval( $forcast->location->precipitation->attributes()->value ); // bsp: 0.0
			
			// temperatur 
			$min = floatval( $forcast->location->minTemperature->attributes()->value ); // bsp: 4.2
			$max = floatval( $forcast->location->maxTemperature->attributes()->value ); // bsp: 6.8
			
			
		
			// *******************************
			// übersichtszeile erstellen
			// vormittagswerte zwischenspeichern
			if ( $from=='06' && $to=='12' ) {
				$symbol_1   = $symbol;
				$symbol_nr1 = $symbol_nr;
				$regen_1	= $regen;
				$min_1  	= $min;
				$max_1   	= $max;				
			}
			
			// nachmittagswerte mit vormittag zusammenwerfen
			// min/max berechnen und übersicht ausgeben
			if ( $from=='12' && $to=='18' ) {
				
				// bei erstem tag: gibt es vormittagswerte?
				if ( isset($symbol_1) ) {					
					
					// regenmenge zusammenrechnen
					$ueb_regen = $regen_1 + $regen;
					
					// minimaltemperatur bestimmen
					if ( $min_1 < $min ) { $ueb_min = $min_1; } 
					else { $ueb_min = $min; }
					
					// maxmaltemperatur bestimmen
					if ( $max_1 < $max ) { $ueb_max = $max; } 
					else { $ueb_max = $max_1; }
					
					//symbole(e)
					if ( $symbol_nr1 == $symbol_nr ) {
						$ueb_symbol = '<td>'. $symbol;
					} else {
						$ueb_symbol = '<td class="sym">'. $symbol_1 . $symbol;
					}
				}
				
				// keine vormittagswerte beim ersten tag
				else {
					$ueb_regen = $regen;
					$ueb_min = $min;
					$ueb_max = $max;
					$ueb_symbol = '<td>'. $symbol;
				}				
			}
			
			
			// wenn der erste wert 18-00 ist, würde 
			// die erste zeile leer bleiben. dies verhindern
			if ( !$lastdate && $from=='18' && $to=='00' ) {
				$ueb_regen = $regen;
				$ueb_min = $min;
				$ueb_max = $max;
				$ueb_symbol = '<td>'. $symbol;	
			}
			
			
			// *******************************
			// jeweils detailzeile erstellen
			$detail .= '<tr style="display:none" class="detail detail'. $vorhersagetag .'">';
				$detail .= '<td>'. $from .'-'. $to .' Uhr</td>';
				$detail .= '<td>'. $symbol .'</td><td colspan="2">';
				$detail .= number_format( floatval($min), 1, ',', '' ) .'&deg; &mdash; ';
				$detail .= number_format( floatval($max), 1, ',', '' ) .'&deg;<br/>';
				$detail .= number_format( floatval($regen), 1, ',', '' ) .' &ell;/m&sup2;</td>';
			$detail .= '</tr>';
			
			
			
			// *******************************
			// letzter wert des tages:
			// - übersichtszeile ausgeben
			// - detailzeilen ausgaben
			// - alle variablen zurücksetzen
			if ( $from=='18' ) { 
				echo $ueb_symbol .'</td><td>';
				echo number_format( floatval($ueb_min), 1, ',', '' ) .'&deg; &mdash; ';
				echo number_format( floatval($ueb_max), 1, ',', '' ) .'&deg;<br/>';
				echo number_format( $ueb_regen, 1, ',', '' ) .' &ell;/m&sup2;</td>';
				echo '<td><span id="vorhersage'. $vorhersagetag .'">'. $svg_pfeil .'</span></td>';
				echo '</tr>';
				echo $detail;
				echo '<tr style="display:none" class="leer detail detail'. $vorhersagetag .'"><td colspan="4"></td></tr>';
				
				// hilfskonstrukt um letzte zeile immer auszugeben			
				$lastrow = true;
				
				$detail = '';
				$vorhersagetag++;
			}
						
			// datum auch in lastdate speichern
			$lastdate = $date; 
			
		} // if 00-06 oder 06-12 oder 12-18 oder 18-00
	} // foreach xml->time
	
	
	// hilfskonstrukt um letzte zeile immer auszugeben
	if ( !$lastrow ) {	
		echo $ueb_symbol .'</td><td>';
		echo number_format( floatval($ueb_min), 1, ',', '' ) .'&deg; | ';
		echo number_format( floatval($ueb_max), 1, ',', '' ) .'&deg;<br/>';
		echo number_format( $ueb_regen, 1, ',', '' ) .' l/m2</td>';
		echo '<td><span id="vorhersage'. $vorhersagetag .'">'. $svg_pfeil .'</span></td>';
		echo '</tr>';
		echo $detail;
		echo '<tr style="display:none" class="leer detail detail'. $vorhersagetag .'"><td colspan="4"></td></tr>';
	}

	echo '</table>'; 	
} 

// wetterdaten: toggle, auf- und zuklappen
// https://stackoverflow.com/a/31812843	?>
<script>$(document).ready(function() { <?php
	echo '$("[id^=vorhersage]").on("click", function() {';
		echo '$(".detail"+ this.id.match(/\d+$/)[0]).toggle("slow");';
		echo '$(this).toggleClass("open");';
	echo '});'; ?>
});</script>