<?php defined('ABSPATH') or die("No script kiddies please!");
 
// Theme Name:  elsch-ausgaben
// Author:      Michael Schaefer
// Author URI:  https://elsch.net
// Version:     1.0 (2018-12)
// Text Domain:	elschnet_td
// Template Name: datentabelle
// *******************************


// **********************************************
//  ausgabe
// **********************************************
get_header(); ?>
<header class="content-header"><?php

	// offcanvas-menü button ?>
	<label for="offcanvas-menu" class="toggle-btn">
		<svg width="30" height="30" id="hamburger" viewBox="0 0 24 16">
			<line fill="none" stroke="#FFF" stroke-width="2" x1="0" y1="1" x2="24" y2="1" />
			<line fill="none" stroke="#FFF" stroke-width="2" x1="0" y1="8" x2="24" y2="8" />
			<line fill="none" stroke="#FFF" stroke-width="2" x1="0" y1="15" x2="24" y2="15" />
		</svg>
	</label><?php
	
	// headline ?>
	<span class="logo"></span>
	
</header>
<?php



// **********************************************
//  karte
// ********************************************** ?>
<div class="content-body"><?php

$externe = $wpdb->prefix . 'posts_latlon';
$daten = $wpdb->get_results("SELECT * FROM $externe" );

// kategorie-namen 
$o_kat = array();
foreach ( get_terms( 'pin' ) as $pin ) {
	$o_kat[ $pin->term_id ] = $pin->name;
} 

// specials-namen 
$o_spe = array();
foreach ( get_terms( 'special' ) as $special ) {
	$o_spe[ $special->term_id ] = $special->name;
}

// besuch-namen 
$o_bes = array();
foreach ( get_terms( 'besuchen' ) as $besuchen ) {
	$o_bes[ $besuchen->term_id ] = $besuchen->name;
}


if ( $daten ) { ?>

	<table>
		<thead>
			<tr>
				<th>ID</th>
				<th>Breitengrad</th>
				<th>Längengrad</th>
				<th>Name</th>
				<th>Kategorie</th>
				<th>Special / Besuch</th>
				<th>aktion</th>
			</tr>
		</thead>
		<tbody><?php	
			foreach ( $daten as $d ) { 
			
				// eventuell zu langen title kürzen
				$o_name = $d->post_title;
				if ( strlen( $o_name ) > 15 ) { $o_name = substr($o_name, 0, 15) .'&hellip;'; } 
				
				$special_besuch  = '';
				$special_besuch .= $o_spe[ $d->special ];	
				if ( $o_bes[ $d->besuchen ] ) { 
					if ( !empty ($special_besuch) ) { $special_besuch .= ' / '; }
					$special_besuch .= $o_bes[ $d->besuchen ];
				}
				?>
				
				<tr>
					<td><?php echo $d->post_id ?></td>
					<td><?php echo number_format($d->lat, "6", ".", "") ?></td>
					<td><?php echo number_format($d->lon, "6", ".", "") ?></td>
					<td><?php echo $o_name ?></td>
					<td><?php echo $o_kat[ $d->pin ] ?></td>
					<td><?php echo $special_besuch ?></td>
					<td><?php 
						echo '<a href="/?p='. $d->post_id .'">show</a> | ';
						echo '<a href="/wp-admin/post.php?post='. $d->post_id .'&action=edit">edit</a>';
					?></td>
				</tr><?php		
			} ?>
		</tbody>
	</table><?php	
	
} ?>
		
</div><?php



// **********************************************
//  seitenfuß
// ********************************************** ?>
<footer class="content-footer">

	<div class="action left"></div>

</footer>
<?php




// **********************************************
//  css
// **********************************************
function tablestyle() { 

?><style>
td, th { border: 1px solid #999; padding: 0.5rem 30px; }
table { border-spacing: 0.5rem; border-collapse: collapse; }
td { background: hsl(200, 99%, 50%); }
td:nth-child(2),td:nth-child(3) { background: hsl(150, 50%, 50%); }
td:nth-child(1),td:nth-child(7) { background: none; }
th { background:#ebebeb; color:#000; }
thead { font-weight: bold; }
</style><?php

}

add_action( 'wp_footer', 'tablestyle', 100 );



get_footer(); ?> 	