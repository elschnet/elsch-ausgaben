<?php defined('ABSPATH') or die("No script kiddies please!");
 
// Theme Name:  elsch-ausgaben
// Author:      Michael Schaefer
// Author URI:  https://elsch.net
// Version:     1.0 (2018-12)
// Text Domain:	elschnet_td
// Template Name: Luftlinie
// *******************************

// es wird die luftlinie zwischen zwei koordinaten berechnet
// sehr genaue berechnung
// https://de.wikipedia.org/wiki/Orthodrome

$koordinaten = $_GET['k'];
list($lat1, $lon1, $lat2, $lon2) = explode(',', $koordinaten);

$luftlinie = luftlinie( $lat1, $lon1, $lat2, $lon2 );

$out = '';
if ( $luftlinie !== false && $luftlinie > 0.05 ) {	
	$out = 'Entfernung: '. number_format($luftlinie, 2, ',', '.') .' km (Luftlinie)';
}

echo $out; ?> 