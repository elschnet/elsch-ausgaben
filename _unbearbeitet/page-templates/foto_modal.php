<?php defined('ABSPATH') or die("No script kiddies please!");
 
// Theme Name:  elsch-ausgaben
// Author:      Michael Schaefer
// Author URI:  https://elsch.net
// Version:     1.0 (2018-12)
// Text Domain:	elschnet_td
// Template Name: foto_modal
// *******************************

$rows = get_field('fotos', $_GET['id'] );
if($rows) {
	foreach($rows as $row) {
		$image = wp_get_attachment_image_src( $row['abb']['id'], 'medium_large' );
		echo '<img src="'. $image[0] .'" />';
	}
} ?>