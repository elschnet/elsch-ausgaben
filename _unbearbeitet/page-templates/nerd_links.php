<?php defined('ABSPATH') or die("No script kiddies please!");
 
// Theme Name:  elsch-ausgaben
// Author:      Michael Schaefer
// Author URI:  https://elsch.net
// Version:     1.0 (2018-12)
// Text Domain:	elschnet_td
// Template Name: nerd_links
// *******************************



// *******************************
//  koordinaten übernehmen und prüfen
// *******************************
$koordinaten = $_GET['k'];
list($lat, $lon) = explode(',', $koordinaten);

if ( !is_numeric($lat) || !is_numeric($lon) ) {		
	wp_die('koordinate fehlen'); // mindestens ein wert ist keine zahl oder leer
} else {
	$gps = "$lat,$lon";	
	$geohack = fraction_to_min_sec($lat, 'lat', true ) .'_'. fraction_to_min_sec($lon, 'lon', true );
	$geops = "lat=$lat&lon=$lon";
}




?>
<div id="photolinks" class="modal">
	
	<h2>Fotomotive</h2>
	<section>	
		<button class="einzeln"><a target="_blank" href="https://www.flickr.com/nearby/<?php echo $gps ?>?by=everyone&taken=alltime&sort=date&page=1&show=detail">Flickr Places</a></button>
		<button class="einzeln"><a target="_blank" href="https://tools.wmflabs.org/geocommons/proximityrama?latlon=<?php echo $gps ?>">Wikipedia Fotos</a></button>
		<button class="einzeln"><a target="_blank" href="https://virtualglobetrotting.com/ll/<?php echo $gps ?>">virtual Globetrotting</a></button>
	</section>
	
	<h2>Nerd-Content</h2>
	<section>
		<button class="einzeln"><a target="_blank" href="http://tracker.geops.ch/?z=14&s=20&<?php echo $geops ?>">Weltweiter ÖNV-Tracker</a></button>
		<button class="einzeln"><a target="_blank" href="https://tools.wmflabs.org/geohack/geohack.php?params=<?php echo $geohack ?>">GeoHack</a></button>
		<button class="einzeln"><a target="_blank" href="http://geohash.org/?format=style&q=<?php echo $gps ?>">GeoHash</a></button>
		<button class="einzeln"><a target="_blank" href="https://w3w.co/<?php echo $gps ?>">What3Words</a></button>
	</section>
	
</div>

