<?php defined('ABSPATH') or die("No script kiddies please!");
 
// Theme Name:  elsch-ausgaben
// Author:      Michael Schaefer
// Author URI:  https://elsch.net
// Version:     1.0 (2018-12)
// Text Domain:	elschnet_td
// Template Name: neu
// *******************************



/*
// **********************************************
	Ablauf: 
	
1)	Es werden Koordinaten beim Aufruf übergeben (action=step1).
	Aufgrund der Koordinaten wir die API von Open Street Map befragt und
	Daten zur Adresse übernommen. 
	
2)	Diese Daten werden ein neuer Beitrag im CPT Orte als Entwurf gespeichert. 
	Ein Teil der Daten werden ebenfalls in der externen Tabelle gespeichert.
	
3)	Der Benutzer kommt die Daten angezeigt und wird gleichzeitig nach 
	dem Titel des Ortes und einer Kategorie befragt. 
	
4a)	Wenn Titel und Kategorie angegeben werden (action=step2), erfolgt die 
	Aktualisierung des Beitrags und der externen Tabelle. Der Beitrag wird 
	veröffentlicht.
5a)	Es wird eine Bestätigungsseite angezeigt mit den Links: weitere Details
	erfassen, Karte anzeigen (Startwert = neuer Ort), Ort anzeigen
	
4b)	Oder der Benutzer klickt auf Abbrechen (action=step0). Dann wird der 
	Entwurf sowie die Daten in der externen Tabelle wieder gelöscht.	
5b)	Es wird die Startseite angezeigt (Startwert = dieser nicht gespeicherte Ort)

// **********************************************
*/

// **********************************************
//  Quellen 
// ********************************************** 
// in externer tabelle abspeichern
// https://codex.wordpress.org/Class_Reference/wpdb

// **********************************************
//  postwerte übernehmen
// **********************************************
if ( $_POST["action"] == 'step1' || $_POST["action"] == 'step2' || $_POST["action"] == 'step0' ) {

	$lat 		= filter_input( INPUT_POST, "new_lat", FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION );
	$lon 		= filter_input( INPUT_POST, "new_lon", FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION );
	$action 	= filter_input( INPUT_POST, "action", FILTER_SANITIZE_STRING );
	$o_post_id 	= filter_input( INPUT_POST, "o_post_id", FILTER_SANITIZE_NUMBER_FLOAT );
	$o_cat 		= filter_input( INPUT_POST, "o_cat", FILTER_VALIDATE_INT );
	$o_name 	= filter_input( INPUT_POST, "o_name", FILTER_SANITIZE_STRING );
	$o_special 	= filter_input( INPUT_POST, "o_special", FILTER_VALIDATE_INT );
	$o_besuch 	= filter_input( INPUT_POST, "o_besuch", FILTER_VALIDATE_INT );
	$o_beschreibung = '';
	if ( isset($_POST['o_beschreibung']) ) { $o_beschreibung = sanitize_text_field( $_POST['o_beschreibung'] ); }
	
	$msg = array();	
}



// **********************************************
//  keine daten
// **********************************************
if ( $action == 'step1' && ( empty($lat) || empty ($lon) ) ||
	 $action == 'step2' && ( empty($o_post_id) || empty ($o_cat) || empty ($o_name) ) ||
	 $action == 'step0' && ( empty($o_post_id) ) ||
	 empty( $action ) ) {
	
	$msg = array();
	$msg['type']  = 'error';
	$msg['head']  = 'Ups!';
	$msg['text']  = '<p>Diese Seite kann nicht direkt aufgerufen werden.</p>';
	$msg['text'] .= '<p>Es wurden keine Koordinaten übergeben.</p>';
	$msg['text'] .= '<p><button><a href="/">zur Startseite</a></button></p>';	
}	

	


// **********************************************
//  [1] osm werte für koordinaten holen
// **********************************************
if ( $action=='step1' ) {
	if ( !wp_verify_nonce( $_POST['_ononce'], 'neuer_ort1' ) ) { wp_die( 'Failed security check 1' ); } 
		
	// adressdaten abfragen (osm api abfragen)
	$file = 'https://nominatim.openstreetmap.org/reverse?accept-language=de&format=json&lat='. $lat .'&lon='. $lon .'&zoom=18&addressdetails=1';

	$ch = curl_init($file); 
	curl_setopt($ch, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']); 
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_ENCODING,'gzip');
	curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5); 
	curl_setopt($ch, CURLOPT_FAILONERROR, 1); 
	$osm_data=@curl_exec($ch);	

		
	// **********************************************
	// fehler "keine daten"
	if ( !isset( $osm_data ) || empty( $osm_data ) ) {
		// es wurde $lat und $lon übergeben, aber die osm api hat keine adresse geliefert
		
		// get variable erzeugen		
		$get_var = array(
			'lat'	=> $lat,
			'lon'	=> $lon
		);
		
		$msg['type']  = 'error';
		$msg['head']  = 'Koordinaten nicht gefunden';
		$msg['text']  = '<p>Der Ort kann nicht gespeichert werden.</p>';
		$msg['text'] .= '<p>Open Street Maps liefert keine Adressdaten für diese Koordinaten:<br/>';
		$msg['text'] .= '<code>lat: '. $lat .'<br/>lon: '. $lon .'<br/>evaluieren: <a href="'. $file .'">JSON</a></code></p>';
		$msg['text'] .= '<p><button><a href="/?'. http_build_query( $get_var ) .'">zur Startseite</a></button></p>';

	}	


	// **********************************************
	// es gibt daten = prüfen
	if ( !empty( $osm_data ) ) {
		$obj = json_decode($osm_data);
				
		// **********************************************
		// koordinaten prüfen
		$vorhanden = $wpdb->get_results( 'SELECT * FROM '. $GLOBALS['orte_latlon'] .' 
			WHERE lat="'. $obj->{'lat'} .'" AND lon="'. $obj->{'lon'} .'"
			LIMIT 1' );			
	}
	
	
	// **********************************************
	// fehler "bereits vorhanden"
	// zwei gleiche orte wären nie anklickbar
	if ( !empty( $vorhanden ) ) {
			
		$msg['type']  = 'error';
		$msg['head']  = 'Bereits bekannter Ort';
		$msg['text']  = '<p>Diese Koordinaten sind bereits eingetragen.</p>';
		$msg['text'] .= '<p>Bitte diesen Ort gegebenenfalls anpassen:<br/><a href="/?p='. $vorhanden[0]->post_id .'">'. $vorhanden[0]->post_title .'</a></p>';
		$msg['text'] .= '<p><button><a href="/">zur Startseite</a></button></p>';
		//$msg['text'] .= '<pre>'. print_r($vorhanden, true) .'</pre>';
		
	}
	

	// **********************************************
	//  [2] daten verarbeiten
	// **********************************************
	if ( !empty( $osm_data ) && empty( $vorhanden ) ) {	
		$adr = $obj->{'address'};
		$ort_tags = array();
		
		// bisherigen werte speichern
		$pin_lat = $lat;
		$pin_lon = $lon;
		
		// osm-werte benutzen
		// u.a. wichtig für zweite tabelle
		$lat = $obj->{'lat'};
		$lon = $obj->{'lon'};
		
		// stadt ermitteln
		$city = '';
		if ( !empty( $adr->{'city'} ) ) { $city = $adr->{'city'}; } 
		elseif ( !empty( $adr->{'town'} ) ) { $city = $adr->{'town'}; } 
		elseif ( !empty( $adr->{'county'} ) ) { $city = $adr->{'county'}; }
		if ( !empty( $city ) ) { $ort_tags[] = $city; }
		
		// bundesland ermitteln
		$bundesland = '';
		// bei Birmingham liefert state "England", state_district aber "West Midlands"
		// bei Frankfurt liefert state "Hessen", state_district aber "Regierungsbezirk Darmstadt"
		// Regel: bei "de" nehmen wir state, sonst state_district als Bundesland
		// Regel: als tag werden beide gespeichert	
		if ( !empty( $adr->{'state'} ) ) { 
			$bundesland = $adr->{'state'}; 
			$ort_tags[] = $bundesland;
		} 
		if ( !empty( $adr->{'state_district'} ) && $adr->{'country_code'} != 'de' ) { 
			$bundesland = $adr->{'state_district'};
			$ort_tags[] = $bundesland;
		} 

		// land ermitteln
		$land = '';
		if ( !empty( $adr->{'country'} ) ) { 
			$land = $adr->{'country'};
			$ort_tags[] = $land;
		}
		
		// eventuelle doppelte werte entfernen
		$ort_tags = array_unique( $ort_tags );
		$ort_tags_commaseperated = implode(', ', $ort_tags);

		
		// vorläufige werte		 
		$o_name = wp_strip_all_tags( 'OSM '. $obj->{'osm_id'} ); //title
		$pin = 130; //pin(kategorie) = "none" = id 130

		
		
		// **********************************************
		// raw daten
		/*
		$admin  = 'display_name: '. $obj->{'display_name'} .'<br/>';
		$admin .= '<br/>';
		$admin .= 'Bundesland: '. $bundesland .'<br/>';
		$admin .= 'Stadt: '. $city .'<br/>';
		$admin .= 'PLZ: '. $adr->{'postcode'} .'<br/>';
		$admin .= 'Land: '. $land .'<br/>';
		$admin .= 'Code: '. $adr->{'country_code'} .'<br/>';
		$admin .= '<br/>';
		$admin .= 'place_id: '. $obj->{'place_id'} .'<br/>';
		$admin .= 'osm_id: '. $obj->{'osm_id'} .'<br/>';
		$admin .= '<br/>';
		$admin .= 'osm_lat: '. $lat .'<br/>';//
		$admin .= 'osm_lon: '. $lon .'<br/>';//
		$admin .= 'lat: '. $pin_lat .'<br/>';
		$admin .= 'lon: '. $pin_lon .'<br/>';
		$admin .= '<br/>';
		$admin .= '<a href="https://www.google.de/maps/?q='. $lat .','. $lon .'">google maps test</a><br/>';
		$admin .= '<a href="https://nominatim.openstreetmap.org/reverse?format=xml&lat='. $lat .'&lon='. $lon .'&zoom=18&addressdetails=1">nominatim xml</a><br/>';
		$admin .= '<a href="https://nominatim.openstreetmap.org/reverse?format=json&lat='. $lat .'&lon='. $lon .'&zoom=18&addressdetails=1">nominatim json</a><br/>';
		*/
		
		
		// **********************************************
		// ort speichern als entwurf
		// @https://wordpress.stackexchange.com/a/8600		
		$new_orte_post = array(
			'post_title' => $o_name,
			'post_status' => 'draft',
			'post_type' => 'ort'
		);
		$o_post_id = wp_insert_post($new_orte_post);

		
		// **********************************************
		// metadaten zum post hinzufügen
		add_post_meta( $o_post_id, 'lat', $lat, true );
		add_post_meta( $o_post_id, 'lon', $lon, true );
		add_post_meta( $o_post_id, 'display_name', $obj->{'display_name'}, true );
		add_post_meta( $o_post_id, 'osm_id', $obj->{'osm_id'}, true );
		add_post_meta( $o_post_id, 'place_id', $obj->{'place_id'}, true );
		add_post_meta( $o_post_id, 'stadt', $city, true );
		add_post_meta( $o_post_id, 'bundesland', $bundesland, true );
		add_post_meta( $o_post_id, 'land', $land, true );
		add_post_meta( $o_post_id, 'plz', $adr->{'postcode'}, true );
		add_post_meta( $o_post_id, 'code', $adr->{'country_code'}, true );
		
		// **********************************************
		// adressen auch als tags speichern
		wp_set_object_terms( $o_post_id, $ort_tags, 'adr' );
		
		// **********************************************
		// vorläufige kategorie speichern
		wp_set_object_terms( $o_post_id, $pin, 'pin' );
			
		
		// **********************************************
		// daten in externer tabelle speichern
		$wpdb->insert(
			$GLOBALS['orte_latlon'],
			array(
				'post_id' => $o_post_id,
				'lat' => $lat,
				'lon' => $lon,
				'post_title' => $o_name,
				'pin' => $pin
			),
			array(
				'%d',
				'%s',
				'%s',
				'%s',
				'%d'
			)
		);
		
		
		// **********************************************
		// pins (kategorien) holen
		$pins = get_terms( array(
			'taxonomy' => 'pin',
			'exclude' => array( 112, 66 ), // 'home'+'none'
			'hide_empty' => false,
		) );
		
		$options_pin = '<option value="">Kategorie auswählen*</option>';
		foreach( $pins as $pin ) {
			$options_pin .= '<option value="'. $pin->term_id .'">'. $pin->name .'</option>';
		} 
		
		
		// **********************************************
		// specials holen
		$specials = get_terms( array(
			'taxonomy' => 'special',
			'hide_empty' => false,
		) );
		
		$options_special = '<option value="">Special auswählen</option>';
		foreach( $specials as $special ) {
			$options_special .= '<option value="'. $special->term_id .'">'. $special->name .'</option>';
		} 
		
		
		// **********************************************
		// besuch holen
		$besuche = get_terms( array(
			'taxonomy' => 'besuchen',
			'hide_empty' => false,
		) );
		
		$options_besuch = '<option value="">Besuch auswählen</option>';
		foreach( $besuche as $besuch ) {
			$options_besuch .= '<option value="'. $besuch->term_id .'">'. $besuch->name .'</option>';
		} 
	
			
		// **********************************************
		//  [3] formular anzeigen
		// **********************************************
		$msg['type']  = '';
		$msg['head']  = 'Neuen Ort eintragen';		
		$msg['text']  = '<form action="'. $_SERVER['REQUEST_URI'] .'" method="post" id="neu" class="no_label">';			
			$msg['text'] .= "<label for='o_name'>Name:</label> <input type='text' name='o_name' value='' placeholder='Bezeichnung dieses Ortes*' required><br/>";
			$msg['text'] .= "<label for='o_cat'>Icon:</label> <span class='elschselect'><select name='o_cat' required>$options_pin</select></span><br/>";
			$msg['text'] .= '<p>Diese Adressdaten werden u.a. gespeichert:<br/>'. $ort_tags_commaseperated .'</p>';
			$msg['text'] .= '<p>Optional:</p>';
			$msg['text'] .= '<textarea placeholder="Kurzbeschreibung (max. 200 Zeichen)" rows="8" cols="50" name="o_beschreibung"></textarea>';
			$msg['text'] .= "<label for='o_cat'>Special:</label> <span class='elschselect optional'><select name='o_special'>$options_special</select></span><br/>";
			$msg['text'] .= "<label for='o_cat'>Besuch:</label> <span class='elschselect optional'><select name='o_besuch'>$options_besuch</select></span><br/>";
			
			$msg['text'] .= '<input type="hidden" name="o_post_id" value="'. $o_post_id .'">';
			$msg['text'] .=  wp_nonce_field('neuer_ort2', '_ononce', TRUE, FALSE );
			$msg['text'] .= '<input type="hidden" name="action" value="step2">';
			$msg['text'] .= '<button type="submit">Ort speichern</button>';
		$msg['text'] .= '</form>';
		
		$msg['text'] .= '<form action="'. $_SERVER['REQUEST_URI'] .'" id="stop" method="post">';
			$msg['text'] .= '<input type="hidden" name="o_post_id" value="'. $o_post_id .'">';
			$msg['text'] .=  wp_nonce_field('neuer_ort0', '_ononce', TRUE, FALSE );
			$msg['text'] .= '<input type="hidden" name="action" value="step0">';
			$msg['text'] .= '<button type="submit" onclick="return confirm(\'Das Speichern dieses Ortes wirklich abbrechen?\')">abbrechen</button>';
		$msg['text'] .= '</form>';
	}
} 



// **********************************************
//  [4a] titel+kategorie speichern
// **********************************************
if ( $action=='step2' ) {
	if ( !wp_verify_nonce( $_POST['_ononce'], 'neuer_ort2' ) ) { wp_die( 'Failed security check 2' ); } 

	// **********************************************
	// prüfen, ob der post_status 'draft' ist
	if ( get_post_status( $o_post_id ) != 'draft' ) {
	
		$msg['type']  = 'error';
		$msg['head']  = 'Nope.';
		$msg['text']  = "<p>Dieser Ort kann nicht veröffentlicht werden.</p>";
		$msg['text'] .= "<p>Der Status wurde zwischenzeitlich geändert.</p>";
		$msg['text'] .= "<p><code>evaluieren: <a href='/?p=". $o_post_id ."'>[". $o_post_id ."]</a></code></p>";
		$msg['text'] .= '<p><button><a href="/">zur Startseite</a></button></p>';
	
	} 
	
	else {
	
		// **********************************************
		// daten in wp abspeichern
		// https://codex.wordpress.org/Function_Reference/wp_update_post	
		wp_update_post(array(
			'ID'			=> $o_post_id,
			'post_title'	=> $o_name,
			'post_status'	=> 'publish'
		));	
		/*
		$msg['text'] .= "title: $o_name<br/>"; 
		$msg['text'] .= "kurzbeschreibung: $o_beschreibung<br/>"; 
		$msg['text'] .= "pin: $o_cat<br/>"; 
		$msg['text'] .= "special: $o_special<br/>"; 
		$msg['text'] .= "besuchen: $o_besuch<br/>";
		*/
		// kurzbeschreibung
		update_post_meta( $o_post_id, 'kurzbeschreibung', $o_beschreibung ); 
		
		// kategorie speichern
		wp_set_object_terms( $o_post_id, $o_cat, 'pin' );
		
		// special speichern
		wp_set_object_terms( $o_post_id, $o_special, 'special' );
		
		// besuch speichern
		wp_set_object_terms( $o_post_id, $o_besuch, 'besuchen' );

	
		// in externer tabelle abspeichern
		// https://codex.wordpress.org/Class_Reference/wpdb
		$wpdb->update( 
			$GLOBALS['orte_latlon'], 
			array( 
				'post_title' => $o_name,
				'pin' => $o_cat,
				'special' => $o_special,
				'besuchen' => $o_besuch
			), 
			array( 'post_id' => $o_post_id ), 
			array(  '%s', '%d', '%d', '%d' ), 
			array( '%d' ) 
		);
		
		
		// **********************************************
		//  [5a] bestätigungsseite ausgeben
		// **********************************************
		$start_lat = get_post_meta( $o_post_id, 'lat', TRUE );
		$start_lon = get_post_meta( $o_post_id, 'lon', TRUE );
		
		$msg['type']  = 'success';
		$msg['head']  = 'Ort gespeichert!';
		$msg['text']  = "<button class='einzeln'><a href='/wp-admin/post.php?post=". $o_post_id ."&action=edit'>Infos hinzufügen (Backend)</a></button>";
		$msg['text'] .= "<button class='einzeln'><a href='/?p=". $o_post_id ."'>Ort anzeigen (Single-Seite)</a></button>";		
		$msg['text'] .= '<form action="/" method="post">';
			$msg['text'] .= '<input type="hidden" id="show_lat" name="show_lat" value="'. $start_lat .'">';
			$msg['text'] .= '<input type="hidden" id="show_lon" name="show_lon" value="'. $start_lon .'">';
			$msg['text'] .=  wp_nonce_field('orte_zeigen', '_ononce', TRUE, FALSE );
			$msg['text'] .= '<button type="submit" id="show" class="einzeln">Startseite</button>';
		$msg['text'] .= '</form>';
	}
}



// **********************************************
//  [4b] abbruch
// **********************************************
if ( $action=='step0' ) {
	if ( !wp_verify_nonce( $_POST['_ononce'], 'neuer_ort0' ) ) { wp_die( 'Failed security check 0' ); } 

	
	// **********************************************
	// prüfen, ob der post_status 'draft' ist
	if ( get_post_status( $o_post_id ) != 'draft' ) {
	
		$msg['type']  = 'error';
		$msg['head']  = 'Nope.';
		$msg['text']  = "<p>Irgendetwas ist faul.</p>";
		$msg['text'] .= "<p>Der Status dieses Ortes wurde zwischenzeitlich geändert.</p>";
		$msg['text'] .= "<p><code>evaluieren: <a href='/?p=". $o_post_id ."'>[". $o_post_id ."]</a></code></p>";
		$msg['text'] .= '<p><button><a href="/">zur Startseite</a></button></p>';
	
	} else {
	
		// koordinaten aufheben
		$start_lat = get_post_meta( $o_post_id, 'lat', TRUE );
		$start_lon = get_post_meta( $o_post_id, 'lon', TRUE );
	
		// löschen = post_status auf 'papierkorb' ändern
		wp_update_post(array(
			'ID'			=> $o_post_id,
			'post_status'	=> 'trash'
		));	
		
		// werte in externen tabelle löschen
		$wpdb->delete( $GLOBALS['orte_latlon'], array( 'post_id' => $o_post_id ) );		
		// dies können wir ohne schmerzen tun. der beitrag ist ja noch im wp papierkorb
		// sollte er wieder publiziert werden, werden die daten automatisch in die externe
		// tabelle gespiegelt
		
		
		// **********************************************
		//  [5b] zurück zur startseite 
		// **********************************************
		// startseite mit aktueller (nicht gespeicherter) position anzeigen
		// hier werden die koordinaten als get übergeben. u.a. weil auf der startseite
		// nicht automatisch orte für diese koordinate angezeigt werden sollen.
		// bewusst keine weitergabe mit header(location), meldung soll erscheinen.
		
		// get variable erzeugen		
		$get_var = array(
			'lat'	=> $start_lat,
			'lon'	=> $start_lon
		);
		
		$msg['type']  = 'notice';
		$msg['head']  = 'Ort nicht gespeichert';
		$msg['text']  = '<p>Dieser Ort wurde nicht gespeichert. Die zwischenzeitlich gespeicherten Informationen wurden wieder gelöscht.</p>';
		$msg['text'] .= '<br/><button><a href="/?'. http_build_query( $get_var ) .'">zur Startseite</a></button>';	
	}	
}
	
	
	
// **********************************************
//  ausgabe
// **********************************************
get_header(); ?>

<header class="content-header"><?php

	// offcanvas-menü button ?>
	<label for="offcanvas-menu" class="toggle-btn">
		<svg width="30" height="30" id="hamburger" viewBox="0 0 24 16">
			<line fill="none" stroke="#FFF" stroke-width="2" x1="0" y1="1" x2="24" y2="1" />
			<line fill="none" stroke="#FFF" stroke-width="2" x1="0" y1="8" x2="24" y2="8" />
			<line fill="none" stroke="#FFF" stroke-width="2" x1="0" y1="15" x2="24" y2="15" />
		</svg>
	</label><?php
	
	// headline ?>
	<span class="logo"></span>
	
</header><?php



// **********************************************
//  karte
// ********************************************** ?>
<div class="content-body">
	
	<article id="neuerort" class="box <?php echo $msg['type'] ?>">
		<h1><?php echo $msg['head'] ?></h1>
		<?php echo $msg['text'] ?>
	</article>
		
</div><?php



// **********************************************
//  seitenfuß
// ********************************************** ?>
<footer class="content-footer">
	<div class="action left"></div>
</footer><?php


// erneutes absenden des formulars verhindern ?>
<script>if ( window.history.replaceState ) { window.history.replaceState( null, null, window.location.href ); }</script>


<?php get_footer(); ?> 	