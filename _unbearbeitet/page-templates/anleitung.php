<?php defined('ABSPATH') or die("No script kiddies please!");
 
// Theme Name:  elsch-ausgaben
// Author:      Michael Schaefer
// Author URI:  https://elsch.net
// Version:     1.0 (2018-12)
// Text Domain:	elschnet_td
// Template Name: Anleitung
// *******************************

get_header(); 


// **********************************************
//  menü
// ********************************************** ?>
<header class="content-header"><?php

	// offcanvas-menü button ?>
	<label for="offcanvas-menu" class="toggle-btn">
		<svg width="30" height="30" id="hamburger" viewBox="0 0 24 16">
			<line fill="none" stroke="#FFF" stroke-width="2" x1="0" y1="1" x2="24" y2="1" />
			<line fill="none" stroke="#FFF" stroke-width="2" x1="0" y1="8" x2="24" y2="8" />
			<line fill="none" stroke="#FFF" stroke-width="2" x1="0" y1="15" x2="24" y2="15" />
		</svg>
	</label><?php
	
	// headline ?>
	<span class="logo"></span>
	
</header><?php





// **********************************************
//  inhalt
// ********************************************** ?>
<div class="content-body"><?php 
	if ( have_posts() ) {
		while ( have_posts() ) {
			the_post(); 
			
			
			// **********************************************
			// inhalte aus wordpress ?>
			<article class="box">
				<h2><?php the_title(); ?></h2>
				<?php the_content(); ?>
			</article><?php
			
			
			// **********************************************
			// tag cloud ?>
			<article class="box">
				<h2>Tag Cloud</h2>
				<div id="tagcloud"><?php
					
					$args = array(
						'smallest'	=> 14, 
						'largest'	=> 50,
						'unit'		=> 'px', 
						'number'	=> 0, 
						'taxonomy'	=> 'adr'
					); 
					
					wp_tag_cloud( $args ); ?>
					
				</div>
			</article><?php
			
			
			
			// **********************************************
			// alle kategorie-icons
			
			// svg datei einbinden
			include get_template_directory() .'/assets/mapmarker.php';
			
			// alle kategorien holen und durchlaufen
			$terms = get_terms( array(
				'taxonomy' => 'pin',
				'hide_empty' => false,
			) ); ?>	
									
			<article class="box" id="all_icons">	
				<h2>Alle Kategorie-Icons</h2><?php
				
				foreach ( $terms as $term ) {
					
					$pin = $term->term_id;
					$name = $term->name;
					$slug = $term->slug;
					
					// ersatzgrafik, wenn es keine svg für diese kategorie gibt
					if ( !array_key_exists( $pin, $svg ) ) { 
						$pin = 0; 
						$name .= ' (Ersatzgrafik!)';
					}

					$svg_path = $svg[ $pin ]['path'];
					$svg_view = $svg[ $pin ]['view'];
					$svg_css  = $svg[ $pin ]['css'];
					
					if ( isset($svg_css) ) { $css = ' style="'. $svg_css .'"'; }
					else { $css = ''; }

					echo '<div class="all_icons">';
						echo '<a href="/pin/'. $slug .'">';
							echo '<svg'. $css .' viewBox="'. $svg_view .'"><title>'. $name .'</title><path d="'. $svg_path .'"></path></svg><br/>';
							echo '<small>'. $name .'</small>';
						echo '</a>';
					echo '</div>';
					
				} ?>
				
			</article><?php			
		} 
	} ?>	
</div>
<?php



// **********************************************
//  seitenfuß
// ********************************************** ?>
<footer class="content-footer"></footer><?php 



get_footer(); ?> 