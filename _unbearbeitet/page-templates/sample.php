<?php defined('ABSPATH') or die("No script kiddies please!");
 
// Theme Name:  elsch-ausgaben
// Author:      Michael Schaefer
// Author URI:  https://elsch.net
// Version:     1.0 (2018-12)
// Text Domain:	elschnet_td
// Template Name: sample
// *******************************


// https://code.google.com/archive/p/geo-location-javascript/source/default/source


get_header(); 


// **********************************************
//  menü
// ********************************************** ?>
<header class="content-header"><?php

	// offcanvas-menü button ?>
	<label for="offcanvas-menu" class="toggle-btn">
		<svg width="30" height="30" id="hamburger" viewBox="0 0 24 16">
			<line fill="none" stroke="#FFF" stroke-width="2" x1="0" y1="1" x2="24" y2="1" />
			<line fill="none" stroke="#FFF" stroke-width="2" x1="0" y1="8" x2="24" y2="8" />
			<line fill="none" stroke="#FFF" stroke-width="2" x1="0" y1="15" x2="24" y2="15" />
		</svg>
	</label><?php
	
	// headline ?>
	<span class="logo"></span>
	
</header>


<div id="showInfo" class="box">

<a href="dffdhbadfhb">404</a><br/>
<a href="/besuchen/muss/">0 Orte</a><br/>
<a href="/?s=eberstadt">Suche Eberstadt</a><br/>
<a href="/?s=dfdsfdsfdsfdsf">Suche erfolglos</a>

</div>

<?php
function script() { ?>
	
	<script src="<?php echo get_template_directory_uri(); ?>/assets/js/geo-min.js" type="text/javascript" charset="utf-8"></script>
	<script>
		if(geo_position_js.init()){
			geo_position_js.getCurrentPosition(success_callback,error_callback,{enableHighAccuracy:true});
		}
		else{
			alert("Functionality not available");
		}

		function success_callback(p)
		{
			alert('lat='+p.coords.latitude.toFixed(4)+';lon='+p.coords.longitude.toFixed(4));
		}
		
		function error_callback(p)
		{
			alert('error='+p.message);
		}		
	</script>
	
<?php
	
}


add_action( 'wp_footer', 'script', 100 );

get_footer(); ?> 