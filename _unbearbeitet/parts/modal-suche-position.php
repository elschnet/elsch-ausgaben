<?php defined('ABSPATH') or die("No script kiddies please!");
 
// Theme Name:  elsch-ausgaben
// Author:      Michael Schaefer
// Author URI:  https://elsch.net
// Version:     1.0 (2018-12)
// Text Domain:	elschnet_td
// *******************************

//  auf mobile erscheint die komplette box
//  auf desktop erscheint nur die suchbox 

?>
<div id="position" class="modal">	
	<div class="mobile">
		<h2>Marker setzen bzw. <br/>Position des Markers ändern</h2>
		<section>
			<button class="pos_aktuell einzeln"><span>aktueller Standort</span> 
				<svg width="30px" height="30px" viewBox="0 0 512 512"><path d="M500 224h-30.364C455.724 130.325 381.675 56.276 288 42.364V12c0-6.627-5.373-12-12-12h-40c-6.627 0-12 5.373-12 12v30.364C130.325 56.276 56.276 130.325 42.364 224H12c-6.627 0-12 5.373-12 12v40c0 6.627 5.373 12 12 12h30.364C56.276 381.675 130.325 455.724 224 469.636V500c0 6.627 5.373 12 12 12h40c6.627 0 12-5.373 12-12v-30.364C381.675 455.724 455.724 381.675 469.636 288H500c6.627 0 12-5.373 12-12v-40c0-6.627-5.373-12-12-12zM288 404.634V364c0-6.627-5.373-12-12-12h-40c-6.627 0-12 5.373-12 12v40.634C165.826 392.232 119.783 346.243 107.366 288H148c6.627 0 12-5.373 12-12v-40c0-6.627-5.373-12-12-12h-40.634C119.768 165.826 165.757 119.783 224 107.366V148c0 6.627 5.373 12 12 12h40c6.627 0 12-5.373 12-12v-40.634C346.174 119.768 392.217 165.757 404.634 224H364c-6.627 0-12 5.373-12 12v40c0 6.627 5.373 12 12 12h40.634C392.232 346.174 346.243 392.217 288 404.634zM288 256c0 17.673-14.327 32-32 32s-32-14.327-32-32c0-17.673 14.327-32 32-32s32 14.327 32 32z"></path></svg>
			</button>
		</section>
		
		<section>
			<button class="pos_center einzeln"><span>Kartenmitte</span> 
				<svg width="30px" height="30px" viewBox="0 0 576 512"><path d="M288 0c-69.59 0-126 56.41-126 126 0 56.26 82.35 158.8 113.9 196.02 6.39 7.54 17.82 7.54 24.2 0C331.65 284.8 414 182.26 414 126 414 56.41 357.59 0 288 0zm0 168c-23.2 0-42-18.8-42-42s18.8-42 42-42 42 18.8 42 42-18.8 42-42 42zM20.12 215.95A32.006 32.006 0 0 0 0 245.66v250.32c0 11.32 11.43 19.06 21.94 14.86L160 448V214.92c-8.84-15.98-16.07-31.54-21.25-46.42L20.12 215.95zM288 359.67c-14.07 0-27.38-6.18-36.51-16.96-19.66-23.2-40.57-49.62-59.49-76.72v182l192 64V266c-18.92 27.09-39.82 53.52-59.49 76.72-9.13 10.77-22.44 16.95-36.51 16.95zm266.06-198.51L416 224v288l139.88-55.95A31.996 31.996 0 0 0 576 426.34V176.02c0-11.32-11.43-19.06-21.94-14.86z"></path></svg>
			</button>
		</section>
	</div>
		
	<h2 class="desktop">Suchen<br/></h2><br/>
	<section>
		<input id="address" type="textbox" value="" placeholder="Google Maps-Suche&hellip;">
		<button id="suche">
			<svg width="30px" height="30px" viewBox="0 0 512 512"><path d="M505 442.7L405.3 343c-4.5-4.5-10.6-7-17-7H372c27.6-35.3 44-79.7 44-128C416 93.1 322.9 0 208 0S0 93.1 0 208s93.1 208 208 208c48.3 0 92.7-16.4 128-44v16.3c0 6.4 2.5 12.5 7 17l99.7 99.7c9.4 9.4 24.6 9.4 33.9 0l28.3-28.3c9.4-9.4 9.4-24.6.1-34zM208 336c-70.7 0-128-57.2-128-128 0-70.7 57.2-128 128-128 70.7 0 128 57.2 128 128 0 70.7-57.2 128-128 128z"></path></svg>
		</button>
		<p><small>Du kannst Google Maps nach Orten, Städten, Sehenswürdigkeiten durchsuchen, oder direkt GPS-Koordinaten eingeben.</small></p>
	</section>
	
	<br/><br/>
	<h2 class="mobile">Text-Suche<br/><br/></h2>
	<section>		
		<?php echo get_search_form(); ?>
		<p><small>Unsere bereits gespeicherten Orte durchsuchen.</small></p>		
	</section>
</div>