<?php defined('ABSPATH') or die("No script kiddies please!");
 
// Theme Name:  elsch-ausgaben
// Author:      Michael Schaefer
// Author URI:  https://elsch.net
// Version:     1.0 (2018-12)
// Text Domain:	elschnet_td
// *******************************

?>
<div class="box" id="orte_liste"><div>

	<h2>Im Umkreis von <?php echo $umkreis ?> km gefunden:</h2><?php
	
	// kategorien 
	$o_kat = array();
	foreach ( get_terms( 'pin' ) as $pin ) {
		$o_kat[ $pin->term_id ] = $pin->name;
	} 
	
	// mapmarker.php ist bereits oben in index.php eingebunden
	// $svg steht zur verfügung

	// orte auflisten (array ist bereits nach luftlinie sortiert)
	foreach ( $orte_list as $ort ) {
		
		$svg_path = $svg[ $ort['m'] ]['path'];
		$svg_view = $svg[ $ort['m'] ]['view'];
		$svg_css = $svg[ $ort['m'] ]['css'];
		$kat = $o_kat[ $ort['m'] ];

		if ( isset($svg_css) ) { $css = ' style="'. $svg_css .'"'; }
		else { $css = ''; }
				
		// sollte es noch keine svg daten für diese kategorie(pin) geben,
		// wird 0 übergeben, wir brauchen aber den kategorienamen!
		if ( $ort['m'] == 0 ) {
			$kat = wp_get_post_terms( $ort['m'], 'pin' ); 
			$kat = $kat[0]->name;
		} else {
			
		}
		
			
		echo '<button><a href="/?p='. $ort['p'] .'">';
			echo '<svg'. $css .' viewBox="'. $svg_view . '"><path '. $svg_color .' d="'. $svg_path .'"></path></svg>'; // icon
			echo '<span>'. get_field('stadt', $ort['p']) .'<br/>'; // stadt
			echo number_format($ort['l'], 2, ',', '.') .' km</span>'; // luftlinie
			echo '<small>'. $kat .'</small>'; // kategorie
			echo $ort['t']; // name des ortes
		echo '</a></button>';
	} ?>
	
</div></div>