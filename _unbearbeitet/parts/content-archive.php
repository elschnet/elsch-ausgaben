<?php defined('ABSPATH') or die("No script kiddies please!");
 
// Theme Name:  elsch-ausgaben
// Author:      Michael Schaefer
// Author URI:  https://elsch.net
// Version:     1.0 (2018-12)
// Text Domain:	elschnet_td
// *******************************



// korrdinaten für berechnung luftlinie holen
// variable wird in archive bzw search gesetzt.
list($lat, $lon) = explode(',', $GLOBALS['korr']);


// kategorien holen
$o_kat = array();
foreach ( get_terms( 'pin' ) as $pin ) {
	$o_kat[ $pin->term_id ] = $pin->name;
}


// svg datei einbinden
include get_template_directory() .'/assets/mapmarker.php';
	
	
// array aller gefunden orte schreiben
$orte_list = array();
while ( have_posts() ) { 
	the_post();
	
	// kategorie-id holen 
	$kat_id = get_the_terms( get_the_ID(), 'pin' );
	$kat_id = $kat_id[0]->term_id;
	
	// eventuell zu langen title kürzen
	$title = get_the_title();
	if ( strlen( $title ) > 25 ) { 					
		$title = substr($title, 0, 24) .'&hellip;'; 
	}	

	// koordinaten des ortes holen
	$lat_id = get_post_meta( get_the_ID(), 'lat', true );
	$lon_id = get_post_meta( get_the_ID(), 'lon', true );
	
	// berechnung luftlinie, für sortierung in textlinks
	$luftlinie = luftlinie( $lat, $lon, $lat_id, $lon_id );
	
	// array schreiben
	$orte_list[] = array( 
		'p' => get_the_ID(), // post-id
		'm' => $kat_id, // kategorie-id
		't' => $title, //title
		's' => get_field('stadt'), // stadt
		'l' => $luftlinie
	);
}


// sortierung der orte: neueste zuerst oder nach entfernung
if ( isset($_GET['show']) ) { $show = filter_input( INPUT_GET, "show", FILTER_SANITIZE_STRING ); }
$sort_url = '/ort/?k='. $lat .','. $lon;
echo '<p id="sortierung"><small>Sortierung nach: &nbsp;';
	if ( isset($show) && $show == 'news' ) {
		echo '<a href="'. $sort_url .'">Entfernung</a> &nbsp;';
		echo '<span>Speicherdatum</span>';	
	} 

	else {
		
		// array nach entfernung sortieren
		$luftlinie	= array_column($orte_list, 'l');
		array_multisort($luftlinie, SORT_ASC, $orte_list);
		echo '<span>Entfernung</span>';
		echo ' &nbsp;<a href="'. $sort_url .'&amp;show=news">Speicherdatum</a>';	
	
	}
echo '</small></p>';

// orte auflisten
foreach( $orte_list as $ort ) {

	// wenn es keine grafik für diese kategorie gibt, ersatzgrafik nehmen
	if ( !array_key_exists( $ort['m'], $svg ) ) { 
		$svg_path = $svg[0]['path'];
		$svg_view = $svg[0]['view'];
	} 
	
	// svg-grafik holen
	else {				
		$svg_path = $svg[ $ort['m'] ]['path'];
		$svg_view = $svg[ $ort['m'] ]['view'];
	}
	
	// sollte es noch keine svg daten für diese kategorie(pin) geben,
	// wird 0 übergeben, wir brauchen aber den kategorienamen!
	if ( $ort['m'] == 0 ) {
		$kat = wp_get_post_terms( $ort['p'], 'pin' ); 
		$kat = $kat[0]->name;
	} else {
		// kategoriename
		$kat = $o_kat[ $ort['m'] ]; 
	}

	// button ausgeben
	echo '<button><a href="/?p='. $ort['p'] .'">';
		echo '<svg viewBox="'. $svg_view .'"><path d="'. $svg_path .'"></path></svg>'; // icon
		echo '<span>'. $ort['s'] .'<br/>'; // stadt
		echo number_format($ort['l'], 2, ',', '.') .' km</span>'; // luftlinie
		echo '<small>'. $kat .'</small>'; // kategorie		
		echo $ort['t']; // name des ortes
	echo '</a></button>';
}