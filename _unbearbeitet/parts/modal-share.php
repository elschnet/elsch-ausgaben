<?php defined('ABSPATH') or die("No script kiddies please!");
 
// Theme Name:  elsch-ausgaben
// Author:      Michael Schaefer
// Author URI:  https://elsch.net
// Version:     1.0 (2018-12)
// Text Domain:	elschnet_td
// *******************************

//  auf mobile erscheinen mehr icons
//  als auf dem desktop

?>
<div id="share" class="modal">
	
	<h2>Position weitergeben</h2>
	<section>
		<a class="appicon google"	id="button_google"	>Google</a>
		
		<div class="mobile">		
			<a class="appicon here"		id="button_here"	>Here</a>
			<a class="appicon apple"	id="button_apple"	>Apple</a>
			<a class="appicon mapme"	id="button_mapme"	>Map-me</a>
			<a class="appicon simple"	id="button_simple"	>Simplenote</a>
			<!--<a class="appicon tripadv"	id="button_tripadv"	>TripAdvisor</a>-->
		</div>
		
		<div class="desktop">
			<a class="appicon here"		id="button_here2"	>Here</a>
			<a class="appicon osm"		id="button_osm"		>OSM</a>			
		</div>
		
		<a class="appicon tripadv"	id="button_tripadv2">TripAdvisor</a>
		<a class="appicon photo" 	id="button_nerd">Fotomotive Nerd-Content</a>
		
	</section>		
</div>