<?php defined('ABSPATH') or die("No script kiddies please!");
 
// Theme Name:  elsch-ausgaben
// Author:      Michael Schaefer
// Author URI:  https://elsch.net
// Version:     1.0 (2018-12)
// Text Domain:	elschnet_td
// *******************************


?>

<div class="content-body">
	<article class="box" id="single"><?php
	
	
		// svg (pin) kategorie name + title ?>
		<header><?php
			$svg_path = $svg[ $pin ]['path'];
			$svg_view = $svg[ $pin ]['view'];
			$kategorie = get_term( $pin, 'pin' );
			$kategorie = $kategorie->name;

			echo '<svg viewBox="'. $svg_view .'"><path d="'. $svg_path .'"></path></svg>';
			echo '<div class="single_head">';
				echo '<span>'. $kategorie .'</span>';
				the_title(); 
			echo '</div>'; ?>
		</header><?php
		
		
		
		// beitragsfoto
		if ( has_post_thumbnail( get_the_ID() ) ) {
			echo '<picture>';
				the_post_thumbnail('medium_large');
			echo '</picture>';			
		} 		

		
		
		// kurzbeschreibung 
		if ( get_field('kurzbeschreibung') ) { ?>
			<section id="kurzbeschreibung"><?php 
				echo '<h5>Kurzbeschreibung:</h5>';
				echo get_field('kurzbeschreibung'); ?>
			</section><?php
		}
		
		
		
		// tags 
		$atags = get_the_terms( get_the_ID(), 'adr' );                         
		if ( $atags && ! is_wp_error( $atags ) ) { ?>
			<section id="tags">
				<h5>Tags:</h5><?php 
				
				$adressen = '';
				foreach ( $atags as $atag ) {
					$adressen .= '<a class="k" href="/adr/'. $atag->slug .'">'. $atag->name .'</a>, ';
				}
				$adressen = substr( $adressen, 0, -2 ); // letztes komma wegschneiden
				echo $adressen; ?>
			</section><?php
		} 
		
		
		
		// buttons: koordinaten + wetter + weitere fotos ?>			
		<section id="singlebuttons">
			<button class="einzeln"><a href="#single_koordinaten" rel="modal:open">Koordinaten + Adressen</a></button><?php
			
			if( have_rows('fotos') ) { ?>
				<button class="einzeln"><a href="/fotos/?id=<?php the_ID() ?>" rel="modal:open">weitere Fotos</a></button>
			<?php } ?>
			
			<button class="einzeln"><a href="/wetter/?k=<?php echo number_format($start_lat, "4", ".", "") .','. number_format($start_lon, "4", ".", "") .'&t='. $single_title ; ?>" rel="modal:open">Wettervorhersage</a></button>
			
			<button class="einzeln"><a href="<?php echo get_edit_post_link() ?>">Ort bearbeiten</a></button>
		</section><?php
		
		
		
		
		// notizen, hinweise, beschreibung 
		if ( get_field('text') ) { ?>
			<section id="longtext">
				<h5>Notizen, Hinweise, Beschreibung</h5><?php 			
				echo get_field('text'); ?>
			</section><?php
		}
		
		
		
		// pdf-dateien
		if( have_rows('pdf-dateien') ) { ?>		
			<section id="pdf">
				<h5>PDF-Dateien:</h5><?php 
				while ( have_rows('pdf-dateien') ) {
					the_row();
					$file = get_sub_field('pdf');
					$thumbnail = wp_get_attachment_image( $file, 'thumbnail' );
					
					// bei mobile sollen pdf in chrome geöffnet werden
					// es gibt sonst kein zurück in der fake-app-ansicht
					$url = wp_get_attachment_url( $file );
					if ( wp_is_mobile() ) { $url = preg_replace('#^https?://#', 'googlechrome://', $url); }
					
					echo '<a href="'. $url .'" target="_blank">'. $thumbnail .'</a> ';
				} ?>
			</section><?php 
		} 
		
		
		
		// links
		if( have_rows('links') ) { ?>
			<section id="links">
				<h5>Links:</h5><?php 
				while ( have_rows('links') ) {
					the_row();
										
					// bei mobile sollen pdf in chrome geöffnet werden
					// es gibt sonst kein zurück in der fake-app-ansicht
					$url = get_sub_field('adresse');
					if ( wp_is_mobile() ) { $url = preg_replace('#^https?://#', 'googlechrome://', $url); }
					
					echo '<a href="'. $url .'" target="_blank">'. get_sub_field('anzeigetext') .'</a><br/>';
				} ?>
			</section><?php 
		}  ?>
		
	</article>
</div><?php



// **********************************************
// modal: koordinaten + adressen ?>
<div id="single_koordinaten" class="modal">
	<section id="dez">
		<h5>Koordinaten: Dezimalgrad</h5><?php
		echo '<p><span>Lat: </span>'. number_format($start_lat, "4", ".", "");
		echo '<br/><span>Lon: </span>'. number_format($start_lon, "4", ".", "") .'</p>'; ?>		
	</section>
	
	<section id="grad">
		<h5>Koordinaten: Grad Minuten Sekunden</h5><?php
		echo '<p><span>Lat: </span>'. fraction_to_min_sec($start_lat, 'lat' );
		echo '<br/><span>Lon: </span>'. fraction_to_min_sec($start_lon, 'lon' ) .'</p>'; ?>		
	</section>
	
	<section id="add">
		<h5>Adresse (OpenStreetMap)</h5><?php
		echo '<p>'. get_field('display_name'); 
		echo '<a href="#share" rel="modal:open">Position&nbsp;weitergeben</a></p>';?>	
	</section>
		
</div>