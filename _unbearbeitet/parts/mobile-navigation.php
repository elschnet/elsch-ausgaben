<?php defined('ABSPATH') or die("No script kiddies please!");
 
// Theme Name:  elsch-ausgaben
// Author:      Michael Schaefer
// Author URI:  https://elsch.net
// Version:     1.0 (2018-12)
// Text Domain:	elschnet_td
// *******************************


?>

<div id="nav">
	<button><a href="javascript:history.back();"><</a></button>	
	<button><a href="javascript:location.reload();">&#9850;</a></button>	
	<button><a href="javascript:history.forward();">></a></button>
</div>