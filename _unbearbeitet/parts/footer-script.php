<?php defined('ABSPATH') or die("No script kiddies please!");
 
// Theme Name:  elsch-ausgaben
// Author:      Michael Schaefer
// Author URI:  https://elsch.net
// Version:     1.0 (2018-12)
// Text Domain:	elschnet_td
// *******************************

function mapsscript() {
	global $zoom, $map_center, $map_center2, $show_orte, $show_koordinate, $passende_orte, 
		$umkreis, $icons, $single_pin, $msg, $fehler, $lat_lon; 
?>

<script>
function initMap() {<?php
	
	
	// die google maps api fügt so einiges im head der seite ein, u.a. wird die schrift
	// roboto direkt von fonts.googleapis.com gezogen. dies unterbinden wir	
	// https://stackoverflow.com/a/25902239
	echo 'var head = document.getElementsByTagName("head")[0];';
	echo 'var insertBefore = head.insertBefore;';
	echo 'head.insertBefore = function (newElement, referenceElement) {';

		// soll nur roboto geblockt werden, muss die iff bedingung wieder aktiviert werden
		#echo 'if (newElement.href && newElement.href.indexOf("//fonts.googleapis.com/css?family=Roboto") > -1) {';
			
			// diese zeile aktivieren, um zu sehen, was wir alles nicht laden
			// alle css-sachen, die da einzeln kamen, sind nun im css via "_googlemaps.scss"
			#echo 'console.info(newElement);';
			
			echo 'return;'; // gar nix laden :)
			
		#echo '}';
		echo 'insertBefore.call(head, newElement, referenceElement);';
	echo '};';
	

	// unserer eigenes kartendesign
	echo 'var styledMapType = new google.maps.StyledMapType([';
		echo '{"featureType":"administrative.locality","elementType":"geometry.fill","stylers":[{"visibility":"on"}]},';
		echo '{"featureType":"landscape","stylers":[{"hue":"#bfd592"},{"saturation":10},{"lightness":-5},{"visibility":"on"}]},';
		echo '{"featureType":"landscape.man_made","stylers":[{"hue":"#e6ebf1"},{"lightness":50},{"visibility":"on"}]},';
		#echo '{"featureType":"landscape.natural.terrain","stylers":[{"color":"#34374b"},{"lightness":60}]},';
		echo '{"featureType":"poi","stylers":[{"hue":"#91ba3d"},{"saturation":5},{"visibility":"on"}]},';
		echo '{"featureType":"poi.business","stylers":[{"hue":"#000000"},{"visibility":"off"}]},';
		echo '{"featureType":"poi.business","elementType":"labels.icon","stylers":[{"visibility":"on"},{"weight":1}]},';
		echo '{"featureType":"poi.business","elementType":"labels.text","stylers":[{"saturation":100},{"visibility":"simplified"}]},';
		echo '{"featureType":"poi.park","stylers":[{"hue":"#91BA3D"},{"saturation":4},{"lightness":-1},{"visibility":"on"}]},';
		echo '{"featureType":"poi.place_of_worship","stylers":[{"hue":"#000000"},{"lightness":5},{"visibility":"on"}]},';
		echo '{"featureType":"road.highway","elementType":"geometry","stylers":[{"saturation":100},{"lightness":40},{"visibility":"on"},{"weight":1}]},';
		echo '{"featureType":"road.local","elementType":"geometry","stylers":[{"hue":"#ffffff"},{"saturation":-100},{"lightness":100},{"visibility":"on"}]},';
		echo '{"featureType":"water","stylers":[{"hue":"#82a3c6"},{"saturation":-17},{"lightness":-15},{"visibility":"on"}]}';
	echo '],{name: "maxWOMO"});';

	
	// karte erstellen
	echo 'var mapOptions = {';
		echo 'zoom:'. $zoom .',';
		echo 'center:'. $map_center .',';
		echo 'mapTypeId:"'. $GLOBALS['map_type'] .'",';
		if ( wp_is_mobile() ) { echo 'zoomControl: false,'; }
		echo 'mapTypeControlOptions: {mapTypeIds:["roadmap","satellite","hybrid","terrain","styled_map"]}';
	echo '};';	
	echo 'var map = new google.maps.Map(document.getElementById("map"), mapOptions);'; 
	
	//Associate the styled map with the MapTypeId and set it to display.
	echo 'map.mapTypes.set("styled_map", styledMapType);';
	echo 'map.setMapTypeId("styled_map");';
	
		
	// *******************************
	// marker definieren für position des (single) ortes
	if ( is_single() ) {		 
		echo 'var ico = '. $single_pin;
		echo 'var single = new google.maps.Marker({';
			echo 'position:'. $map_center .',';
			echo 'title:"zum Inhalt springen",';
			echo 'icon:ico,';
			echo 'url:"#single",';
			echo 'animation: google.maps.Animation.DROP';
		echo '});';	
		echo 'single.setMap(map);';	
		
		// animation bei click auf marker
		echo 'single.addListener("click", function() {';
			echo 'single.setAnimation(google.maps.Animation.BOUNCE);';
			echo 'setTimeout(function(){ single.setAnimation(null); }, 1200);';
			echo 'window.location.href = this.url;';
		echo '});';
				
	}
	
	
	// *******************************
	// marker definieren für: aktuelle position, suche, center
	// https://developers.google.com/maps/documentation/javascript/reference/marker#Marker.setVisible
	echo 'var maxwomo = new google.maps.Marker({';
		if ( is_single() ) { echo 'position:'. $map_center2 .','; }
		else { echo 'position:'. $map_center .','; }
		//echo 'position:'. $map_center .',';
		echo 'draggable: true,';
		echo 'zIndex: 999'; // ist immer über allen anderen
	echo '});';	
	echo 'maxwomo.setMap(map);';

	
	// *******************************
	// aktuelle position ermitteln
	echo 'navigator.geolocation.getCurrentPosition(function(position) {';
		echo 'var pos = {';
			echo 'lat: position.coords.latitude,';
			echo 'lng: position.coords.longitude';
		echo '};';	

		// marker bei öffnen der seite positionieren, also aktuellen standort ermitteln. 
		// wurde der marker verschoben, oder über die suche positioniert, oder koordinaten 
		// via get/post übergeben, brauchen wir diese aktuelle position nicht (wäre contraproduktiv)
		if ( ( $show_orte != TRUE && $show_koordinate != TRUE ) && !is_single() ) { 
	
			// positions-text anpassen
			echo 'document.getElementById("pos_text").innerHTML = "aktuelle Position";';
							
			// aktuellen standort für kartenmitte und marker benutzen + karte zoomen
			// bei single bleibt kartenmitte auf single position
			if ( !is_single() ) { echo 'map.setCenter(pos);'; }
			echo 'maxwomo.setPosition(pos);';
			
			// aktuellen standort in formularfelder übernehmen
			//if ( !is_single() ) { 
			echo 'setLatLon(position.coords.latitude,position.coords.longitude);'; 
			//}
						
			// aktuellen standort in verstecktem feld speichern
			// notwendig für funktion luftlinie auf startseite
			echo 'var lat = position.coords.latitude;';
			echo 'lat = lat.toFixed(6);';
			echo 'var lon = position.coords.longitude;';
			echo 'lon = lon.toFixed(6);';
			echo 'document.getElementById("aktuell_pos").innerHTML = lat +","+ lon;';
		}

		if ( is_single() ) {
			//echo 'console.log("obelix");';
			echo 'setLatLon('. $lat_lon .');';	
			echo 'document.getElementById("pos_text").innerHTML = "Position des Ortes";';
			echo 'maxwomo.setVisible(false);';
		}
			
		// aktuellen standort mit kreuz
		echo 'var o = new google.maps.Marker({';
			echo 'icon: "'. get_template_directory_uri() .'/assets/images/kreuz.png",';
			echo 'zIndex: 10'; // unter allen anderen
		echo '});';	
		echo 'o.setMap(map);';
		echo 'o.setPosition(pos);';
			
	echo '}, showError);';	

	
	// es wurden koordinaten per get übergeben (passiert bei abbruch von "Neu eintragen")
	// oder es werden orte angezeigt: Hier muss der Link "alle Orte" trotzdem koordinaten per get bekommen
	if ( $show_orte == TRUE || $show_koordinate == TRUE  ) { 			
		echo 'var latlon = '. number_format($GLOBALS['start_lat'], "4", ".", "") .'+","+'. number_format($GLOBALS['start_lon'], "4", ".", "") .';';
		
		// link in navigation "alle orte"
		echo '$("a.alleorte").attr("href", "/ort/?k=" +latlon);';
		
		// koor in suchformular einbauen
		echo 'document.getElementById("search-koor").value = latlon;';
	}
	

	
	// *******************************
	// bei bewegung des markers aktuellen standort in formularfelder übernehmen
	echo 'maxwomo.addListener("dragend", function(event) {';
		echo 'setLatLon(event.latLng.lat(),event.latLng.lng());';

		// positions-text anpassen
		echo 'document.getElementById("pos_text").innerHTML = "PIN Position";';
		
		// distanz luftlinie aktualisieren
		echo 'luftlinie(event.latLng.lat(),event.latLng.lng());';
	echo '});';
	
	
	
	// *******************************
	// suchbox auswerten nach "enter" oder "click"
	// https://stackoverflow.com/a/14542114
	echo 'var geocoder = new google.maps.Geocoder();'; ?>
	document.getElementById("address").addEventListener("keypress", function(e) { <?php
		echo 'var key = e.which || e.keyCode || 0;';
		echo 'if (key === 13) {'; 
			echo 'geocodeAddress(geocoder,map,maxwomo);'; // auf "enter" reagieren	
			echo 'if ( $.modal.isActive() ) {';
				echo '$.modal.close();'; // wenn modal offen, schließen
				echo 'window.scrollTo(0,0);'; // nach oben scrollen, wichtig bei mobile
			echo '}';
		echo '}';
	echo '});';	
	echo 'document.getElementById("suche").addEventListener("click", function() {';
		echo 'geocodeAddress(geocoder,map,maxwomo);'; // auf "click" reagieren 
		echo 'if ( $.modal.isActive() ) {';
			echo '$.modal.close();';  // wenn modal offen, schließen
			echo 'window.scrollTo(0,0);'; // nach oben scrollen, wichtig bei mobile
		echo '}';
		?>
	});
	<?php
	
	
	// *******************************
	// click auf button "aktuelle position anzeigen" ?>
	$(".pos_aktuell").click(function () { <?php
		echo 'navigator.geolocation.getCurrentPosition(function(position) {';
			echo 'var pos = {';
				echo 'lat: position.coords.latitude,';
				echo 'lng: position.coords.longitude';
			echo '};';				
				
			// positions-text anpassen
			echo 'document.getElementById("pos_text").innerHTML = "aktuelle Position";';
							
			// aktuellen standort für kartenmitte und marker benutzen + karte zoomen
			echo 'map.setCenter(pos);';
			echo 'maxwomo.setPosition(pos);';
			echo 'maxwomo.setVisible(true);';
			echo 'console.log("pin auf aktuellen standort");';
			
			// distanz luftlinie anzeigen
			echo 'luftlinie(position.coords.latitude,position.coords.longitude);';
						
			// zoom der karte
			echo 'map.setZoom(12);';
			
			// aktuellen standort in formularfelder übernehmen
			echo 'setLatLon(position.coords.latitude,position.coords.longitude);';

			if ( is_single() ) {
				// auto-zoom und auto-center
				// https://coderwall.com/p/hojgtq/auto-center-and-auto-zoom-a-google-map
				echo 'bounds  = new google.maps.LatLngBounds();'; // var erstellen
				echo 'loc = new google.maps.LatLng('. $lat_lon .');';
				echo 'bounds.extend(loc);'; // position dieses ortes (single)
				echo 'loc = new google.maps.LatLng(position.coords.latitude,position.coords.longitude);';
				echo 'bounds.extend(loc);'; // aktuelle position
				echo 'map.fitBounds(bounds);';	// auto-zoom
				echo 'map.panToBounds(bounds);';// auto-center
			}
			
		echo '});';
		
		// wenn ein modal offen ist, diese schließen
		echo 'if ( $.modal.isActive() ) {';
			echo '$.modal.close();';
			echo 'window.scrollTo(0,0);'; // nach oben scrollen, wichtig bei mobile
		echo '}'; ?>
		
	}); 
	<?php
	
	
	// *******************************
	// click auf button "marker auf zentrum verschieben" ?>
	$(".pos_center").click(function () { <?php

		// wenn der marker schon in der mitte ist, 
		// liefert ein weiter klick keine daten! dies fangen wir ab
		echo 'if ( map.getCenter().lat() ) {';
		
			// marker auf kartenmitte verschieben
			echo 'maxwomo.setPosition( map.getCenter() );';
			echo 'maxwomo.setVisible(true);';
			echo 'console.log("pin auf kartenmitte");';
				
			// zoom der karte
			echo 'map.setZoom(12);';
				
			// distanz luftlinie anzeigen
			echo 'luftlinie(map.getCenter().lat(),map.getCenter().lng());';	
			
			// aktuellen standort in formularfelder übernehmen
			echo 'setLatLon(map.getCenter().lat(),map.getCenter().lng());';	
			
			// positions-text anpassen
			echo 'document.getElementById("pos_text").innerHTML = "PIN Position";';
						
			// wenn ein modal offen ist, diese schließen
			echo 'if ( $.modal.isActive() ) {';
				echo '$.modal.close();';
				echo 'window.scrollTo(0,0);'; // nach oben scrollen, wichtig bei mobile
			echo '}';

			if ( is_single() ) {
				// auto-zoom und auto-center
				echo 'bounds  = new google.maps.LatLngBounds();'; // var erstellen
				echo 'loc = new google.maps.LatLng('. $lat_lon .');';
				echo 'bounds.extend(loc);'; // position dieses ortes (single)	
				echo 'loc = new google.maps.LatLng(map.getCenter().lat(),map.getCenter().lng());';
				echo 'bounds.extend(loc);'; // position center karte
				echo 'map.fitBounds(bounds);';	// auto-zoom
				echo 'map.panToBounds(bounds);';// auto-center	
			}
			
		echo '}';
		?>			

	}); 
	<?php
	
	// zoom änderung an formular weitergeben
	echo 'google.maps.event.addListener(map, "zoom_changed", function() {';
		echo 'document.getElementById("zoom").value = map.getZoom();';		
	echo '});';


	// *******************************
	// orte anzeigen 
	if ( $show_orte == TRUE ) {

		// alle marker ausgeben 
		echo 'var locations = [';
			echo implode(",", $passende_orte);
		echo '];'; 

		
		// kreis zeichnen
		echo 'var circle = new google.maps.Circle({';
			echo 'map: map,';
			echo 'center: '. $map_center .',';
			echo 'radius: '. ($umkreis * 1000) .','; //in meters
			echo 'strokeColor: "#ff0000",';
			echo 'strokeOpacity: 0.6,';
			echo 'strokeWeight: 1,';
			echo 'fillColor: "#ffffff",';
			echo 'fillOpacity: 0';
		echo '});';
		//echo 'map.fitBounds(circle.getBounds());';

	
		// marker icons definieren
		echo 'var icons = [];';
		echo $icons;
		
		  
		// infowindows der marker
		// https://stackoverflow.com/a/40047725
		echo 'var infoWin = new google.maps.InfoWindow();';
		
		echo 'var markers = locations.map(function(location, i) {';
			echo 'var marker = new google.maps.Marker({';
				echo 'position: location,';
				echo 'zIndex: 80,';
				echo 'icon: icons[location.m]';
			echo '});';
			echo 'google.maps.event.addListener(marker, "click", function(evt) {';
				echo 'infoWin.setContent("<div id=\"popup\"><a href=\"/?p="+location.p+"\">"+location.t+"</a></div>");';
				echo 'infoWin.open(map, marker);';
			echo '});';
			echo 'return marker;';
		echo '});';
		
		
		// marker cluster: icons und textfarbe
		// https://stackoverflow.com/a/37302915
		echo 'var clustererOptions = {styles:[';
			echo '{';
				echo 'textColor: "white",';
				echo 'textSize: 14,';
				echo 'url: "'. get_template_directory_uri() .'/assets/images/cluster.svg",';
				echo 'height: 60,';
				echo 'width: 60';
			echo '},{'; 
				echo 'textColor: "white",';				
				echo 'textSize: 18,';
				echo 'url: "'. get_template_directory_uri() .'/assets/images/cluster.svg",';
				echo 'height: 75,';
				echo 'width: 75';
			echo '}';
		echo ']};';
		echo 'var markerCluster = new MarkerClusterer(map, markers,clustererOptions);';

	} // if ( $show_orte == TRUE 
	?>	
	
} 
<?php	


// fehlermeldung standort nicht ermittelbar ?>
function showError(error) { <?php
	$msg = array(); 
	$msg['text']  = 'Bitte eventuell Einstellungen prüfen. Ich verwende ['. $GLOBALS['pos_text'] .'] als Position.';
	$msg['head']  = '<h2>Standort nicht ermittelbar!</h2>';
						
	echo 'document.getElementById("msg").innerHTML = "'. $msg['head'].$msg['text'] .'";';
	echo '$("#msg").modal();';
	
	// https://www.w3schools.com/html/html5_geolocation.asp
	echo 'switch(error.code) {';
		echo 'case error.PERMISSION_DENIED: console.log("User denied the request for Geolocation."); break;';
		echo 'case error.POSITION_UNAVAILABLE: console.log("Location information is unavailable."); break;';
		echo 'case error.TIMEOUT:	console.log("The request to get user location timed out."); break;';
		echo 'case error.UNKNOWN_ERROR: console.log("An unknown error occurred."); break;';
	echo '}'; ?>	
} 
<?php


// suchbox auswerten ?>
function geocodeAddress(geocoder, map, maxwomo) {<?php
	global $lat_lon, $msg;
	
	echo 'var address = document.getElementById("address").value;';
	echo 'geocoder.geocode({"address": address}, function(results, status) {';
		echo 'if (status === "OK") {';

			// marker verschieben + sichtbar
			echo 'maxwomo.setPosition(results[0].geometry.location);';
			echo 'maxwomo.setVisible(true);';
			echo 'console.log("pin auf " +address);';
			
			// zoom der karte
			echo 'map.setZoom(12);';
			
			// distanz luftlinie anzeigen
			echo 'luftlinie(results[0].geometry.location.lat(),results[0].geometry.location.lng());';
			
			// mittelpunkt der karte setzen
			echo 'map.setCenter(results[0].geometry.location);';
			
			// eventuell verhandene fehlermeldung löschen
			echo 'document.getElementById("msg").innerHTML = "";';
						
			// aktuellen standort in formularfelder übernehmen
			echo 'setLatLon(results[0].geometry.location.lat(),results[0].geometry.location.lng());';
									
			// positions-text anpassen
			echo 'document.getElementById("pos_text").innerHTML = "PIN Position";';
			
			if ( is_single() ) {
				// auto-zoom und auto-center
				// https://coderwall.com/p/hojgtq/auto-center-and-auto-zoom-a-google-map
				echo 'bounds  = new google.maps.LatLngBounds();'; // var erstellen
				echo 'loc = new google.maps.LatLng('. $lat_lon .');';
				echo 'bounds.extend(loc);'; // position dieses ortes (single)
				echo 'loc = new google.maps.LatLng(results[0].geometry.location.lat(),results[0].geometry.location.lng());';
				echo 'bounds.extend(loc);'; // aktuelle position
				echo 'map.fitBounds(bounds);';	// auto-zoom
				echo 'map.panToBounds(bounds);';// auto-center
			
			} else {
				
				// auf der startseite wurde ein ort gesucht und gefunden
				// eventuell vorhandene "gefundene orte" werden ausgeblendet, machen jetzt keinen sinn mehr
				echo '$("#ergebnis").hide();';
				echo '$("#luftlinie").hide();';
				echo '$("#orte_liste").hide();';
				echo '$(".filter_button").removeClass("active")';
				
			}
			
		echo '} else {';
			// fehlermeldung ausgeben
			$msg = array();
			$msg['head']  = '<h2>Ort nicht gefunden!</h2>';
			$msg['text']  = '<p>Google Maps kennt keinen Ort mit diesem Namen. Bitte die Schreibweise prüfen.</p>';
			$msg['text'] .= '<p><button><a href=\"#position\" rel=\"modal:open\">Suche&hellip;</a></button></p>';
						
			echo 'document.getElementById("msg").innerHTML = "'. $msg['head'].$msg['text'] .'";';
			echo '$("#msg").modal();';
			echo 'console.log("suche status: " + status);';
		echo '}';
	echo '});'; ?>
}
<?php 


// *******************************
// luftlinie per ajax holen ?>
function luftlinie(lat,lon) { <?php
	
	// auf startseite aktuelle position als zweiten wert holen
	echo 'var latlon = document.getElementById("aktuell_pos").innerHTML;';
	
	// auf single ist natürlich die single position der zweite wert
	if ( is_single() ) {
		global $lat_lon;
		echo 'var latlon = "'. $lat_lon .'";';
	}
	
	// werte kürzen
	echo 'lat = lat.toFixed(6);';
	echo 'lon = lon.toFixed(6);';
	/*
	echo 'console.log("latlon:" +latlon );';
	echo 'console.log("lat:" +lat );';
	echo 'console.log("lon:" +lon );';
	*/	
	echo 'if ( latlon ) {';
		//https://www.w3schools.com/js/tryit.asp?filename=tryjs_ajax_xmlhttp
		//https://www.w3schools.com/js/js_ajax_http.asp
		echo 'var xhttp = new XMLHttpRequest();';
		echo 'xhttp.onreadystatechange = function() {';
			echo 'if (this.readyState == 4 && this.status == 200) {';
				echo 'document.getElementById("luftlinie").innerHTML = this.responseText;';
			echo '}';
		echo '};';
		echo 'xhttp.open("GET", "https://o.elsch.de/luftlinie/?k="+latlon+","+lat+","+lon, true);';
		echo 'xhttp.send();'; 
	echo '} else {';
		echo 'console.log("kein zweiter wert für berechnung");';
	echo '}';
	?>
}
<?php 


// *******************************
// den formularen die neue position zuweisen ?>
function setLatLon(lat,lon) {<?php
	// form neu
	echo 'document.getElementById("new_lat").value  = lat;';
	echo 'document.getElementById("new_lon").value  = lon;';
	// form orte zeigen
	echo 'document.getElementById("show_lat").value = lat;';
	echo 'document.getElementById("show_lon").value = lon;';
	// anzeige lat/lon
	echo 'document.getElementById("admin_lat").innerHTML = lat.toFixed(4);';
	echo 'document.getElementById("admin_lon").innerHTML = lon.toFixed(4);'; 
	
	// link zu "alle orte" anpassen
	echo 'var latlon = lat.toFixed(4)+ "," +lon.toFixed(4);';
	echo '$("a.alleorte").attr("href", "/ort/?k=" +latlon);';
	
	// koor in suchformular einbauen
	echo 'document.getElementById("search-koor").value = latlon;';
	
	// links zu tags mit koor versehen
	if ( is_single() ) {
		echo '$( "a.k" ).each( function() {';
			echo 'var href = $(this).prop("href");';
			echo '$(this).prop("href", href+"?k="+latlon);';
		echo '});';	
	}
	
	?>
}
<?php


// **********************************************
// filter ?>
$(document).ready(function() { <?php
	
	// loading.gif bei klick auf "orte zeigen"
	echo '$( "#button_zeigen" ).click(function() {';
		echo '$("#load").show();';
	echo '});';

	// funktion für button im modal filtern
	// sendet das formular "Orte anzeigen" ab
	echo '$( "#orte_filtern" ).click(function() {';
		echo '$.modal.close();';
		echo 'window.scrollTo(0,0);'; // nach oben scrollen, wichtig bei mobile
		echo '$("#load").show();';
		echo '$( "#orte_zeigen" ).submit();';
	echo '});';
	
	// filter kategorien: button alle kategorien
	echo '$(".kategorie").on("click", ".k-all", function(e) {';
		echo 'e.preventDefault();';
		
		echo '$("a.k").removeClass("active");';
		echo '$(this).addClass("active");';		
		echo 'var kategorie = "";';
		
		echo 'document.getElementById("kategorien").value = kategorie;';
	echo '});';
		
	// filter kategorien: buttons einzelne kategorien
	echo '$(".kategorie").on("click", ".k", function(e) {';
		echo 'e.preventDefault();';	
		echo '$("a.k-all").removeClass("active");'; //button "alle" ist nicht mehr active		
		echo 'var kategorie = document.getElementById("kategorien").value;';	//bisherige werte holen
		echo 'var wert = $(this).data("filter");'; //wert als variable
		
		//active löschen
		echo 'if ( $(this).hasClass("active") ) {'; 			
			echo '$(this).removeClass("active");';
			echo 'var kat = kategorie.split(",");';
			echo 'var kat = arrayRemove(kat, wert);';
			echo 'var kategorie = kat.toString();';
			echo 'if (kategorie=="") {$(".k-all").addClass("active")};';
		//active setzen
		echo '} else {';			
			echo 'var kategorie = kategorie + "," + wert;';
			echo '$(this).addClass("active");';	
		echo '}';
		
		//zurückgeben 
		echo 'document.getElementById("kategorien").value = kategorie;';
	echo '});';

	// filter umkreis
	// https://stackoverflow.com/a/43169454
	echo '$(".umkreis").on("click", ".u", function(e) {';
		echo 'e.preventDefault();';
		
		echo '$("a.u").removeClass("active");';
		echo '$(this).addClass("active");';		
		echo 'var umkreis = $(this).data("filter");';
		
		echo 'document.getElementById("umkreis").value = umkreis;';
	echo '});';	

	// filter specials
	echo '$(".special").on("click", ".s", function(e) {';
		echo 'e.preventDefault();';
		
		echo '$("a.s").removeClass("active");';
		echo '$(this).addClass("active");';
		echo 'var special = $(this).data("filter");';
		
		echo 'document.getElementById("specials").value = special;';
	echo '});';
		
	// filter besuchen
	echo '$(".besuchen").on("click", ".b", function(e) {';
		echo 'e.preventDefault();';
		
		echo '$("a.b").removeClass("active");';
		echo '$(this).addClass("active");';
		echo 'var besuchen = $(this).data("filter");'; 
		
		echo 'document.getElementById("besuch").value = besuchen;';
	echo '});';

		
		
	// **********************************************
	// ein element aus array löschen
	// https://love2dev.com/blog/javascript-remove-from-array/
	echo 'function arrayRemove(arr, value) {';
	   echo 'return arr.filter(function(ele){';
		   echo 'return ele != value;';
	   echo '});';
	echo '}'; ?>
});
<?php


// **********************************************
// share modal: position weitergeben
// suchfeld leeren ?>
$(document).ready(function() { <?php

	// weitergabe an google position (button_google)
	echo '$( "#button_google" ).click(function() {';
		echo 'var wtglat = document.getElementById("admin_lat").innerHTML;';
		echo 'var wtglon = document.getElementById("admin_lon").innerHTML;';
		echo 'var wtgurl = "https://www.google.de/maps/search/" +wtglat+ "," +wtglon;';
		// route
		// echo 'var wtgurl = "https://www.google.de/maps/dir/?api=1&travelmode=driving&destination=" +wtglat+ "," +wtglon;';		
		echo 'window.open(wtgurl);';
	echo '});';
	
	// weitergabe an here (button_here) mobile
	echo '$( "#button_here" ).click(function() {';
		echo 'var wtglat = document.getElementById("admin_lat").innerHTML;';
		echo 'var wtglon = document.getElementById("admin_lon").innerHTML;';
		echo 'var wtgurl = "here-location://" +wtglat+ "," +wtglon;';
		echo 'window.open(wtgurl);';
	echo '});';
	
	// weitergabe an here (button_here2) desktop
	echo '$( "#button_here2" ).click(function() {';
		echo 'var wtglat = document.getElementById("admin_lat").innerHTML;';
		echo 'var wtglon = document.getElementById("admin_lon").innerHTML;';
		echo 'var wtgurl = "https://wego.here.com/?map=" +wtglat+ "," +wtglon+ ",15,normal";';
		echo 'window.open(wtgurl);';
	echo '});';

	// weitergabe an apple karten (button_apple)
	echo '$( "#button_apple" ).click(function() {';
		echo 'var wtglat = document.getElementById("admin_lat").innerHTML;';
		echo 'var wtglon = document.getElementById("admin_lon").innerHTML;';
		echo 'var pin_title = document.getElementById("pin_title").innerHTML;';
		// route
		// echo 'var wtgurl = "http://maps.apple.com/?dirflg=d&daddr=" +wtglat+ "," +wtglon;'; 
		echo 'var wtgurl = "http://maps.apple.com/?q=" +pin_title+ "&ll=" +wtglat+ "," +wtglon;';
		echo 'window.open(wtgurl);';
	echo '});';

	// weitergabe an map.me (button_mapme)
	echo '$( "#button_mapme" ).click(function() {';
		echo 'var wtglat = document.getElementById("admin_lat").innerHTML;';
		echo 'var wtglon = document.getElementById("admin_lon").innerHTML;';
		echo 'var pin_title = document.getElementById("pin_title").innerHTML;';
		echo 'var wtgurl = "mapswithme://map?v=1&ll=" +wtglat+ "," +wtglon+ "&n=" +pin_title;';
		echo 'window.open(wtgurl);';
	echo '});';

	// weitergabe an osm (button_osm)
	echo '$( "#button_osm" ).click(function() {';
		echo 'var wtglat = document.getElementById("admin_lat").innerHTML;';
		echo 'var wtglon = document.getElementById("admin_lon").innerHTML;';
		echo 'var wtgurl = "https://www.openstreetmap.org/?mlat=" +wtglat+ ",&mlon=" +wtglon+ "&zoom=15";';
		echo 'window.open(wtgurl);';
	echo '});';

	// weitergabe an simplenote (button_simple)
	echo '$( "#button_simple" ).click(function() {';
		echo 'var wtglat = document.getElementById("admin_lat").innerHTML;';
		echo 'var wtglon = document.getElementById("admin_lon").innerHTML;';
		echo 'var pin_title = document.getElementById("pin_title").innerHTML;';
		echo 'var wtgurl = "simplenote://new?content=" +pin_title+ "%3A " +wtglat+ "%2C " +wtglon;';
		echo 'window.open(wtgurl);';
	echo '});';

	// weitergabe an tripadvisor (button_tripadv) mobile
	// leider wird hier die position nicht weitergegeben
	/*
	echo '$( "#button_tripadv" ).click(function() {';
		echo 'var wtglat = document.getElementById("admin_lat").innerHTML;';
		echo 'var wtglon = document.getElementById("admin_lon").innerHTML;';
		echo 'var triplat = wtglat.replace(".", "__2E__");';
		echo 'var triplon = wtglon.replace(".", "__2E__");';
		echo 'var wtgurl = "tripadvisor://Home-a_latitude." +triplat+ "-a_longitude." +triplon;';
		echo 'window.open(wtgurl);';
	echo '});';
	*/
	
	// weitergabe an tripadvisor (button_tripadv2) desktop
	echo '$( "#button_tripadv2" ).click(function() {';
		echo 'var wtglat = document.getElementById("admin_lat").innerHTML;';
		echo 'var wtglon = document.getElementById("admin_lon").innerHTML;';
		echo 'var triplat = wtglat.replace(".", "__2E__");';
		echo 'var triplon = wtglon.replace(".", "__2E__");';
		echo 'var wtgurl = "https://www.tripadvisor.de/Home-a_latitude." +triplat+ "-a_longitude." +triplon;';
		echo 'window.open(wtgurl);';
	echo '});';
	
	// öffnen von nerd-links (button_nerd) 
	echo '$( "#button_nerd" ).click(function() {';
		echo 'var wtglat = document.getElementById("admin_lat").innerHTML;';
		echo 'var wtglon = document.getElementById("admin_lon").innerHTML;';
		echo 'var wtgurl = "https://o.elsch.de/nerd-links/?k=" +wtglat+ "," +wtglon;';

		echo '$.get(wtgurl, function(html) {';
			echo '$(html).appendTo("body").modal();';
		echo '});';	
	echo '});';
	
	/*
	// weitergabe an gmail		
	echo '$( "#button_gmail" ).click(function() {';
		//echo 'var wtglat = document.getElementById("admin_lat").innerHTML;';
		//echo 'var wtglon = document.getElementById("admin_lon").innerHTML;';
		//echo 'var subject = "Mein Standort";';
		//echo 'var body = "<a href=\'https://www.google.de/maps/search/?api=1&query=" +wtglat+ "," +wtglon+ "\'>Mein Standort</a>";';
		echo 'var wtgurl = "inbox-gmail:///co?subject=sub&body=hi";';
		//echo 'var wtgurl = "googlegmail:///co?subject=" +encodeURIComponent(subject)+ "&body=" +encodeURIComponent(body);';
		echo 'window.open(wtgurl);';
	echo '});';
	*/
	
	// suchfeld bei wiederholter suche leeren
	echo '$("#address").focus(function(){$(this).val("");});';
	
	?>
});
<?php


// **********************************************
// smooth scroll + fehlermeldungen ?>
$(document).ready(function(){ <?php
	// https://www.w3schools.com/howto/howto_css_smooth_scroll.asp#section2
	
	// Add smooth scrolling to all links
	echo '$("a#scroll").on("click", function(event) {';

		// Make sure this.hash has a value before overriding default behavior
		echo 'if (this.hash !== "") {';
			// Prevent default anchor click behavior
			echo 'event.preventDefault();';

			// Store hash
			echo 'var hash = this.hash;';

			// Using jQuery's animate() method to add smooth page scroll
			// The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
			echo '$("html, body").animate({';
				echo 'scrollTop: $(hash).offset().top';
				echo '}, 800 );';
				
		echo '}'; 
	echo '});'; 
	
	
	// fehlermeldung im modal ausgeben
	// "keine orte gefunden" bzw. "kein suchergebnis" bzw. "falsches template"
	if ( isset( $fehler ) ) {
		echo '$("#msg").modal();';		
	} ?>
});  
<?php



// **********************************************
// - die höhe der karte an das iphone anpassen
//   https://www.mediaevent.de/javascript/window-screen.html
// - standalone oder im browser geöffnet
//   https://stackoverflow.com/a/41749865	
 ?>
$(document).ready(function() {<?php
	
	// für iphoneXR die karte vergrößern
	echo 'if ( window.screen.availHeight > 667 && window.screen.availHeight < 1000 ) {';
		echo 'var map_height = window.screen.availHeight - 270;';
		echo 'document.getElementById("map").style.height = map_height + "px";';
	echo '}';
			
	// für desktop die karte vergrößern
	echo 'if ( window.screen.availHeight > 1000 ) {';
		echo 'var map_height = window.screen.availHeight - 320;';
		echo 'document.getElementById("map").style.height = map_height + "px";';
	echo '}'; 
	
	if ( is_single() ) {
		echo 'single.scrollIntoView();';
	}
	
	
	// testen, ob die seite fullscreen oder im browser geöffnet wurde	
	echo 'if ( !window.matchMedia("(display-mode: standalone)").matches ) {';
		echo 'console.log("This is running NOT as standalone.");';
	echo '}'; ?>
	
});


</script>
<script src="https://maps.googleapis.com/maps/api/js?key=<?php echo $GLOBALS['google_api'] ?>&callback=initMap" async defer></script>
<?php

}
add_action( 'wp_footer', 'mapsscript', 100 );