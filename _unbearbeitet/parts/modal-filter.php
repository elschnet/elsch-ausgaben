<?php defined('ABSPATH') or die("No script kiddies please!");
 
// Theme Name:  elsch-ausgaben
// Author:      Michael Schaefer
// Author URI:  https://elsch.net
// Version:     1.0 (2018-12)
// Text Domain:	elschnet_td
// *******************************

//  auf mobile erscheint die komplette box
//  auf desktop erscheint nur die suchbox 

?>

<div id="filter" class="modal"><?php

	// umkreis ?>	
	<div class="umkreis">
		<span>Umkreis:</span><?php
		$uwerte = array(10,25,50,100,200,500,800);
		//if ( current_user_can('edit_post') ) { $uwerte[] = 22000; }
		foreach ( $uwerte as $uwert ) {
			echo '<button><a href="#" class="u';
			if ( $uwert == $umkreis ) { echo ' active'; }
			echo '" data-filter="'. $uwert .'">'. $uwert .' km</a></button>';			
		} ?>
	</div>
	<?php
	
	// kategorien ?>	
	<div class="kategorie">
		<span>Kategorien:</span><?php
		echo '<button><a href="#" class="k-all';
		if ( empty( $kategorien ) ) { echo ' active'; }
		echo '">alle</a></button>';

		$kat = explode( ',', $kategorien );
		
		$pins = get_terms( array(
			'taxonomy' => 'pin',
			'exclude' => 130 //'none'
		) ); 
		
		foreach( $pins as $pin ) { 
			echo '<button><a href="#" class="k';
			if ( in_array( $pin->term_id, $kat) ) { echo ' active'; }
			echo '" data-filter="'. $pin->term_id;
			echo '">'. $pin->name .'</a></button>';
		} ?>		
	</div>
	<?php
	
	// specials ?>	
	<div class="special">
		<span>Specials:</span><?php
		echo '<button><a href="#" class="s';
		if ( empty( $specials ) ) { echo ' active'; }
		echo '" data-filter="">alle</a></button>';

		$spe = explode( ',', $specials );
		
		$specials = get_terms( array(
			'taxonomy' => 'special'
		) ); 	
		
		foreach( $specials as $special ) { 
			echo '<button><a href="#" class="s';
			if ( in_array( $special->term_id, $spe) ) { echo ' active'; }
			echo '" data-filter="'. $special->term_id;
			echo '">'. $special->name .'</a></button>';
		} ?>			
	</div>
	<?php
	
	// besuchen ?>	
	<div class="besuchen">
		<span>Besuchen:</span><?php
		echo '<button><a href="#" class="b';
		if ( empty( $besuchen ) ) { echo ' active'; }
		echo '" data-filter="">alle</a></button>';

		$spe = explode( ',', $besuchen );
		
		$besuchen = get_terms( array(
			'taxonomy' => 'besuchen'
		) ); 	
		
		foreach( $besuchen as $b ) { 
			echo '<button><a href="#" class="b';
			if ( in_array( $b->term_id, $spe) ) { echo ' active'; }
			echo '" data-filter="'. $b->term_id;
			echo '">'. $b->name .'</a></button>';
		} ?>			
	</div>

	<button id="orte_filtern">Orte zeigen</button>
</div>