<?php defined('ABSPATH') or die("No script kiddies please!");
 
// Theme Name:  elsch-ausgaben
// Author:      Michael Schaefer
// Author URI:  https://elsch.net
// Version:     1.0 (2018-12)
// Text Domain:	elschnet_td
// *******************************

// **********************************************
//  get-variable: koordinaten
// **********************************************
// koordinaten übernehmen und prüfen
$koor = filter_input( INPUT_GET, "k", FILTER_SANITIZE_STRING );
list($lat, $lon) = explode(',', $koor);

// keine koordinaten übergeben: ersatzwerte + subhead
$subhead ='';
if ( !is_numeric($lat) || !is_numeric($lon) ) {		
	// mindestens ein wert ist keine zahl oder leer
	$subhead .= 'Es wurde keine Referenz-Koordinaten übergeben. Ich verwende [';
	$subhead .= $GLOBALS['pos_text'] .'] als Referenz: ';
	
	$lat = $GLOBALS['start_lat'];
	$lon = $GLOBALS['start_lon'];
}

// koordinaten vorhanden: subhead
else { 
	$subhead .= 'Referenz: '; 
}

// subhead zusammenstellen
$subhead .= number_format($lat, 4, '.', ',') .' / '. number_format($lon, 4, '.', '');
$subhead = '<p><small>'. $subhead .'<span id="korr_gefunden"></span></small></p>';

// globale variable für content-archive.php erstellen
$GLOBALS['korr'] = "$lat,$lon";


// **********************************************
//  header + menü
// ********************************************** 
get_header(); ?>
<header class="content-header"><?php

	// offcanvas-menü button ?>
	<label for="offcanvas-menu" class="toggle-btn">
		<svg width="30" height="30" id="hamburger" viewBox="0 0 24 16">
			<line fill="none" stroke="#FFF" stroke-width="2" x1="0" y1="1" x2="24" y2="1" />
			<line fill="none" stroke="#FFF" stroke-width="2" x1="0" y1="8" x2="24" y2="8" />
			<line fill="none" stroke="#FFF" stroke-width="2" x1="0" y1="15" x2="24" y2="15" />
		</svg>
	</label><?php
	
	// headline ?>
	<span class="logo"></span><?php
	
	// alle Orte ?>	
	<div class="right">
		<button><a href="/ort/" class="alleorte">alle Orte</a></button>
	</div>
	
</header><?php



// **********************************************
//  inhalt
// ********************************************** ?>
<div class="content-body" id="result"><?php

	// headline
	$anz_posts = count($posts);
	if ( $anz_posts ) {
		echo '<h1>Suchergebnis: '. $anz_posts;
		echo ' '. ngettext('Ort', 'Orte', $anz_posts);
		echo '</h1>';
	} else {
		echo '<h1>Kein Suchergebnis</h1>';
		echo '<div style="display:none">'. $subhead .'</div>';
	}
		
	// suchformular
	echo '<div class="search">';
	echo get_search_form();
	echo '</div>';

	
	// auflistung gefundene orte 
	if ( have_posts() ) { ?>	
		<div class="box" id="orte_liste"><div><?php
			echo $subhead;
			get_template_part( 'parts/content', 'archive' ); ?>				
		</div></div><?php		
	} ?>
	
</div><?php



// **********************************************
//  seitenfuß
// ********************************************** ?>
<footer class="content-footer"><?php
 		
	// vorherige/nächste seite (nur mobile)
	get_template_part( 'parts/mobile', 'navigation' ); ?>
	
</footer>
<?php


// **********************************************
//  javascript im footer laden
// ********************************************** 
function searchpage() {
	global $koor;

	echo '<script>';
		echo '$("#search-field").focus(';
		echo 'function(){';
			echo '$(this).val("");';
		echo '});';
	echo '</script>';
		
	
	if ( !isset($koor) ) {	
		// wenn keine koordinaten übergeben wurden
		echo '<script src="'. get_template_directory_uri() .'/assets/js/geo-min.js"></script>';
		echo '<script>';
		
			echo 'if ( geo_position_js.init() ) {';
				// koordinaten übermitteln
				echo 'geo_position_js.getCurrentPosition(success_callback,error_callback,{enableHighAccuracy:true});';
			echo '} else {';
				// bei funktion nicht möglich: am pelz
				echo 'var latlon = '. $GLOBALS['start_lat'] .'+","+'. $GLOBALS['start_lon'] .';';
				echo 'get_koordinaten(latlon)'; 
			echo '}';

			echo 'function success_callback(p) {';
				// ermittelte koordinaten verwenden
				echo 'var latlon = p.coords.latitude.toFixed(4) +","+ p.coords.longitude.toFixed(4);';
				echo 'get_koordinaten(latlon)';
				$koordinaten_gefunden = true;
			echo '}';

			echo 'function error_callback(p) {';
				// bei fehler: am pelz
				echo 'var latlon = '. $GLOBALS['start_lat'] .'+","+'. $GLOBALS['start_lon'] .';';
				echo 'get_koordinaten(latlon)'; 
			echo '}';
			
			// koordinaten an links anhängen
			echo 'function get_koordinaten(latlon) {';
				echo '$( "a.alleorte" ).attr("href", "/ort/?k=" +latlon);'; // link in navigation "alle orte"
					
				// möglichkeit zur aktualisierung der seite anbieten
				// wenn koordinaten ermittelt wurden
				if ( $koordinaten_gefunden ) { 
					echo 'document.getElementById("korr_gefunden").innerHTML = "';
					echo '. Neue Koordinaten gefunden: <a class=\'korr_gefunden\' ';
					echo 'href=\'/?k="+latlon+"&s='. get_search_query() .'\'>Referenz aktualisieren</a>";';
				}
			echo '}';
		echo '</script>';		
	} 
	
	
	// es wurden koordinaten übergeben
	else {
		echo '<script>';
			
			// link in navigation + button "alle orte"
			echo '$( "a.alleorte" ).attr("href", "/ort/?k='. $GLOBALS['korr'] .'");'; 
				
			// korr in suchformular einbauen
			echo 'document.getElementById("search-koor").value = "'. $GLOBALS['korr'] .'";';
		echo '</script>';
	}
}
add_action( 'wp_footer', 'searchpage', 100 );

get_footer(); ?> 