<?php defined('ABSPATH') or die("No script kiddies please!");
 
// Theme Name:  elsch-ausgaben
// Author:      Michael Schaefer
// Author URI:  https://elsch.net
// Version:     1.0 (2018-12)
// Text Domain:	elschnet_td
// *******************************



// **********************************************
//  get-variable: koordinaten
// **********************************************
// koordinaten übernehmen und prüfen
$koor = filter_input( INPUT_GET, "k", FILTER_SANITIZE_STRING );
if ( isset($koor) ) { list($lat, $lon) = explode(',', $koor); }

// keine koordinaten übergeben: ersatzwerte + subhead
$subhead ='';
if ( !isset($lat) || !isset($lon) || !is_numeric($lat) || !is_numeric($lon) ) {		
	// mindestens ein wert ist keine zahl oder leer
	$subhead .= 'Es wurde keine Referenz-Koordinaten übergeben. Ich verwende [';
	$subhead .= $GLOBALS['pos_text'] .'] als Referenz: ';
	
	$lat = $GLOBALS['start_lat'];
	$lon = $GLOBALS['start_lon'];
}

// koordinaten vorhanden: subhead
else { 
	$subhead .= 'Referenz: '; 
	$koor = '?k='. $koor;
}

// subhead zusammenstellen
$subhead .= number_format($lat, 4, '.', ',') .' / '. number_format($lon, 4, '.', '');
$subhead = '<p><small>'. $subhead .'<span id="korr_gefunden"></span></small></p>';

// globale variable für content-archive.php erstellen
$GLOBALS['korr'] = "$lat,$lon";


// **********************************************
//  header + menü
// ********************************************** 
get_header(); ?>
<header class="content-header"><?php

	// offcanvas-menü button ?>
	<label for="offcanvas-menu" class="toggle-btn">
		<svg width="30" height="30" id="hamburger" viewBox="0 0 24 16">
			<line fill="none" stroke="#FFF" stroke-width="2" x1="0" y1="1" x2="24" y2="1" />
			<line fill="none" stroke="#FFF" stroke-width="2" x1="0" y1="8" x2="24" y2="8" />
			<line fill="none" stroke="#FFF" stroke-width="2" x1="0" y1="15" x2="24" y2="15" />
		</svg>
	</label><?php
	
	// headline ?>
	<span class="logo"></span><?php
	
	// filter-button ?>	
	<div class="right">
		<button><a href="#filtern" rel="modal:open">Filtern&hellip;</a></button>
	</div>
	
</header>
<?php



// **********************************************
//  inhalt
// ********************************************** ?>
<div class="content-body archive"><?php

	// headline
	$anz_posts = count($posts);
	echo '<h1>';
	
	if ( $anz_posts ) {
		
		// headline je nach archiv-art
		if ( is_tax('special') ) { 
			echo 'Special &bdquo;';
			single_term_title();
			echo '&ldquo;';
		} 
		elseif ( is_tax('besuchen') ) {
			echo strtoupper( single_term_title('', false) );
			echo ' Besuche';
		} 
		elseif ( is_tax('pin') ) {
			echo 'Kategorie &bdquo;';
			single_term_title();
			echo '&ldquo;';
		} 
		elseif ( is_tax('adr') ) {
			echo 'Adresse &bdquo;';
			single_term_title();
			echo '&ldquo;';
		} 
		else {
			// aufruf über /ort/
			echo 'Alle Orte'; 
		}
		echo ' - '. $anz_posts;
		echo '&nbsp;'. ngettext('Ort', 'Orte', $anz_posts);
	} else {
		echo 'Keinen Ort gefunden';
	}
	echo '</h1>';

	
	// auflistung gefundene orte 
	if ( have_posts() ) { ?>	
		<div class="box" id="orte_liste"><div><?php
			echo $subhead;
			get_template_part( 'parts/content', 'archive' ); ?>		
		</div></div><?php
	} 
	
	// 0 gefunden orte (warum auch immer)
	else { ?>
		<div class="box" id="result"><?php
			echo $subhead;
		
			// hilfreiche links
			echo '<button class="einzeln"><a href="/">zur Startseite</a></button>';
			echo '<button class="einzeln"><a href="/ort/'. $koor .'" class="alleorte">alle Orte anzeigen</a></button>'; 
			
			// suchformular
			echo get_search_form(); ?>
		 
		</div><?php
		
	} ?>
	
</div>
<?php



// **********************************************
//  seitenfuß
// ********************************************** ?>
<footer class="content-footer"><?php
 		
	// vorherige/nächste seite (nur mobile)
	get_template_part( 'parts/mobile', 'navigation' ); ?>
	
</footer>
<?php




// **********************************************
//  javascript im footer laden
// ********************************************** 
function archivepage() { 
	global $koor;
	
	if ( !isset($koor) ) {
		
		// wenn keine koordinaten übergeben wurden
		echo '<script src="'. get_template_directory_uri() .'/assets/js/geo-min.js"></script>';
		echo '<script>';
		
			echo 'if ( geo_position_js.init() ) {';
				// koordinaten übermitteln
				echo 'geo_position_js.getCurrentPosition(success_callback,error_callback,{enableHighAccuracy:true});';
			echo '} else {';
				// bei funktion nicht möglich: am pelz
				echo 'var latlon = '. $GLOBALS['start_lat'] .'+","+'. $GLOBALS['start_lon'] .';';
				echo 'get_koordinaten(latlon)'; 
			echo '}';

			echo 'function success_callback(p) {';
				// ermittelte koordinaten verwenden
				echo 'var latlon = p.coords.latitude.toFixed(4) +","+ p.coords.longitude.toFixed(4);';
				echo 'get_koordinaten(latlon)';
				$koordinaten_gefunden = true;
			echo '}';

			echo 'function error_callback(p) {';
				// bei fehler: am pelz
				echo 'var latlon = '. $GLOBALS['start_lat'] .'+","+'. $GLOBALS['start_lon'] .';';
				echo 'get_koordinaten(latlon)'; 
			echo '}';
			
			// koordinaten an links anhängen
			echo 'function get_koordinaten(latlon) {';
				echo '$( "a.alleorte" ).attr("href", "/ort/?k=" +latlon);'; // link in navigation "alle orte"
				echo '$( "a.k" ).each( function() {';
					echo 'var href = $(this).prop("href");';
					echo '$(this).prop("href", href+"?k="+latlon);';
				echo '});';	
					
				// möglichkeit zur aktualisierung der seite anbieten
				// wenn koordinaten ermittelt wurden
				if ( $koordinaten_gefunden ) { 
					$pagename = get_query_var('pagename'); 
					echo 'document.getElementById("korr_gefunden").innerHTML = "';
					echo '. Neue Koordinaten gefunden: <a class=\'korr_gefunden\' ';
					echo 'href=\''. $pagename .'?k="+latlon+"\'>Referenz aktualisieren</a>";';
				}
			echo '}';
		echo '</script>';		
	} 
	
	// es wurden koordinaten übergeben
	else {
		echo '<script>';
			// link in navigation "alle orte"
			echo '$( "a.alleorte" ).attr("href", "/ort/'. $koor .'");'; 
		echo '</script>';
	}

}
add_action( 'wp_footer', 'archivepage', 100 );

get_footer();




// **********************************************
//  hauptmodal: filter
// ********************************************** ?>
<div id="filtern" class="modal archive">
	
	<h1>Filtern</h1>
	<button class="archiv"><a href="#kategorien" rel="modal:open">nach Kategorien filtern&hellip;</a></button>
	<button class="archiv"><a href="#specials" rel="modal:open">nach Specials filtern&hellip;</a></button>
	<button class="archiv"><a href="#adressen" rel="modal:open">nach Adressen filtern&hellip;</a></button>
	<button class="archiv"><a href="/besuchen/kann/<?php echo $koor; ?>" class="k">alle KANN Besuche</a></button>
	<button class="archiv"><a href="/besuchen/muss/<?php echo $koor; ?>" class="k">alle MUSS Besuche</a></button>

</div>
<?php



// **********************************************
//  filter: kategorien
// ********************************************** ?>
<div id="kategorien" class="modal archive">
	
	<button class="einzeln"><a href="#filtern" rel="modal:open">zurück</a></button>
	<h1>Filtern nach Kategorien</h1><?php
	
	$pins = get_terms( array(
		'taxonomy' => 'pin',
		'exclude' => 130, //'none'
	) );
	
	foreach( $pins as $pin ) {
		echo '<button class="archiv"><a href="/pin/'. $pin->slug .'/'. $koor .'" class="k">'. $pin->name .'</a></button>';
	} ?>
	
</div>
<?php



// **********************************************
//  filter: specials
// ********************************************** ?>
<div id="specials" class="modal archive">
	
	<button class="einzeln"><a href="#filtern" rel="modal:open">zurück</a></button>
	<h1>Filtern nach Specials</h1><?php

	$specials = get_terms( array(
		'taxonomy' => 'special'
	) );
	
	foreach( $specials as $special ) {
		echo '<button class="archiv"><a href="/special/'. $special->slug .'/'. $koor .'" class="k">'. $special->name .'</a></button>';
	} ?>

</div>
<?php



// **********************************************
//  filter: adressen
// ********************************************** ?>
<div id="adressen" class="modal archive">
	
	<button class="einzeln"><a href="#filtern" rel="modal:open">zurück</a></button>
	<h1>Filtern nach Adressen</h1><?php

	$adrs = get_terms( array(
		'taxonomy' => 'adr'
	) );
	
	foreach( $adrs as $adr ) {
		echo '<button class="archiv"><a href="/adr/'. $adr->slug .'/'. $koor .'" class="k">'. $adr->name .'</a></button>';
	} ?>
	
</div>