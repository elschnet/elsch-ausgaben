<?php defined('ABSPATH') or die("No script kiddies please!");
 
// Theme Name:  elsch-ausgaben
// Author:      Michael Schaefer
// Author URI:  https://elsch.net
// Version:     1.0 (2018-12)
// Text Domain:	elschnet_td
// *******************************


// hier bleiben alle links ohne get ?k=

get_header(); 


// **********************************************
//  menü
// ********************************************** ?>
<header class="content-header"><?php

	// offcanvas-menü button ?>
	<label for="offcanvas-menu" class="toggle-btn">
		<svg width="30" height="30" id="hamburger" viewBox="0 0 24 16">
			<line fill="none" stroke="#FFF" stroke-width="2" x1="0" y1="1" x2="24" y2="1" />
			<line fill="none" stroke="#FFF" stroke-width="2" x1="0" y1="8" x2="24" y2="8" />
			<line fill="none" stroke="#FFF" stroke-width="2" x1="0" y1="15" x2="24" y2="15" />
		</svg>
	</label><?php
	
	// headline ?>
	<span class="logo"></span>
	
</header><?php



// **********************************************
//  inhalt
// ********************************************** ?>
<div class="content-body" id="result"><?php
	
	// headline
	$anz_posts = count($posts);
	echo '<h1>404 - Seite nicht gefunden</h1>'; ?>	
	
	<div class="box"><?php
	
		// hilfreiche links
		echo '<button class="einzeln"><a href="/">zur Startseite</a></button>';
		echo '<button class="einzeln"><a href="/ort/">alle Orte anzeigen</a></button>'; 
		
		// suchformular
		echo get_search_form(); ?>
	 
	</div>
</div><?php



// **********************************************
//  seitenfuß
// ********************************************** ?>
<footer class="content-footer"><?php
 		
	// vorherige/nächste seite (nur mobile)
	get_template_part( 'parts/mobile', 'navigation' ); ?>
	
</footer><?php

get_footer(); ?> 