<?php defined('ABSPATH') or die("No script kiddies please!");
 
// Theme Name:  elsch-ausgaben
// Author:      Michael Schaefer
// Author URI:  https://elsch.net
// Version:     1.0 (2018-12)
// Text Domain:	elschnet_td
// *******************************

?>

<header class="content-header"><?php
	
	// desktop-navigation
	if ( !wp_is_mobile() ) { ?>
		<div class="right"><?php
			$inc = get_template_directory() .'/parts/navigation.php';
			if ( !@include( $inc ) ) { elsch_include( $inc ); } ?>
		</div><?php
	} 
	
	// offcanvas-menü button ?>
	<label for="offcanvas-menu" class="toggle-btn">
		<svg width="30" height="30" id="hamburger" viewBox="0 0 24 16">
			<line fill="none" stroke="#666" stroke-width="2" x1="0" y1="1" x2="24" y2="1" />
			<line fill="none" stroke="#666" stroke-width="2" x1="0" y1="8" x2="24" y2="8" />
			<line fill="none" stroke="#666" stroke-width="2" x1="0" y1="15" x2="24" y2="15" />
		</svg>
	</label><?php
	
	// headline 
	echo $headline;	?>

</header>
