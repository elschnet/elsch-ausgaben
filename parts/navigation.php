<?php defined('ABSPATH') or die("No script kiddies please!");
 
// Theme Name:  elsch-ausgaben
// Author:      Michael Schaefer
// Author URI:  https://elsch.net
// Version:     1.0 (2018-12)
// Text Domain:	elschnet_td
// *******************************

// **********************************************
//  aktuellen user ermitteln
// **********************************************
$current_user = wp_get_current_user();
$user = strtolower( $current_user->user_firstname ); 
if ( $user == 'michael' ) { $nav_aktuell = '?michael'; }
elseif ($user == 'anja' ) { $nav_aktuell = '?anja'; }


$nav = array();

// aktuell
// icon tortendiagramm https://fontawesome.com/icons/chart-pie?style=solid
$nav[1]['svg'] = '<svg id="svgtorte" viewBox="0 0 544 512"><path d="M527.79 288H290.5l158.03 158.03c6.04 6.04 15.98 6.53 22.19.68 38.7-36.46 65.32-85.61 73.13-140.86 1.34-9.46-6.51-17.85-16.06-17.85zm-15.83-64.8C503.72 103.74 408.26 8.28 288.8.04 279.68-.59 272 7.1 272 16.24V240h223.77c9.14 0 16.82-7.68 16.19-16.8zM224 288V50.71c0-9.55-8.39-17.4-17.84-16.06C86.99 51.49-4.1 155.6.14 280.37 4.5 408.51 114.83 513.59 243.03 511.98c50.4-.63 96.97-16.87 135.26-44.03 7.9-5.6 8.42-17.23 1.57-24.08L224 288z"/></svg>';
$nav[1]['text_m'] = 'aktuell';
$nav[1]['text_d'] = 'Aktuell';
$nav[1]['link'] = '/monat-einzeln/'. $nav_aktuell;


// neu
// icon plus-zeichen https://icons8.com/icon/1500/plus
$nav[2]['svg'] = '<svg id="svgplus" viewBox="0 0 26 26"><path d="M13.5,3.188C7.805,3.188,3.188,7.805,3.188,13.5S7.805,23.813,13.5,23.813S23.813,19.195,23.813,13.5 S19.195,3.188,13.5,3.188z M19,15h-4v4h-3v-4H8v-3h4V8h3v4h4V15z"/></svg>';
$nav[2]['text_m'] = '';
$nav[2]['text_d'] = 'Neue Ausgabe';
$nav[2]['link'] = '/';


// auswertung 
// icon balken https://fontawesome.com/icons/stream?style=solid
$nav[3]['svg'] = '<svg id="svgbalken" viewBox="0 0 544 512"><path d="M16 128h416c8.84 0 16-7.16 16-16V48c0-8.84-7.16-16-16-16H16C7.16 32 0 39.16 0 48v64c0 8.84 7.16 16 16 16zm480 80H80c-8.84 0-16 7.16-16 16v64c0 8.84 7.16 16 16 16h416c8.84 0 16-7.16 16-16v-64c0-8.84-7.16-16-16-16zm-64 176H16c-8.84 0-16 7.16-16 16v64c0 8.84 7.16 16 16 16h416c8.84 0 16-7.16 16-16v-64c0-8.84-7.16-16-16-16z"/></svg>';
$nav[3]['text_m'] = 'auswertung';
$nav[3]['text_d'] = 'Auswertung';
$nav[3]['link'] = '/monat-summen/';



// mobile navigation (im footer)
if ( wp_is_mobile() ) { 


	foreach( $nav as $key => $icon ) {
	//foreach( $nav as $icon ) {
		
		$class='';
		if ( $GLOBALS['aktive_nav'] == $key ) { $class = ' class="active"'; }
		echo '<a href="'. $icon['link'] .'"'. $class .'>'. $icon['svg'];
		if ( !empty($icon['text_m']) ) { echo '<br/>'. $icon['text_m']; }
		echo '</a>';
		
		
	}
} 


// desktop navigation (im header)
else {
	
	foreach( $nav as $key => $icon ) {
		
		$class = '';
		if ( $aktive_nav == $key ) { $class = ' class="active"'; }	
		echo '<button'. $class .'><a href="'. $icon['link'] .'">'. $icon['text_d'] .'</a></button>';
		
	}
}

?>
