<?php defined('ABSPATH') or die("No script kiddies please!");
 
// Theme Name:  elsch-ausgaben
// Author:      Michael Schaefer
// Author URI:  https://elsch.net
// Version:     1.0 (2019-02)
// Text Domain:	elschnet_td
// *******************************
if ( !session_id() ) { session_start(); }



// **********************************************
//  aktueller benutzer
// ********************************************** 
$current_user = wp_get_current_user();
$user = strtolower( $current_user->user_firstname ); 



// **********************************************
//  aktuellen user ermitteln
// **********************************************
if ( isset($_GET['michael']) ) {
	$benutzer = 'Michael';
	$other_user = 'anja';
}
elseif ( isset($_GET['anja']) ) {
	$benutzer = 'Anja';
	$other_user = 'michael';
}
else {
	// aufruf ohne username 
	if ( $user == 'michael' ) { header('Location:'. site_url( '/budget/?michael' ) ); }
	elseif ($user == 'anja' ) { header('Location:'. site_url( '/budget/?anja' ) ); }
	else { // falscher user, z.b. admin
		$_SESSION['fehler'] = 'Auswertung nicht möglich. Falscher Benutzername. (36)';
		header('Location:'. site_url('/fehler/') );
		exit;	
	}	
	exit;
}



// **********************************************
//  filter: nur ein bestimmter monat
// **********************************************
if ( isset($_GET['z']) ) {
	
	$filter = $_GET['z'];
	list( $filter_year, $filter_month ) = explode( '-', $_GET['z'] );
	
	if ( strtotime($filter_year)===false || strtotime("$filter_month/01/2019")===false ) { 
		header('Location:'. site_url( '/budget/?'. $user ) );
		exit;
	}
	
	else {
		$filter_text = date_i18n('F Y', strtotime( $filter_month .'/01-'. $filter_year ) ); 
	} 
}



// **********************************************
//  inhalt zusammenstellen
// ********************************************** 
if ( have_posts() ) { 
	$filtermonate = array();
	$out_budget = '';
	
	
	while ( have_posts() ) { the_post();
		
		$budget_month = get_field('b_monat');
		
		// eventuell filter beachten
		if ( isset( $filter_text ) && "$filter_year-$filter_month" != $budget_month ) { 
			 continue;
		}		
		
		// monat
		list( $year, $month ) = explode( '-', $budget_month );
		$monat = date_i18n( 'F Y', strtotime("$month/1/$year" ));
		$filtermonate[] = $budget_month;
				
		// betrag
		$b_betrag = get_field('b_betrag');
		if ( $b_betrag < 0 ) { $betrag = '<strong style="color:tomato">'; }
		else { $betrag = '<strong>'; }
		$betrag .= number_format ( $b_betrag, 2,',','.' ) . ' &euro;</strong>';
				
		// person
		$person = ucfirst( get_field('b_person') ); 
		 
		// icon
		$ausgabeart = array('budget','bargeld');
		$icon = 'i0';
		if ( $person == $benutzer ) { 
			$icon = 'i1'; 
			$ausgabeart[] = 'luxus'; 
		}
				
		// kurzbeschreibung
		if ( !empty( get_field('b_beschreibung') ) ) {
			$monat .= ' (Info)';
			$kurzbeschreibung = get_field('b_beschreibung');
		}
		
		// einzelne budgets auflisten
		$out_budget .= '<div class="ausgabe '. implode(' ', $ausgabeart ) .'">';
			$out_budget .= '<a href="#" data-id="'. get_the_ID() .'">';
			$out_budget .= 'Budget '. $person .' '. $betrag .'<br/>';
			$out_budget .= '<div class="icon '. $icon .'">für '. $monat .'</div></a>';
		$out_budget .= '</div>';
		
	} // while have_posts()
} //if have_posts()



// **********************************************
//  seitenkopf
// **********************************************
$headline = '<h1>Festgelegte Budgets</h1>';
$GLOBALS['aktive_nav'] = 0;
get_header(); 

$inc = get_template_directory() .'/parts/header.php';
if ( !@include( $inc ) ) { elsch_include( $inc ); } 


// **********************************************
//  inhalt ausgeben
// ********************************************** ?>
<div class="content-body"><?php

	// link zu budget-auslastung
	if ( $filter_year ) {	$date = "$filter_year"; } else { $date = date( 'Y'); } 
	echo '<a id="nav2" href="/budget-auslastung/?'. strtolower($benutzer) .'&z='. $date .'">Budget-Auslastung</a>';
	echo '<div class="clear"></div>';

	// filtertext
	if ( isset( $filter_text ) ) {
		echo '<div class="legende filter">Filter: nur Budgets für '. $filter_text;
		echo '<em><a href="/budget/?'. strtolower($benutzer) .'">&nbsp;</a></em></div>'; 
	}	
	
	// filtermöglichkeit: einzelne monate
	else {
		$filtermonate = array_unique( $filtermonate ); 
		rsort( $filtermonate ); 
		echo '<div class="legende filter" id="filter">Budgets filtern</div>';
		echo '<div id="filtermonate" style="display:none">';
			foreach ( $filtermonate as $filtermonat ) {
				$f = explode( '-', $filtermonat );
				echo '<div class="legende dropdown"><a href="/budget/?'. strtolower($benutzer) .'&z='. $filtermonat .'">'. date_i18n('F Y', strtotime( $f[1] .'/1/'. $f[0] ) ) .'</a></div>';
			}		
		echo '</div>';
	}
	
	// einzelne budgets ausgeben
	if ( $out_budget ) {
		echo $out_budget;
	} 
	
	// keine einträge gefunden
	else {
		echo '<br/><div class="ausgabe">';
			echo 'kein Budget gefunden';
		echo '</div><br/>';		
	}	
	
	
	// neues budget eingeben
	echo '<a id="newbudget" href="/wp-admin/post-new.php?post_type=budget">neues Budget</a>';
	
	
	// user wechseln
	echo '<a id="user" href="/budget/?'. $other_user. '">Person wechseln</a>';
	?>
	
</div>
<?php


// **********************************************
//  javascript im footer laden
// ********************************************** 
function budget_script() { 

	echo '<script>';
	echo '$(document).ready(function() {';
	
		echo '$( "#filter" ).click(function() {';
			echo '$("#filtermonate").fadeToggle("slow");';
		echo '});';
	
	echo '});';
	echo '</script>';

}
add_action( 'wp_footer', 'budget_script', 100 );
	

get_footer(); ?> 