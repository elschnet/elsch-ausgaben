<?php defined('ABSPATH') or die("No script kiddies please!");
 
// Theme Name:  elsch-ausgaben
// Author:      Michael Schaefer
// Author URI:  https://elsch.net
// Version:     1.0 (2019-01)
// Text Domain:	elschnet_td
// Template Name: Suche
// *******************************
if ( !session_id() ) { session_start(); }



// **********************************************
//  suchformular
// ********************************************** 
$suchform = '<div id="suchform"><form id="searchform" action="/" method="post">';
	$suchform .= '<input type="text" name="s" value="" />';
	$suchform .= '<input type="hidden" name="post_type" value="ausgabe" />';
	$suchform .= '<input type="submit" value="Suche" />';
$suchform .= '</form>';
$suchform .= 'beträge bitte mit punkt eingeben<br/>';
$suchform .= '<br/><br/></div>';

//elschtodo: filtermöglichkeiten: einschränken nach datumsbereich, kategorie(n), ausgabearte(n)
//ein ansatz: https://wordpress.stackexchange.com/q/199539



// **********************************************
//  aktueller benutzer
// ********************************************** 
$current_user = wp_get_current_user();
$user = strtolower( $current_user->user_firstname ); 
if ( $user == 'michael' ) {	$luxus = 'luxus-michael'; }
elseif ( $user == 'anja' ) { $luxus = 'luxus-anja'; }


// variablen erstellen
$out_ausgaben = ''; // einzelne ausgaben auflisten



// **********************************************
//  inhalte: suchergebnisse oder suchseite
// **********************************************
if ( have_posts() ) { 


	// suchergebnisse durchlaufen
	if ( isset( $_POST['s'] ) ) { 
		$title = 'Suchergebnis';		
		
		while ( have_posts() ) { 
			the_post();					
		
			$betrag = get_the_title();
			$beschreibung = wp_strip_all_tags( get_the_content() );
						
				
			// kategorie
			$kat = get_the_terms( $post->ID, 'kategorie' );
			$kategorie = $kat[0]->name;
			
			
			// ausgabearten
			$ausgabeart = array();
			$aus = get_the_terms( $post->ID, 'ausgabeart' );
			foreach ( $aus as $aa ) {
				$ausgabeart[] = $aa->slug;
			}
			
			
			// währung
			$waehrung = get_the_terms( $post->ID, 'waehrung' );
			$waehrung_id = $waehrung[0]->term_id;	
			// umrechnung fremdwährung
			if ( $waehrung_id != 37 ) { // euro=37
				$ausgabeart[] = 'waehrung';

				$umrechnungskurs = get_field('a_umrechnung');
				$kurs = str_replace(",", ".", $umrechnungskurs);
				$umrechnungskurs = str_replace(".", ",", $umrechnungskurs);
				$betrag = $betrag * $kurs;
			}
			
			
			// kurzbeschreibung
			if ( strlen($beschreibung) > 31 ) { $kurzbeschreibung = mb_substr($beschreibung, 0, 30) .'&hellip;'; }
			elseif ( empty($beschreibung) ) { $kurzbeschreibung = ' &mdash;'; }
			else { $kurzbeschreibung = $beschreibung; }
					
			
			// betrag runden auf 2 nachkommastellen
			$betrag = round( $betrag, 2 );
			
			
			// sonderfall: womo-diesel
			if ( $kat[0]->term_id == 22 ) {
				$a_liter = get_field('a_liter');
				$a_kilometerstand = get_field('a_kilometerstand');
				$a_betragproliter = get_field('a_betragproliter');
							
				$betragproliter = number_format( $a_betragproliter, 3,',','.' );
				$bpl  = substr( $betragproliter, 0, -1 );
				$bpl .= '<sup>'. substr( $betragproliter, -1 ) .'</sup>';
				$liter = number_format( $a_liter, 2,',','.' );	
				$kilometerstand = number_format( $a_kilometerstand, 0,',','.' );
				$kurzbeschreibung = "$bpl &euro;/&#8467; &mdash; $liter &#8467; &mdash; $kilometerstand km";
			
				// nachrechnen, ob die werte stimmen können
				// mehr als 2 cent plus/minus unterschied
				$rechenwert = round($a_liter * $a_betragproliter, 2);
				if ( ( $rechenwert - 0.02 ) > $betrag || ( $rechenwert + 0.02 ) < $betrag ) {	
					$ausgabeart[] = 'dieselgate';
					$kategorie = '** WERTE PRÜFEN! **';
				}
			}

						
			// icons
			if ( in_array('leben', $ausgabeart) ) {
				$icon = 'il';
			}			
			elseif ( in_array('gemeinsam', $ausgabeart) ) {
				$icon = 'ig';
			}			
			elseif ( in_array('elschnet', $ausgabeart) ) {
				$icon = 'ie';
			}			
			elseif ( in_array('fixkosten', $ausgabeart) ) {
				$icon = 'if';
			}			
			elseif ( in_array($luxus, $ausgabeart) ) {
				$ausgabeart[] = 'luxus';
				$icon = 'i1';
			}			
			else {
				$icon = 'i0';
			}
			
			
			// einzelne ausgaben auflisten
			$out_ausgaben .= '<div class="ausgabe '. implode(' ', $ausgabeart ) .'">';
				$out_ausgaben .= '<a href="'. get_the_permalink() .'">';
				$out_ausgaben .= get_post_time('d.m.') .' '. $kategorie .'<strong>';
				$out_ausgaben .= number_format ( $betrag, 2,',','.' ) . ' &euro;</strong><br/>';
				$out_ausgaben .= '<div class="icon '. $icon .'">'. $kurzbeschreibung .'</div></a>';
			$out_ausgaben .= '</div>';
			
		} 		
	}
		
		
	// suchseite
	// suche noch nicht abgeschickt
	else {		
		$title = 'Suche';		
	}	
}


// **********************************************
//  keine suchergebnisse 
// **********************************************
else {
	
	$title = 'kein Suchergebnis';	
	
	$out_ausgaben = '<div class="ausgabe">';
		$out_ausgaben .= '<div class="icon">Keine Ausgaben gefunden</div></a>';
	$out_ausgaben .= '</div>';	
}


// **********************************************
//  ausgabe
// **********************************************
$headline = '<h1>'. $title .'</h1>';
$GLOBALS['aktive_nav'] = 0;
get_header(); 

$inc = get_template_directory() .'/parts/header.php';
if ( !@include( $inc ) ) { elsch_include( $inc ); } 
?>


<div class="content-body">
	<?php 
	echo $suchform;
	echo $out_ausgaben; 
	?>
	<div class="clear"></div>
</div>


<?php get_footer(); ?> 