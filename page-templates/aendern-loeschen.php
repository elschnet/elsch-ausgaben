<?php defined('ABSPATH') or die("No script kiddies please!");
 
// Theme Name:  elsch-ausgaben
// Author:      Michael Schaefer
// Author URI:  https://elsch.net
// Version:     1.0 (2018-12)
// Text Domain:	elschnet_td
// Template Name: aendern-loeschen
// *******************************


// **********************************************
//  session starten
// ********************************************** 
if ( !session_id() ) { session_start(); }



// **********************************************
//  post-id prüfen
// **********************************************
if ( !isset( $_GET['edit'] ) || !is_numeric( $_GET['edit'] ) ) {		
	// keine (numerische) id übergeben
	$_SESSION['fehler'] = 'keine ID übergeben (Code: 24)';
}
elseif ( get_post_type( $_GET['edit'] ) != 'ausgabe' ) {
	// id ist keine ausgabe
	$_SESSION['fehler'] = 'keine Ausgabe gefunden (Code: 28)';
}
elseif ( !current_user_can( 'edit_post', $_GET['edit'] )) {
	// user nicht berechtigt
	$_SESSION['fehler'] = 'Du darfst nicht ändern (Code: 32)';
}
else {	
	// alles okay
	$post = get_post( $_GET['edit'] );
	setup_postdata( $post );
}



// **********************************************
// es gab fehler: abbrechen
// **********************************************
if ( isset( $_SESSION['fehler'] ) ) {
	header( 'Location:'. site_url('/fehler/') );
	exit;		
}



// **********************************************
//  es soll gelöscht werden
// **********************************************
if ( isset( $_GET['delete']) ) { // rückfrage "wirklich löschen" stellen
	$form2 = true;
}



// **********************************************
//  löschen durchführen
// **********************************************
elseif ( isset( $_GET['del_ausgabe']) ) {	// löschen durchführen, rückfrage wurde bestätigt
	
	// nonce prüfen
	if ( !wp_verify_nonce( $_GET['del_ausgabe'], 'deletenow' ) ) { 
		$_SESSION['fehler'] = 'Failed security check (Code: 68)'; 
	} 
	
	if ( !isset( $_GET['edit'] ) || !is_numeric( $_GET['edit'] ) ) {		
		// keine (numerische) id übergeben
		$_SESSION['fehler'] = 'keine ID übergeben (Code: 73)';
	} 	
		
	if ( get_post_type( $_GET['edit'] ) != 'ausgabe' ) {		
		// id ist keine ausgabe
		$_SESSION['fehler'] = 'ID ist fehlerhaft (Code: 78)';
	} 	
	
	// es gab fehler: abbrechen
	if ( isset( $_SESSION['fehler'] ) ) {
		header( 'Location:'. site_url('/fehler/') );
		exit;		
	}
	
	
	// **********************************************	
	// ausgabe in den papierkorb verschieben
	wp_trash_post( $_GET['edit'] );
	
	
	// **********************************************
	// alles erledigt: umleitung zur seite /monat-einzeln/
	header( 'Location:'. site_url( '/monat-einzeln/' ) );
	exit;
}



// **********************************************
//  bereits geänderte daten übernehmen
// **********************************************
elseif ( !empty($_POST) ) { // geänderte daten übernehmen und speichern
	// übernehmen und absichern der empfangenen daten
	// vieles übernimmt wp_insert_post
	$a_post_id = get_the_ID();
	
	
	// nonce prüfen
	if ( !wp_verify_nonce( $_POST['_ononce'], 'edit_ausgabe' ) ) { 
		$_SESSION['fehler'] = 'Failed security check (Code: 88)'; 
	} 
	
	
	// datum: kommt wie 2018-12-30, als Y-m-d
	// wp erwartet diese struktur [ Y-m-d H:i:s ]
	// datum auf gültigkeit prüfen und aktuelle uhrzeit dazu!
	$date = explode( '-', $_POST['a_datum'] );
	if( !checkdate($date[1],$date[2],$date[0]) ) { $_SESSION['fehler'] = 'Datum fehlerhaft. (Code: 96)'; }	
	$a_datum = $_POST['a_datum'] .' '. current_time('H:i:s'); 
	
		
	// betrag: kommt als 12*50, oder 12,50 oder 12#50 oder 12.50 oder ...
	// betrag als zahl, also mit punkt, speichern!	
	$a_betrag = $_POST['a_betrag'];
	$suchen = array('*','#',',',';','+'); // suchen und ersetzen
	$a_title = str_replace( $suchen, '.', $a_betrag );
	$a_title = number_format($a_title, 2, '.', ''); // 2 nachkommastellen
	if ( !is_numeric($a_title) ) { $_SESSION['fehler'] = 'Betrag fehlerhaft. (Code: 106)'; }
	
	
	// waehrung
	$a_waehrung = intval($_POST['a_waehrung']);
	$waehrungen = get_terms( 'waehrung', array( 'fields' => 'ids', 'hide_empty' => 0 ) );
	if( !in_array($a_waehrung, $waehrungen) ) { $_SESSION['fehler'] = 'Betrag fehlerhaft. (Code: 112)'; }
	

	// umrechnung als zahl, also mit punkt, speichern!	
	$a_umrechnung = $_POST['a_umrechnung'];
	$suchen = array('*','#',',',';','+'); // suchen und ersetzen
	$a_umrechnung = str_replace( $suchen, '.', $a_umrechnung );
	$a_umrechnung = number_format($a_umrechnung, 4, '.', ''); // 4 nachkommastellen
	if ( !is_numeric($a_umrechnung) ) { $_SESSION['fehler'] = 'Umrechnung fehlerhaft. (Code: 121)'; }
	

	// ausgabeart: anja, michael, beide, leben, luxus, fixkosten, bar, konto
	$a_budgetarten = intval($_POST['a_budgetarten']);
	$a_zahlarten = intval($_POST['a_zahlarten']);
	
	$ausgabearten = get_terms( 'ausgabeart', array( 'fields' => 'ids', 'hide_empty' => 0 ) );
	if( !in_array($a_budgetarten, $ausgabearten) ) { $_SESSION['fehler'] = 'Budget fehlerhaft. (Code: 129)'; }
	if( !in_array($a_zahlarten, $ausgabearten) ) { $_SESSION['fehler'] = 'Budget fehlerhaft. (Code: 130)'; }
	
	$ausgabeart = array( $a_budgetarten, $a_zahlarten );
		
	
	// beschreibung
	$a_beschreibung = wp_strip_all_tags( $_POST['a_beschreibung'] );	
	
	
	// kategorie: ...
	// prüfen, ob die id einer bestehenden kategorie übergeben wurde
	$kategorie = intval($_POST['a_kat']);	
	$kategorien = get_terms( 'kategorie', array( 'fields' => 'ids', 'hide_empty' => 0 ) );
	if( !in_array($kategorie, $kategorien) ) { $_SESSION['fehler'] = 'Kategorie fehlerhaft. (Code: 143)'; /*unset($kategorie);*/ }	

		
	// sonderfall: womo-diesel
	if ( $kategorie == 22 ) {
	
		$a_kilometerstand = intval($_POST['a_kilometerstand']);
		if ( !empty($a_kilometerstand) && !is_numeric($a_kilometerstand) ) { $_SESSION['fehler'] = 'Kilometerstand fehlerhaft. (Code: 150)'; }
		
		$a_betragproliter = $_POST['a_betragproliter'];
		$suchen = array('*','#',',',';','+'); // suchen und ersetzen
		$a_betragproliter = str_replace( $suchen, '.', $a_betragproliter );
		$a_betragproliter = number_format($a_betragproliter, 3, '.', ''); // 3 nachkommastellen
		if ( !empty($a_betragproliter) && !is_numeric($a_betragproliter) ) { $_SESSION['fehler'] = 'Betrag/Liter fehlerhaft. (Code: 156)'; }
			
		$a_liter = $_POST['a_liter'];
		$suchen = array('*','#',',',';','+'); // suchen und ersetzen
		$a_liter = str_replace( $suchen, '.', $a_liter );
		$a_liter = number_format($a_liter, 2, '.', ''); // 2 nachkommastellen
		if ( !empty($a_liter) && !is_numeric($a_liter) ) { $_SESSION['fehler'] = 'Liter fehlerhaft. (Code: 162)'; }

		$a_land = wp_strip_all_tags( $_POST['a_land'] );	

	} else {
		
		// kategorie ist nicht (mehr) "diesel womo"
		// also löschen wir eventuell vorhandene werte		
		$a_kilometerstand = '';
		$a_betragproliter = '';
		$a_liter = '';
		$a_land = ''; 
	}
	
	

	
	// **********************************************
	// es gab fehler: nicht speichern, fehler anzeigen!
	if ( isset( $_SESSION['fehler'] ) ) {
		header( 'Location:'. site_url('/fehler/') );
		exit;		
	}
	 
	
	 
	// **********************************************
	// ausgabe ändern
	
	
	$neue_ausgabe = array(
		'ID'			=> $a_post_id,
		'post_type'		=> 'ausgabe',
		'post_title'	=> $a_title,
		'post_date'     => $a_datum,
		'post_content'	=> $a_beschreibung,
		'post_status'	=> 'publish'		
	);	
	wp_update_post( $neue_ausgabe );
	
	
	// metadaten zum post hinzufügen
	#update_post_meta( $a_post_id, 'a_betrag', $a_betrag, true );
	#update_post_meta( $a_post_id, 'a_datum', $a_datum, true );
	update_post_meta( $a_post_id, 'a_umrechnung', $a_umrechnung );
	update_post_meta( $a_post_id, 'a_kilometerstand', $a_kilometerstand );
	update_post_meta( $a_post_id, 'a_betragproliter', $a_betragproliter );
	update_post_meta( $a_post_id, 'a_liter', $a_liter );
	update_post_meta( $a_post_id, 'a_land', $a_land );
	
	// ausgabeart und kategorie speichern
	wp_set_object_terms( $a_post_id, $ausgabeart, 'ausgabeart' );
	wp_set_object_terms( $a_post_id, $kategorie, 'kategorie' );
	
	// waehrung speichern
	wp_set_object_terms( $a_post_id, $a_waehrung, 'waehrung' );
	
	
	
	// **********************************************
	// alles erledigt: umleitung zur seite /monat-einzeln/
	header( 'Location:'. get_the_permalink( $a_post_id ) );
	exit;	
	
	
	/*
	echo "ID: $a_post_id<br/>";
	echo "a_datum: $a_datum<br/>";
	echo "a_title: $a_title<br/>";
	echo "a_waehrung: $a_waehrung<br/>";
	echo "a_umrechnung: $a_umrechnung<br/>";
	echo "ausgabeart: ". implode(", ", $ausgabeart) .'<br/>';
	echo "kategorie: $kategorie<br/>";
	echo "a_kilometerstand: $a_kilometerstand<br/>";
	echo "a_liter: $a_liter<br/>";
	echo "a_betragproliter: $a_betragproliter<br/>";
	echo "a_land: $a_land<br/><br/>";
	echo "a_beschreibung: $a_beschreibung<br/>";	
	die('geändert');
	*/
}



// **********************************************
//  formular anzeigen
// **********************************************
else { // formular1: daten holen und füpr anzeige aufbereiten
	$form1 = true;

	
	// währung, umrechnungskurs
	$waehrung = get_the_terms( $post->ID, 'waehrung' );
	$waehrungen = waehrung_options( $waehrung[0]->term_id );
	$umrechnungskurs = number_format( get_field('a_umrechnung'), 4,',','.' );
	if ( $waehrung[0]->term_id != 37 ) { // euro=37
		$betragstext = 'Originalbetrag (Umrechnung erfolgt automatisch): *';
		$fremdwaehrung = true;
	} else { $betragstext = 'Betrag: *'; }

		
	// zahlart, budget
	$ausgabeart = array();
	$aa = get_the_terms( $post->ID, 'ausgabeart' );
	foreach ( $aa as $a ) { $ausgabeart[] = $a->term_id; } 
	$zahlarten = ausgabeart_options( 'zahlart', $ausgabeart ) ;
	$budgetarten = ausgabeart_options( 'budget', $ausgabeart ) ;


	// beschreibung
	$beschreibung = wp_strip_all_tags( get_the_content() );
		
	// kategorie
	$kategorie = get_the_terms( $post->ID, 'kategorie' );
	$kategorien = kategorie_options( $kategorie[0]->term_id, false, true );

}





// **********************************************
//  seitenkopf
// **********************************************
$headline = '<h1>Einzelne Ausgabe <span style="color:tomato">ändern</span></h1>';
$GLOBALS['aktive_nav'] = 0;
get_header(); 

$inc = get_template_directory() .'/parts/header.php';
if ( !@include( $inc ) ) { elsch_include( $inc ); } 



// **********************************************
//  inhalt
// ********************************************** 
echo '<div class="content-body single">'; 
	
	if ( $form1 ) { // formular1: single daten anzeigen 
		echo '<form action="'. $_SERVER['REQUEST_URI'] .'" method="post" id="ausgabe">';
			
			// zurück
			echo '<button class="abbrechen"><a href="'. get_the_permalink() .'?stop">abbrechen</a></button><br/><br/><br/>';
			
			// datum
			echo '<div class="field">Datum: *<input class="edit" type="date" id="a_datum" name="a_datum" value="'. get_the_date('Y-m-d') .'" required /></div>';
			
			// betrag
			echo '<div class="field">'. $betragstext .'<input type="tel" name="a_betrag" value="'. number_format( get_the_title(), 2,',','.' ) .'" required /></div>';

			// währung
			echo '<div class="field">Währung: *<span class="elschselect"><select id="a_waehrung" name="a_waehrung">'. $waehrungen .'</select></span></div>';
			
			// sonderfall: umrechnungskurs
			echo '<div id="umrechnung"'; if ( !$fremdwaehrung ) { echo ' style="display:none;"'; } echo '>';
				echo '<div class="field">Umrechnungskurs: *<input name="a_umrechnung" type="text" value="'. $umrechnungskurs .'" /></div>';
			echo '</div>';	
				
			// zahlart
			echo '<div class="field">Zahlart: *<span class="elschselect"><select name="a_zahlarten">'. $zahlarten .'</select></span></div>';
			
			// budget
			echo '<div class="field">Budget: *<span class="elschselect"><select name="a_budgetarten">'. $budgetarten .'</select></span></div>';
			
			// kategorie
			echo '<div class="field">Kategorie: *<span class="elschselect"><select name="a_kat" id="a_kat" required>'. $kategorien .'</select></span></div>'; 
					
			// sonderfall: womo-diesel
			echo '<div id="a_diesel"'; if ( $kategorie[0]->term_id != 22 ) { echo ' style="display:none;"'; } echo '>';
				echo '<div class="field">Kilometerstand: <input type="tel" name="a_kilometerstand" value="'. get_field('a_kilometerstand') .'" /></div>';
				echo '<div class="field">Liter: <input type="tel" name="a_liter" value="'. number_format( get_field('a_liter'), 2,',','.' ) .'" /></div>';
				echo '<div class="field">Betrag/Liter: <input type="tel" name="a_betragproliter" value="'. number_format( get_field('a_betragproliter'), 3,',','.' ) .'" /></div>';
				echo '<div class="field">getankt in: <span class="elschselect land"><select name="a_land">'. laender_options( get_field('a_land') ) .'</select></span></div>';
			echo '</div>';	
			
			// kurzbeschreibung grau
			echo '<div class="field">Kurzbeschreibung: <textarea name="a_beschreibung">'. $beschreibung .'</textarea></label></div>';

			// absicherung
			echo wp_nonce_field("edit_ausgabe", "_ononce", TRUE, FALSE );
			
			echo '<br/><button id="delete"><a href="'. $_SERVER['REQUEST_URI'] .'&delete">löschen</a></button>';
			echo '<button type="submit" id="submit">ändern</button><br/>&nbsp;';

		echo '</form>';	
	}

	elseif ( $form2 ) { // rückfrage "wirklich löschen" stellen
		
		$waehrung = get_the_terms( $post->ID, 'waehrung' );
		echo '<div class="frage" id="ausgabe">Die Ausgabe über '. number_format( get_the_title(), 2,',','.' ) .' ';
		echo $waehrung[0]->name .' vom '. get_the_date('d.m.Y') .' wirklich löschen?';
		
		echo '<br/><br/><br/><button id="delete"><a href="'. wp_nonce_url('/ausgabe-aendern/?edit='. get_the_ID(), 'deletenow', 'del_ausgabe' ) .'">ja, löschen</a></button>';
		echo '<button class="abbrechen"><a href="javascript:window.history.back()">abbrechen</a></button></div>';

	}
	
echo '</div>';

// **********************************************
//  javascript
// **********************************************
function aendern() { 
	// bei änderung auf kategorie "diesel womo", die zusatzfelder einblenden
	// bei änderung der währung auf nicht-"euro", das feld umrechnung einblenden
	echo '<script>';
	echo '$(document).ready(function() {';
	
		// währung: umrechnung anzeigen
		echo '$( "#a_waehrung" ).change(function() {';
			echo 'var wid = document.getElementById("a_waehrung").value;';
			echo 'if( wid!=37 ) {';
				echo '$("#umrechnung").fadeIn("slow");'; 
			echo '} else {';
				echo '$("#umrechnung").fadeOut("slow");'; 
		echo '}});';	
				
		// womo-diesel: weitere felder einblenden
		echo '$( "#a_kat" ).change(function() {';
			echo 'var kid = document.getElementById("a_kat").value;';
			echo 'if( kid==22 ) {';
				echo '$("#a_diesel").fadeIn("slow");'; 
			echo '} else {';
				echo '$("#a_diesel").fadeOut("slow");'; 
		echo '}});';	

	echo '});';
	echo '</script>';
}

add_action( 'wp_footer', 'aendern', 100 );

get_footer(); ?> 