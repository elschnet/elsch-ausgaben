<?php defined('ABSPATH') or die("No script kiddies please!");
 
// Theme Name:  elsch-ausgaben
// Author:      Michael Schaefer
// Author URI:  https://elsch.net
// Version:     1.0 (2018-12)
// Text Domain:	elschnet_td
// Template Name: neue Ausgabe
// *******************************
// datei ist für neueintragung von 
// ausgaben zuständig

if ( !session_id() ) { session_start(); }


// **********************************************
//  aktueller benutzer
// ********************************************** 
$current_user = wp_get_current_user();
$user = strtolower( $current_user->user_firstname ); 



// **********************************************
//  bereits gesendete daten verarbeiten
// ********************************************** 
if ( !empty($_POST) ) {
	// übernehmen und absichern der empfangenen daten
	// vieles übernimmt wp_insert_post
	
	
	// nonce prüfen
	if ( !wp_verify_nonce( $_POST['_ononce'], 'neue_ausgabe' ) ) { 
		$_SESSION['fehler'] = 'Failed security check (Code: 76)'; 
	} 

	
	// betrag: kommt als 12*50, oder 12,50 oder 12#50 oder 12.50 oder ...
	// betrag als zahl, also mit punkt, speichern!	
	$a_betrag = $_POST['a_betrag'];
	$suchen = array('*','#',',',';','+'); // suchen und ersetzen
	$a_title = str_replace( $suchen, '.', $a_betrag );
	$a_title = number_format($a_title, 2, '.', ''); // 2 nachkommastellen
	if ( !is_numeric($a_title) ) { $_SESSION['fehler'] = 'Betrag fehlerhaft. (Code: 86)'; }
	
	
	// waehrung
	$waehrung = intval($_POST['a_waehrung']);
	$waehrungen = get_terms( 'waehrung', array( 'fields' => 'ids', 'hide_empty' => 0 ) );
	if( !in_array($waehrung, $waehrungen) ) { $_SESSION['fehler'] = 'Betrag fehlerhaft. (Code: 92)'; }
	
	
	// beschreibung
	$a_beschreibung = wp_strip_all_tags( $_POST['a_beschreibung'] );	
	
	
	// datum: kommt wie 2018-12-30, als Y-m-d
	// wp erwartet diese struktur [ Y-m-d H:i:s ]
	// datum auf gültigkeit prüfen und aktuelle uhrzeit dazu!
	$date = explode( '-', $_POST['a_datum'] );
	if( !checkdate($date[1],$date[2],$date[0]) ) { $_SESSION['fehler'] = 'Datum fehlerhaft. (Code: 103)'; }	
	$a_datum = $_POST['a_datum'] .' '. current_time('H:i:s'); 
	
	
	// kategorie: ...
	// prüfen, ob die id einer bestehenden kategorie übergeben wurde
	$kategorie = intval($_POST['a_kat']);	
	$kategorien = get_terms( 'kategorie', array( 'fields' => 'ids', 'hide_empty' => 0 ) );
	if( !in_array($kategorie, $kategorien) ) { $_SESSION['fehler'] = 'Kategorie fehlerhaft. (Code: 110)'; }	

	
	// ausgabeart: anja, michael, beide, leben, luxus, bar, konto
	$b_budget = intval($_POST['b_budget']);
	$b_zahlart = intval($_POST['b_zahlart']);
	
	$ausgabearten = get_terms( 'ausgabeart', array( 'fields' => 'ids', 'hide_empty' => 0 ) );
	if( !in_array($b_budget, $ausgabearten) ) { $_SESSION['fehler'] = 'Budget fehlerhaft. (Code: 119)'; }
	if( !in_array($b_zahlart, $ausgabearten) ) { $_SESSION['fehler'] = 'Zahlart fehlerhaft. (Code: 120)'; }
	
	$ausgabeart = array( $b_budget, $b_zahlart );

	
	// sonderfall: womo-diesel
	if ( $_POST['a_kat'] == 22 ) {
	
		$a_kilometerstand = intval($_POST['a_kilometerstand']);
		if ( !empty($a_kilometerstand) && !is_numeric($a_kilometerstand) ) { $_SESSION['fehler'] = 'Kilometerstand fehlerhaft. (Code: 128)'; }
		
		$a_betragproliter = $_POST['a_betragproliter'];
		$suchen = array('*','#',',',';','+'); // suchen und ersetzen
		$a_betragproliter = str_replace( $suchen, '.', $a_betragproliter );
		$a_betragproliter = number_format($a_betragproliter, 3, '.', ''); // 3 nachkommastellen
		if ( !empty($a_betragproliter) && !is_numeric($a_betragproliter) ) { $_SESSION['fehler'] = 'Betrag/Liter fehlerhaft. (Code: 134)'; }
			
		$a_liter = $_POST['a_liter'];
		$suchen = array('*','#',',',';','+'); // suchen und ersetzen
		$a_liter = str_replace( $suchen, '.', $a_liter );
		$a_liter = number_format($a_liter, 2, '.', ''); // 2 nachkommastellen
		if ( !empty($a_liter) && !is_numeric($a_liter) ) { $_SESSION['fehler'] = 'Liter fehlerhaft. (Code: 140)'; }

		$a_land = wp_strip_all_tags( $_POST['a_land'] );	

	}
	
	
	// **********************************************
	// es gab fehler: nicht speichern, fehler anzeigen!
	if ( isset( $_SESSION['fehler'] ) ) {
		header( 'Location:'. site_url('/fehler/') );
		exit;		
	}
	 
	
	 
	// **********************************************
	// ausgabe speichern
	$neue_ausgabe = array(
		'post_type'		=> 'ausgabe',
		'post_title'	=> $a_title,
		'post_date'     => $a_datum,
		'post_content'	=> $a_beschreibung,
		'post_status'	=> 'publish'		
	);	
	$a_post_id = wp_insert_post( $neue_ausgabe );
	
	// metadaten zum post hinzufügen
	#add_post_meta( $a_post_id, 'a_betrag', $a_betrag, true );
	#add_post_meta( $a_post_id, 'a_datum', $a_datum, true );
	add_post_meta( $a_post_id, 'a_umrechnung', 1, true );	
	add_post_meta( $a_post_id, 'a_eingetragen', date('c'), true );
	
	// ausgabeart und kategorie speichern
	wp_set_object_terms( $a_post_id, $ausgabeart, 'ausgabeart' );
	wp_set_object_terms( $a_post_id, $kategorie, 'kategorie' );
	
	// waehrung speichern
	wp_set_object_terms( $a_post_id, $waehrung, 'waehrung' );
	
	// sonderfall: womo-diesel 
	if ( $kategorie == 22 ) {
		add_post_meta( $a_post_id, 'a_kilometerstand', $a_kilometerstand, true );
		add_post_meta( $a_post_id, 'a_betragproliter', $a_betragproliter, true );
		add_post_meta( $a_post_id, 'a_liter', $a_liter, true );
		add_post_meta( $a_post_id, 'a_land', $a_land, true );
	}
	
	
	// **********************************************
	// alles erledigt: umleitung zur seite /monat-einzeln/
	header( 'Location:'. site_url( '/monat-einzeln/?'. $user ) );
	exit;	
}

	
	
// **********************************************
//  formular anzeigen
// **********************************************
else {


	// **********************************************
	// sonderfall: fixkosten
	if ( isset($_GET['fixkosten']) ) {		
		$fixkosten = true;
	}

	
	get_header(); 

	// **********************************************
	//  seitenkopf
	// **********************************************
	$headline = '<h1>Neue Ausgabe</h1>';
	if ( $fixkosten ) { $headline = '<h1>Neue Fixkosten-Ausgabe</h1>'; }
	$GLOBALS['aktive_nav'] = 2;
	$inc = get_template_directory() .'/parts/header.php';
	if ( !@include( $inc ) ) { elsch_include( $inc ); } 


	// **********************************************
	//  inhalt
	// ********************************************** ?>
	<div class="content-body"><?php
		

		// **********************************************
		// auswahlbox: waehrungen 
		$waehrungen = waehrung_options();
		

		// **********************************************
		// auswahlbox: kategorien 
		// bei fixkosten erscheinen anderen kategorien und andere kategorien oben
		if ( $fixkosten ) { $kategorien = kategorie_options( false, true ); }
		else { $kategorien = kategorie_options(); }
		

		// **********************************************
		// feld: budget/ausgabearten
		$b_zahlart = 'bar'; // standards = aktueller user + bargeld
		
		// inhalt textfeld
		if ( $user == 'michael' ) { $budgettext = 'Luxus Michael, '; }
		else { $budgettext = 'Luxus Anja, '; }

		if ( $b_zahlart == 'bar' ) { $budgettext .= 'Bargeld'; }
		else { $budgettext .= 'Karte/Konto'; }
		
		
		
		// **********************************************
		// sonderfall: fixkosten
		if ( $fixkosten ) {
			$budgettext = 'Fixkosten, Karte/Konto';
			$b_zahlart = 'karte';
			unset( $user );
		}
		
		

		// **********************************************
		// formular ?>
		<form action="<?php echo $_SERVER['REQUEST_URI'] ?>" method="post" id="ausgabe">
			<?php
						
			// feld: betrag
			echo '<input type="tel" id="a_betrag" name="a_betrag" value="" placeholder="Betrag *" required>';
			echo '<input type="text" id="a_waehrung" value="Euro" readonly><br/>';
			
			// bei klick auf euro
			echo '<div id="a_waehrungen" style="display:none;">';			
				echo '<span class="elschselect"><select name="a_waehrung">'. $waehrungen .'</select></span>';
			echo '</div>';
			
			// auswahlbox: kategorie
			echo '<span class="elschselect"><select name="a_kat" id="a_kat" required>'. $kategorien .'</select></span>'; 
						
			// sonderfall womo diesel ?>
			<div id="a_diesel" style="display:none;"><?php

				// feld: kilometerstand
				echo '<label class="labelleft" for="a_kilometerstand">Kilometerstand</label>';
				echo '<input class="labelleft" type="tel" name="a_kilometerstand" value="" placeholder="Kilometerstand"><br/>';
				
				// feld: betrag/liter
				echo '<label class="labelleft" for="a_betragproliter">Betrag/Liter</label>';
				echo '<input class="labelleft" type="tel" name="a_betragproliter" value="" placeholder="Betrag/Liter"><br/>';
								
				// feld: liter
				echo '<label class="labelleft" for="a_liter">Liter</label>';
				echo '<input class="labelleft" type="tel" name="a_liter" value="" placeholder="Liter"><br/>';
												
				// feld: land
				echo '<label class="labelleft" for="a_land">getankt in</label>';
				echo '<span class="elschselect land"><select name="a_land">'. laender_options() .'</select></span>';
				
			?>
			</div>
			<?php
			
			// feld: kurzbeschreibung
			echo '<textarea placeholder="Kurzbeschreibung (max. 200 Zeichen)" rows="5" cols="50" maxlength="200" name="a_beschreibung"></textarea>'; 
						
			// feld: ausgabearten 
			echo '<div id="a_budget">Budget: '. $budgettext .'</div>';
			
			// sonderfall: elschnet
			echo '<div id="a_elschnet" style="display:none">Budget: elschnet, Karte/Konto</div>';
						
			// bei klick auf auswahlboxen ?>
			<div id="a_auswahlen" style="display:none"><?php		
				
				// 1.zeile: auswahl ausgabe für anja/michael	
				echo '<div class="auswahl">';
					echo '<input type="radio" id="ba" value="24" name="b_budget"';
						if ( $user == 'anja' ) { echo ' checked'; }
					echo '><label for="ba">Luxus Anja</label>';			
	
					echo '<input type="radio" id="ms" value="25" name="b_budget"';
						if ( $user == 'michael' ) { echo ' checked'; }
					echo '><label for="ms">Luxus Michael</label>';					
				echo '</div>';
				
				// 2.zeile: auswahl auswahl ausgabe für gemeinsam/leben
				echo '<div class="auswahl">'; 
					echo '<input type="radio" id="gemeinsam" value="26" name="b_budget"';
					echo '><label for="gemeinsam">Gemeinsam</label>';
					
					echo '<input type="radio" id="leben" value="27" name="b_budget"';
					echo '><label for="leben">Leben</label>';
					
					if ( $fixkosten ) {
						echo '<input type="radio" value="45" name="b_budget" checked>';
					}
					
					// elschnet: nicht sichtbar, aber notwendig
					echo '<div style="display:none">';
					echo '<input type="radio" id="elschnet" value="53" name="b_budget"';
					echo '><label for="elschnet">elschnet</label>';
					echo '</div>';
				echo '</div>';
				
				// 3.zeile: auswahl bar/karte/konto
				echo '<div class="auswahl">';
					echo '<input type="radio" id="bar" value="29" name="b_zahlart"';
						if ( $b_zahlart == 'bar' ) { echo ' checked'; }
					echo '><label for="bar">Bargeld</label>';
					
					echo '<input type="radio" id="karte" value="30" name="b_zahlart"';
						if ( $b_zahlart == 'karte' ) { echo ' checked'; }
					echo '><label for="karte">Karte/Konto</label>';
				echo '</div>'; ?>		
			</div>
			<?php
						
			// datum + button "eintragen"
			echo '<input type="date" name="a_datum" id="a_datum" value="'. date('Y-m-d') .'">';
			echo '<button type="submit" id="submit">Eintragen</button>';	
			
			// absicherung
			echo wp_nonce_field("neue_ausgabe", "_ononce", TRUE, FALSE );
			
			if ( $fixkosten ) { 
				echo '<br/><br/><br/><a href="/"><i>normale</i> Ausgabe eintragen</a>';
			}
			?>
			
		</form>
	</div>
	
	<?php	
	
	// **********************************************
	//  javascript im footer laden
	// **********************************************
	if ( !$fixkosten ) { function ausgabe() { 
		echo '<script>';
		echo '$(document).ready(function() {';
		
			// währungen anzeigen
			echo '$( "#a_waehrung" ).click(function() {';
				echo '$("#a_waehrungen").fadeIn("slow");'; 
				echo '$("#a_waehrung").val("");';
			echo '});';
		
			// auswahlen anzeigen
			echo '$( "#a_budget" ).click(function() {';
				echo '$("#a_auswahlen").fadeIn("slow");'; 
				echo '$("#a_budget").hide();'; 
			echo '});';
			
			// sonderfälle womo-diesel + elschnet
			echo '$( "#a_kat" ).change(function() {';
				echo 'var id = document.getElementById("a_kat").value;';
				
				// womo-diesel: weitere felder einblenden
				echo 'if( id==22 ) {';				
					// felder einblenden
					echo '$("#a_diesel").fadeIn("slow");'; 					
					// budgettext ändern
					echo '$("#a_budget").html("Budget: Leben, Karte/Konto");';					
					// budget radiobuttons ändern
					echo '$("input:radio[name=b_budget]").filter("[value=27]").prop("checked", true);'; // leben
					echo '$("input:radio[name=b_zahlart]").filter("[value=30]").prop("checked", true);'; // karte/konto				
				echo '}';				
				echo 'if( id!=22 ) { $("#a_diesel").fadeOut("slow"); }';
				
				// elschnet: budget bestimmen
				echo 'if( id==50 ) {';				
					// felder ein/ausblenden
					echo '$("#a_elschnet").fadeIn("slow");'; 
					echo '$("#a_budget").hide();';				
					// budget radiobuttons ändern
					echo '$("input:radio[name=b_budget]").filter("[value=53]").prop("checked", true);'; // elschnet
					echo '$("input:radio[name=b_zahlart]").filter("[value=30]").prop("checked", true);'; // karte/konto					
				echo '}';				
				echo 'if( id!=50 ) {';
					// felder ein/ausblenden
					echo '$("#a_elschnet").hide();'; 
					echo '$("#a_budget").fadeIn("slow");'; 
					// budgettext ändern
					echo '$("#a_budget").html("Budget: Leben, Karte/Konto");'; 
					// budget radiobuttons ändern
					echo '$("input:radio[name=b_budget]").filter("[value=27]").prop("checked", true);'; // leben
					echo '$("input:radio[name=b_zahlart]").filter("[value=30]").prop("checked", true);'; // karte/konto					
				echo '}';
				
			echo '})';			

		echo '});';
		echo '</script>';
	}
	add_action( 'wp_footer', 'ausgabe', 100 );
	}


	get_footer(); 	
} 

?> 