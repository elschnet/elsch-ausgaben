<?php defined('ABSPATH') or die("No script kiddies please!");
 
// Theme Name:  elsch-ausgaben
// Author:      Michael Schaefer
// Author URI:  https://elsch.net
// Version:     1.0 (2019-02)
// Text Domain:	elschnet_td
// Template Name: budget-auslastung
// *******************************
if ( !session_id() ) { session_start(); }



// **********************************************
//  aktueller benutzer
// ********************************************** 
$current_user = wp_get_current_user();
$user = strtolower( $current_user->user_firstname ); 


/*	
echo '<pre>';
print_r($args);
echo '</pre>';
*/

$beginn = '2019';
$zukunft = current_time('Y')+1;



// **********************************************
//  anzeigezeitraum
// **********************************************
if ( isset( $_GET['z'] ) ) { // bestimmtes jahr
		
	// kein jahr zu weit in der zukunft
	// kein jahr vor festgelegtem beginn 
	if ( $_GET['z']>$zukunft || $_GET['z']<$beginn || strtotime($_GET['z'])===false ) {
		header('Location:'. site_url( '/budget-auslastung/?z='. current_time('Y') ) );
		exit;
	}

	$year = $_GET['z'];	
} 

else { // aktuelles jahr
	$year = current_time('Y');
}



// **********************************************
//  jahreslinks herstellen
// **********************************************
$last_year = date( 'Y', mktime(0,0,0, 1, 1, $year-1 ));
$next_year = date( 'Y', mktime(0,0,0, 1, 1, $year+1 ));

// kein jahr vor festgelegtem beginn 
if ( $last_year < $beginn ) { unset($last_year); } 	
// kein jahr weiter in der zukunft
if ( $next_year > $zukunft ) { unset($next_year); } 




// **********************************************
//  aktuellen benutzer ermitteln
// **********************************************
if ( isset($_GET['michael']) ) {
	$benutzer = 'Michael';
	$other_user = 'anja';
	$cache_name = 'cache_ms'.$year;	
	$filter = '&f=lm';
}
elseif ( isset($_GET['anja']) ) {
	$benutzer = 'Anja';
	$other_user = 'michael';
	$cache_name = 'cache_ba'.$year;
	$filter = '&f=la';
}
else {
	// aufruf ohne username 
	if ( $user == 'michael' ) { header('Location:'. site_url( '/budget-auslastung/?michael' ) ); }
	elseif ($user == 'anja' ) { header('Location:'. site_url( '/budget-auslastung/?anja' ) ); }
	else { // falscher user, z.b. admin
		$_SESSION['fehler'] = 'Auswertung nicht möglich. Falscher Benutzername. (83)';
		header('Location:'. site_url('/fehler/') );
		exit;	
	}	
	exit;
}



// **********************************************
//  festgelegte budget holen
// **********************************************
$args = array(
    'posts_per_page' => -1,
    'post_type' => 'budget',
	'post_status' => 'any', // auch zukünftige budgets anzeigen
    
	'meta_key'   => 'b_person',
	'meta_value' => strtolower($benutzer),
	
); 
$budget_posts = get_posts( $args );



// **********************************************
//  seitenkopf
// **********************************************
$headline = '<h1>Budget '. $benutzer .' '. $year .'</h1>';
$GLOBALS['aktive_nav'] = 0;
get_header(); 
$inc = get_template_directory() .'/parts/header.php';
if ( !@include( $inc ) ) { elsch_include( $inc ); } 



// variablen
$id_budgetauslastung = get_the_id();


// **********************************************
//  inhalt
// ********************************************** 
if ( have_posts() ) { while ( have_posts() ) { the_post(); ?>

<div class="content-body"><?php

	// monats-navigation
	if ( $last_year ) { echo '<a id="nav1" href="/budget-auslastung/?'. strtolower($benutzer) .'&z='. $last_year .'">vorheriges Jahr</a>'; }
	if ( $next_year ) { echo '<a id="nav2" href="/budget-auslastung/?'. strtolower($benutzer) .'&z='. $next_year .'">nächstes Jahr</a>'; }
	echo '<div class="clear"></div>';


	#delete_post_meta( $id_budgetauslastung, 'cache_ms' );
	#delete_post_meta( $id_budgetauslastung, 'cache_ba' );
	
	// eingeräumtes budget
	if ( $budget_posts ) {
		$budget = array();
		foreach ( $budget_posts as $post ) {
			setup_postdata( $post );
			
			list( $b_year, $b_month ) = explode( '-', get_field('b_monat') );
			if ( $b_year != $year ) { continue; }
			
			// prüfen ob es schon einen wert für diesen monat gibt
			if ( array_key_exists( $b_month, $budget ) ) {
				$budget[ $b_month ] = $budget[ $b_month ] + get_field('b_betrag');
			} else {
				$budget[ $b_month ] = get_field('b_betrag');
			}
		}  
		wp_reset_postdata();		
	} 
	
	
	// summe ausgaben 
	$cache = get_post_meta( $id_budgetauslastung, $cache_name, true );
	
	// aus string array machen
	$cache_array = explode( ',', $cache ); 
	/* [0] => [1] => 01*326.75 [2] => 02*26.98 */
	$ausgabe = array();
	foreach ( $cache_array as $cache ) {
		$a = explode( '*', $cache );
		$ausgabe[$a[0]] = $a[1];
	}

	
	// variablen
	$sum_b = 0;
	$sum_a = 0;
	$sum_r = 0;

	
	echo '<table id="bud">';
	echo '<thead><tr>';
		echo '<th>Monat</th>';
		echo '<th>Budget</th>';
		echo '<th>Ausgaben</th>';
		echo '<th>Restbudget</th>';
	echo '</tr></thead><tbody>';
	
	// **********************************************
	//  monate durchlaufen
	// **********************************************
	$monate = array('01','02','03','04','05','06','07','08','09','10','11','12');
	foreach ( $monate as $monat ) {

		// monat
		$mc = '><a href="/monat-summen/?'. strtolower($benutzer) .'&z='. $year .'-'. $monat .'">';
	
		// budget
		if ( empty($budget[$monat]) ) { 
			$b = 0; 
			$bc = ' class="grey">'; 
			$bl = '';
		} else { 
			$b = $budget[$monat]; 
			$sum_b = $sum_b + $b; 
			$bc = '><a href="/budget/?'. strtolower($benutzer) .'&z='. $year .'-'. $monat .'">';
			$bl = '</a>';
		}
		
		// ausgaben
		if ( empty($ausgabe[$monat]) ) { 
			$a = 0; 
			$ac = ' class="grey">'; 
			$al = '';
		} else { 
			$a = $ausgabe[$monat]; 
			$sum_a = $sum_a + $a; 
			$ac = '><a href="/monat-einzeln/?'. strtolower($benutzer) .'&z='. $year .'-'. $monat . $filter .'">'; 
			$al = '</a>';;
		}

		// rest
		$r = $b - $a;
		$sum_r = $sum_r + $r;
		if ( $r < 0 ) { $rc = ' class="red">'; }
		elseif ( $r == 0 ) { $rc = ' class="grey">'; }
		else { $rc = '>'; }

		
		echo '<tr>';
			echo '<td'. $mc . date_i18n('F', strtotime( "$monat/01/2019" ) ) .'</td>';
			echo '<td'. $bc . number_format ( $b, 2,',','.' ) .'&nbsp;&euro;'. $bl .'</td>';
			echo '<td'. $ac . number_format ( $a, 2,',','.' ) .'&nbsp;&euro;'. $al .'</td>';
			echo '<td'. $rc . number_format ( $r, 2,',','.' ) .'&nbsp;&euro;</td>';
		echo '</tr>';
	}
	
	// summen
	if ( $sum_r < 0 ) { $rc = ' class="red">'; }
	else { $rc = '>'; }
	
	echo '</tbody><tfoot>';	
		echo '<tr>';
			echo '<td>Summen</td>';
			echo '<td>'. number_format ( $sum_b, 2,',','.' ) .'&nbsp;&euro;</td>';
			echo '<td>'. number_format ( $sum_a, 2,',','.' ) .'&nbsp;&euro;</td>';
			echo '<td'. $rc . number_format ( $sum_r, 2,',','.' ) .'&nbsp;&euro;</td>';
		echo '</tr>';	
	echo '</tfoot></table>';
	
	
	// neues budget eingeben
	echo '<a id="newbudget" href="/wp-admin/post-new.php?post_type=budget">neues Budget</a>';
	
	
	// user wechseln
	echo '<a id="user" href="/budget-auslastung/?'. $other_user.$link_time .'">Person wechseln</a>';
	echo '<div class="clear"></div>';
			
	?>
</div>
<?php


}} //if have_posts() und while have_posts()


get_footer(); ?> 