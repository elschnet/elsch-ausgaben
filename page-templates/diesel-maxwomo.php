<?php defined('ABSPATH') or die("No script kiddies please!");
 
// Theme Name:  elsch-ausgaben
// Author:      Michael Schaefer
// Author URI:  https://elsch.net
// Version:     1.0 (2019-01)
// Text Domain:	elschnet_td
// Template Name: diesel maxwomo
// *******************************
if ( !session_id() ) { session_start(); }


// **********************************************
//  alle ausgaben holen
// **********************************************
$args = array(
    'posts_per_page' => -1,
    'post_type' => 'ausgabe',
	'tax_query' => array(
		array(
			'taxonomy' => 'kategorie',
			'field' => 'term_id',
			'terms' => 22
		)
	)
);	

$ausgaben = get_posts( $args );


// variablen erstellen
$out_ausgaben = ''; // einzelne ausgaben auflisten
$summe_liter = 0; // gesamtliter
$summe_betrag = 0; // gesamtbetrag
$summe_kilometer = 0; // kilometerstand

$grafikwerte = array();


// **********************************************
//  alle ausgaben durchlaufen + summieren
// **********************************************
if ( $ausgaben ) {
	foreach ( $ausgaben as $post ) {
		setup_postdata( $post );
		$betrag = get_the_title();

		
		// ausgabearten
		$ausgabeart = array();
		$aus = get_the_terms( $post->ID, 'ausgabeart' );
		foreach ( $aus as $aa ) {
			$ausgabeart[] = $aa->slug;
		}
		
		
		// währung
		$waehrung = get_the_terms( $post->ID, 'waehrung' );
		$waehrung_id = $waehrung[0]->term_id;	
		// umrechnung fremdwährung
		if ( $waehrung_id != 37 ) { // euro=37
			$ausgabeart[] = 'waehrung';

			$umrechnungskurs = get_field('a_umrechnung');
			$kurs = str_replace(",", ".", $umrechnungskurs);
			$umrechnungskurs = str_replace(".", ",", $umrechnungskurs);
			$betrag = $betrag * $kurs;
		}
			
			
		// betrag runden auf 2 nachkommastellen
		$betrag = round( $betrag, 2 );
		
		
		// liter, kilometerstand, betragproliter
		$a_liter = get_field('a_liter');
		$a_kilometerstand = get_field('a_kilometerstand');
		$a_betragproliter = get_field('a_betragproliter');
		
		
		// nachrechnen, ob die werte stimmen können
		// mehr als 2 cent plus/minus unterschied
		$rechenwert = round($a_liter * $a_betragproliter, 2);
		if ( ( $rechenwert - 0.02 ) > $betrag || ( $rechenwert + 0.02 ) < $betrag ) {	
			$ausgabeart[] = 'dieselgate';
			$kategorie = '** WERTE PRÜFEN! **'. $rechenwert;
		}
		
		
		// kurzbeschreibung
		$betragproliter = number_format( $a_betragproliter, 3,',','.' );
		$bpl  = substr( $betragproliter, 0, -1 );
		$bpl .= '<sup>'. substr( $betragproliter, -1 ) .'</sup>';		
		$liter = number_format( $a_liter, 2,',','.' );	
		$kilometerstand = number_format( $a_kilometerstand, 0,',','.' );		
		$kurzbeschreibung = "$bpl &euro;/&#8467; &mdash; $liter &#8467; &mdash; $kilometerstand km";
		
		
		// summen addieren, kilometerstand fixieren
		$summe_liter = $summe_liter + $a_liter; // gesamtliter
		$summe_betrag = $summe_betrag + $betrag; //gesamtbetrag
		if ( $summe_kilometer == 0 ) {  $summe_kilometer = $a_kilometerstand; } //gesamtkilometer
		
		
		// icons
		$icon = 'il';
		
		
		// deutschland: text + werte für grafik dieselpreise		
		if ( get_field('a_land') == 'Deutschland' ) { // nur preise in deutschland
			$grafikwerte[] = array( 'preis'=>$betragproliter , 'datum'=>get_post_time('d.m.y') );
			$text = 'Diesel, Deutschland';
		}	
		
		// ausland: text
		else {
			$text = 'Diesel, '. get_field('a_land');
		}
		
		// einzelne ausgaben auflisten
		$out_ausgaben .= '<div class="ausgabe '. implode(' ', $ausgabeart ) .'">';
			$out_ausgaben .= '<a href="'. get_the_permalink() .'">';
			$out_ausgaben .= get_post_time('d.m.y') .' '. $text .'<strong>';
			$out_ausgaben .= number_format ( $betrag, 2,',','.' ) . ' &euro;</strong><br/>';
			$out_ausgaben .= '<div class="icon '. $icon .'">'. $kurzbeschreibung .'</div></a>';
		$out_ausgaben .= '</div>';
		
	}  
	wp_reset_postdata();
} 

else {
	$out_ausgaben .= '<div class="ausgabe">';
		$out_ausgaben .= '<div class="icon">Keine Ausgaben gefunden</div></a>';
	$out_ausgaben .= '</div>';
}




// **********************************************
//  seitenkopf
// **********************************************
$headline = '<h1>Diesel MAXwomo</h1>';
$GLOBALS['aktive_nav'] = 0;

get_header(); 

$inc = get_template_directory() .'/parts/header.php';
if ( !@include( $inc ) ) { elsch_include( $inc ); } 



// **********************************************
//  inhalt
// ********************************************** 
if ( have_posts() ) { while ( have_posts() ) { the_post(); ?>

<div class="content-body"><?php
	
	// aktueller verbrauch
	// beim verbrauch gehen wir davon aus, dass zuletzt wieder vollgetankt wurde
	// die erste tankfüllung von 87,87 litern und die bis dahin gefahrenen 7 km ziehen wir ab = initialfüllung
	$verbrauch = ($summe_liter - 87.87) / ( ( $summe_kilometer - 7 ) /100);
	
	echo '<div class="legende">';
		echo 'Verbrauch: <em>'. number_format ( $verbrauch, 2,',','.' ) .' &#8467;/100 km*</em>';
	echo '</div>';
	
	
	// alle ausgaben
	echo $out_ausgaben;

	
	// gesamtbetrag
	echo '<br/><div class="legende">';
		echo 'Gesamtbetrag: <em>'. number_format ( $summe_betrag, 2,',','.' ) .' &euro;</em>';
	echo '</div>';
	
	// gesamtliter
	echo '<div class="legende">';
		echo 'Gesamtmenge: <em>'. number_format ( $summe_liter, 1,',','.' ) .' &#8467;</em>';
	echo '</div>';
	
	// durchschnittlicher literpreis
	echo '<div class="legende">';
		echo 'Durchschn. Literpreis: <em>'. number_format ( $summe_betrag / $summe_liter, 3,',','.' ) .' &euro;/&#8467;</em>';
	echo '</div>';	

	// kilometerstand
	echo '<div class="legende">';
		echo 'Kilometerstand: <em>'. number_format ( $summe_kilometer, 0,',','.' ) .' km</em>';
	echo '</div>';
	
	?>
	<div class="clear"></div>
	
	<small>*) Bei dieser Berechnung wird die erste Tankfüllung herausgerechnet.</small>
	
	
	<?php 
	// grafik dieselpreise
	
	echo '<br/><br/>';
	echo '<h3>Dieselpreise (nur Deutschland)</h3>';
	
	// reihenfolge umdrehen: ältester zuerst
	$grafikwerte = array_reverse($grafikwerte);
	
	// standardabweichung
	// http://www.monkey-business.biz/2985/php-funktion-standardabweichung-stabw/
	
	// maximale balkenlänge in pixel
	$maxpixel = 350;
		
	// minimal + maximalwert
	$min = str_replace( ',', '.', min(array_column($grafikwerte, 'preis')));
	$max = str_replace( ',', '.', max(array_column($grafikwerte, 'preis')));
    	
	// faktor
	$diff = $max-$min;		
	if ( $diff >= 0.5 ) 	{ $faktor = 1; }
	elseif ( $diff >= .4 )	{ $faktor = 2; }
	elseif ( $diff >= .3 )	{ $faktor = 3; }
	elseif ( $diff >= .2 )	{ $faktor = 4; }
	elseif ( $diff >= .1 )	{ $faktor = 5; }
	/*
	echo "min: $min<br/>";
	echo "max: $max<br/>";
	echo "diff: $diff<br/>";
	echo "faktor: $faktor<br/>";
	*/
	echo '<hr>';
	
	
	// alle werte ausgeben
	foreach ( $grafikwerte as $grafikwert ) {

		$preis  = substr( $grafikwert['preis'], 0, -1 );
		$preis .= '<sup>'. substr( $grafikwert['preis'], -1 ) .'</sup>';
		$beschriftung = $grafikwert['datum'] .' &mdash; '. $preis .'&nbsp;&euro;';
		
		// preisabstände etwas deutlicher darstellen
		$balken = str_replace( ',', '.', $grafikwert['preis'] );
		$balken = round( $balken / $max, 5 );
		$faktor = $balken - (1-$balken) * $faktor;
				
		// länge des balkens
		$width = $faktor * $maxpixel;
		$width = round( $width, 0) .'px'; 
		if ( $faktor < 0 ) { $width = '0px'; }
		
		// farbe des balkens
		$filter = 'opacity('. round( ($faktor*100)-30, 0 ) .'%)';
		$filter  = 'grayscale('. round( ($faktor*100), 0 ) .'%)';
		
		echo '<div class="dieselpreis" style="width:'. $width .';filter:'. $filter .'"><span>'. $beschriftung .'</span></div>';
		
		#echo "faktor: $faktor ### balken: $balken<br/>";
	}
	
	echo '<hr>';
	?>
</div>
<?php


}} //if have_posts() und while have_posts() 


get_footer(); ?> 