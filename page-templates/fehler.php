<?php defined('ABSPATH') or die("No script kiddies please!");
 
// Theme Name:  elsch-ausgaben
// Author:      Michael Schaefer
// Author URI:  https://elsch.net
// Version:     1.0 (2018-12)
// Text Domain:	elschnet_td
// Template Name: Fehler
// *******************************


// **********************************************
//  session starten
// ********************************************** 
if ( !session_id() ) { session_start(); }


if ( have_posts() ) { while ( have_posts() ) { the_post();

// **********************************************
//  seitenkopf
// **********************************************
$headline = '<h1>'. get_the_title() .'</h1>';
$GLOBALS['aktive_nav'] = 0;
get_header(); 

$inc = get_template_directory() .'/parts/header.php';
if ( !@include( $inc ) ) { elsch_include( $inc ); } 



// **********************************************
//  inhalt
// ********************************************** ?>
<div class="content-body error">
	<p><?php echo $_SESSION['fehler'] ?></p>
	<p><a href="javascript:window.history.back()">zurück</a></p>
</div>

<?php
}} //if have_posts() und while have_posts() 


unset( $_SESSION['fehler'] );
get_footer(); ?> 