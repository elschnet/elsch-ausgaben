<?php defined('ABSPATH') or die("No script kiddies please!");
 
// Theme Name:  elsch-ausgaben
// Author:      Michael Schaefer
// Author URI:  https://elsch.net
// Version:     1.0 (2019-01)
// Text Domain:	elschnet_td
// Template Name: monat-einzeln
// *******************************
if ( !session_id() ) { session_start(); }



// **********************************************
//  aktueller benutzer
// ********************************************** 
$current_user = wp_get_current_user();
$user = strtolower( $current_user->user_firstname ); 


/*	
echo '<pre>';
print_r($args);
echo '</pre>';
*/

$beginn = '2019-01'; //elschtodo: sollte an zentrale stelle



// **********************************************
//  anzeigezeitraum
// **********************************************
if ( isset( $_GET['z'] ) ) { // bestimmter monat
		
	// kein monat in der zukunft
	// kein monat vor festgelegtem beginn 
	//if ( ($_GET['z'] > date('Y-m')) || ($_GET['z'] < $beginn)	) {
	if ( ($_GET['z'] < $beginn)	) {
		$link = substr( $_SERVER['REQUEST_URI'], 0, -10 );
		header('Location:'. site_url($link) );
		exit;
	}
	
	list( $year, $month ) = explode( '-', $_GET['z'] );	
} 
else { // aktueller monat
	$year = current_time('Y');
	$month = current_time('m');
}
$link_time = '&z='. $year .'-'. $month;



// **********************************************
//  monatslinks herstellen
// **********************************************
$last_month = date( 'Y-m', mktime(0,0,0, $month-1, date("d"), $year ));
$next_month = date( 'Y-m', mktime(0,0,0, $month+1, date("d"), $year ));

// kein monat vor festgelegtem beginn 
if ( "$year-$month" <= $beginn ) { unset($last_month); } 	
// kein monat in der zukunft
if ( "$year-$month" >= date('Y-m') ) { unset($next_month); } 



// **********************************************
//  anzeigefilter
// **********************************************
if ( isset( $_GET['f'] ) ) { $fi = $_GET['f']; }
switch ( $fi ) {
	
	case 'la': // nur luxus-anja 
		$filter = '&f=la';
		$filter_text = 'Luxus Anja';
		$filter_taxonomy = 'luxus-anja';
		$budget_anzeigen = true;
		break;
		
	case 'lm': // nur luxus-michael 
		$filter = '&f=lm';
		$filter_text = 'Luxus Michael';
		$filter_taxonomy = 'luxus-michael';
		$budget_anzeigen = true;
		break;
		
	case 'le': // nur leben 
		$filter = '&f=le';
		$filter_text = 'Lebensunterhalt';
		$filter_taxonomy = 'leben';
		$summe_anzeigen = true;
		break;
		
	case 'ge': // nur gemeinsam 
		$filter = '&f=ge';
		$filter_text = 'gemeinsame Ausgaben';
		$filter_taxonomy = 'gemeinsam';
		$summe_anzeigen = true;
		break;
		
	case 'en': // nur elschnet 
		$filter = '&f=en';
		$filter_text = 'elschnet';
		$filter_taxonomy = 'elschnet';
		$summe_anzeigen = true;
		break;
		
	case 'fi': // nur fixkosten 
		$filter = '&f=fi';
		$filter_text = 'Fixkosten';
		$filter_taxonomy = 'fixkosten';
		$summe_anzeigen = true;
		break;
		
	default: // kein filter bzw. all 
		$filter = '&f=all';
		$budget_anzeigen = true;
}



// **********************************************
//  aktuellen user ermitteln
// **********************************************
if ( isset($_GET['michael']) ) {
	// aufruf /monat-einzeln/?michael
	$benutzer = 'Michael';
	$luxus = 'luxus-michael';
	$other_user = 'anja';
	$cache_name = 'cache_ms'.$year;	
}
elseif ( isset($_GET['anja']) ) {
	// aufruf /monat-einzeln/?anja
	$benutzer = 'Anja';
	$luxus = 'luxus-anja';
	$other_user = 'michael';
	$cache_name = 'cache_ba'.$year;
}
else {
	// aufruf ohne username /monat-einzeln/ 
	if ( $user == 'michael' ) { header('Location:'. site_url( '/monat-einzeln/?michael'. $link_time.$filter ) ); }
	elseif ($user == 'anja' ) { header('Location:'. site_url( '/monat-einzeln/?anja'. $link_time.$filter ) ); }
	else { // falscher user, z.b. admin
		$_SESSION['fehler'] = 'Auswertung nicht möglich. Falscher Benutzername. (126)';
		header('Location:'. site_url('/fehler/') );
		exit;	
	}	
	exit;
}



// **********************************************
//  falsche aufrufe verhindern
// **********************************************
if ( isset($_GET['michael']) && $_GET['f']=='la' ) {
	// aufruf michael, aber filter luxus-anja = doof
	$filter = '&f=lm';
	header('Location:'. site_url( '/monat-einzeln/?michael'. $link_time.$filter ) );	
}
elseif ( isset($_GET['anja']) && $_GET['f']=='lm' ) {
	// aufruf anja, aber filter luxus-michael = auch doof
	$filter = '&f=la';
	header('Location:'. site_url( '/monat-einzeln/?anja'. $link_time.$filter ) );
}



// **********************************************
//  filterlinks bauen
// **********************************************
$f_link = array();
$f_link[] = '<span>Filterlinks:</span>';
if ( $user == 'michael' ) { 
	$f_link[] = '<a href="/monat-einzeln/?michael'. $link_time .'&f=lm">nur eigener Luxus</a>';
	$f_link[] = '<a href="/monat-einzeln/?anja'. $link_time .'&f=la">nur Luxus Anja</a>';
}
elseif ( $user == 'anja' ) {
	$f_link[] = '<a href="/monat-einzeln/?anja'. $link_time .'&f=la">nur eigener Luxus</a>';
	$f_link[] = '<a href="/monat-einzeln/?michael'. $link_time .'&f=lm">nur Luxus Michael</a>';
}
$f_link[] = '<a href="/monat-einzeln/?'. $user.$link_time .'&f=le">nur Lebensunterhalt</a>';
$f_link[] = '<a href="/monat-einzeln/?'. $user.$link_time .'&f=ge">nur gemeinsame Ausgaben</a>';
$f_link[] = '<a href="/monat-einzeln/?'. $user.$link_time .'&f=en">nur elschnet Ausgaben</a>';
$f_link[] = '<a href="/monat-einzeln/?'. $user.$link_time .'&f=fi">nur Fixkosten</a>';
$GLOBALS['f_link'] = $f_link;



// **********************************************
//  festgelegte budget holen
// **********************************************
$args = array(
    'posts_per_page' => -1,
    'post_type' => 'budget',
    'meta_query' => array(
        array(
            'key'     => 'b_person',
            'value'   => strtolower($benutzer),
        ),
        array(
            'key'     => 'b_monat',
            'value'   => "$year-$month",
        ),
    ),
); 
$budget_posts = get_posts( $args );

$budget = 0;

if ( $budget_posts ) {
	foreach ( $budget_posts as $budget_post ) {		
		$budget_id = $budget_post->ID;
		$budget = $budget + get_post_meta( $budget_id, 'b_betrag', true );
	}  
}




// **********************************************
//  alle ausgaben holen
// **********************************************
$args = array(
    'posts_per_page' => -1,
    'post_type' => 'ausgabe',
	'post_status' => 'any', // auch zukünftige ausgaben anzeigen
    
	// anzeigezeitraum
	'date_query' => array(
        array(
            'year'	=> $year,
			'month' => $month			
        ),
    )
);
// filter berücksichtigen
if ( isset($filter_taxonomy) ) {	
	$args['tax_query'] = array(
		array (
			'taxonomy' => 'ausgabeart',
			'field' => 'slug',
			'terms' => $filter_taxonomy
	));	
} 
$ausgaben = get_posts( $args );


// variablen erstellen
$out_ausgaben = ''; // einzelne ausgaben auflisten
$summe_ausgaben = 0; // summe ausgaben die budget belasten



// **********************************************
//  alle ausgaben durchlaufen + summieren
// **********************************************
if ( $ausgaben ) {
	foreach ( $ausgaben as $post ) {
		setup_postdata( $post );
		$betrag = get_the_title();
		$beschreibung = wp_strip_all_tags( get_the_content() );
			
			
		// kategorie
		$kat = get_the_terms( $post->ID, 'kategorie' );
		$kategorie = $kat[0]->name;
		
		
		// ausgabearten
		$ausgabeart = array();
		$aus = get_the_terms( $post->ID, 'ausgabeart' );
		foreach ( $aus as $aa ) {
			$ausgabeart[] = $aa->slug;
		}
		
		
		// währung
		$waehrung = get_the_terms( $post->ID, 'waehrung' );
		$waehrung_id = $waehrung[0]->term_id;	
		// umrechnung fremdwährung
		if ( $waehrung_id != 37 ) { // euro=37
			$ausgabeart[] = 'waehrung';

			$umrechnungskurs = get_field('a_umrechnung');
			$kurs = str_replace(",", ".", $umrechnungskurs);
			$umrechnungskurs = str_replace(".", ",", $umrechnungskurs);
			$betrag = $betrag * $kurs;
		}
		
		
		// kurzbeschreibung
		if ( strlen($beschreibung) > 31 ) { $kurzbeschreibung = mb_substr($beschreibung, 0, 30) .'&hellip;'; }
		elseif ( empty($beschreibung) ) { $kurzbeschreibung = ' &mdash;'; }
		else { $kurzbeschreibung = $beschreibung; }
				
		
		// betrag runden auf 2 nachkommastellen
		$betrag = round( $betrag, 2 );
		
		
		// sonderfall: womo-diesel
		if ( $kat[0]->term_id == 22 ) {
			$a_liter = get_field('a_liter');
			$a_kilometerstand = get_field('a_kilometerstand');
			$a_betragproliter = get_field('a_betragproliter');
						
			$betragproliter = number_format( $a_betragproliter, 3,',','.' );
			$bpl  = substr( $betragproliter, 0, -1 );
			$bpl .= '<sup>'. substr( $betragproliter, -1 ) .'</sup>';
			$liter = number_format( $a_liter, 2,',','.' );	
			$kilometerstand = number_format( $a_kilometerstand, 0,',','.' );
			$kurzbeschreibung = "$bpl &euro;/&#8467; &mdash; $liter &#8467; &mdash; $kilometerstand km";
		
			// nachrechnen, ob die werte stimmen können
			// mehr als 2 cent plus/minus unterschied
			$rechenwert = round($a_liter * $a_betragproliter, 2);
			if ( ( $rechenwert - 0.02 ) > $betrag || ( $rechenwert + 0.02 ) < $betrag ) {	
				$ausgabeart[] = 'dieselgate';
				$kategorie = '** WERTE PRÜFEN! **';
			}
		}

		
		
		// icons
		if ( in_array('leben', $ausgabeart) ) {
			$icon = 'il';
		}
		
		elseif ( in_array('gemeinsam', $ausgabeart) ) {
			$icon = 'ig';
		}
		
		elseif ( in_array('elschnet', $ausgabeart) ) {
			$icon = 'ie';
		}
		
		elseif ( in_array('fixkosten', $ausgabeart) ) {
			$icon = 'if';
		}
		
		elseif ( in_array($luxus, $ausgabeart) ) {
			// luxus kennzeichnen
			// ausgaben summieren		
			$ausgabeart[] = 'luxus';
			$summe_ausgaben = $summe_ausgaben + $betrag;
			$icon = 'i1';
		}
		
		else {
			$icon = 'i0';
		}
		
		
		// summe bilden bei filter "leben", "gemeinsam", "fixkosten"
		if ( $summe_anzeigen ) {
			$summe_ausgaben = $summe_ausgaben + $betrag;
		}
		

		
		// einzelne ausgaben auflisten
		$out_ausgaben .= '<div class="ausgabe '. implode(' ', $ausgabeart ) .'">';
			$out_ausgaben .= '<a href="'. get_the_permalink() .'">';
			$out_ausgaben .= get_post_time('d.m.') .' '. $kategorie .'<strong>';
			$out_ausgaben .= number_format ( $betrag, 2,',','.' ) . ' &euro;</strong><br/>';
			$out_ausgaben .= '<div class="icon '. $icon .'">'. $kurzbeschreibung .'</div></a>';
		$out_ausgaben .= '</div>';
		
	}  
	wp_reset_postdata();
} 
else {
	$out_ausgaben .= '<div class="ausgabe">';
		$out_ausgaben .= '<div class="icon">Keine Ausgaben gefunden</div></a>';
	$out_ausgaben .= '</div>';
}



// **********************************************
//  "zeitstrahl" budgetauslastung
// **********************************************
$rest = $budget - $summe_ausgaben;
if ( $budget == 0 ) { $budgetprozent = 0; }
else { $budgetprozent = round( $summe_ausgaben/$budget*100, 0); }

if ( $rest > 0 ) {
	$budgetgrafik = $budgetprozent;
	$budgettext = 'Rest Monatsbudget';	
} else {
	$budgetgrafik = 100;
	$budgettext = 'Monatsbudget überschritten um';
	$rest = $rest * -1;	
}



// **********************************************
//  seitenkopf
// **********************************************
$headline = '<h2>Einzelne Ausgaben<br/>'. date_i18n('F Y', strtotime( $month .'/01-'. $year ) ) .'</h2>';

// aktiver navigationspunkt: aktuell oder auswertung
$aktuell = 	current_time('Y'). '-'. current_time('m');
if ( $aktuell == "$year-$month" ) { $GLOBALS['aktive_nav'] = 1; }
else { $GLOBALS['aktive_nav'] = 3; }

get_header(); 

$inc = get_template_directory() .'/parts/header.php';
if ( !@include( $inc ) ) { elsch_include( $inc ); } 



// **********************************************
//  inhalt
// ********************************************** 
if ( have_posts() ) { while ( have_posts() ) { the_post(); ?>

<div class="content-body"><?php

	// monats-navigation
	if ( $last_month ) { echo '<a id="nav1" href="/monat-einzeln/?'. strtolower($benutzer) .'&z='. $last_month.$filter .'">vorheriger Monat</a>'; }
	if ( $next_month ) { echo '<a id="nav2" href="/monat-einzeln/?'. strtolower($benutzer) .'&z='. $next_month.$filter .'">nächster Monat</a>'; }
	echo '<div class="clear"></div>';

	
	// filtertext
	if ( isset( $filter_text ) ) {
		echo '<div class="legende filter">Filter: nur '. $filter_text;
		echo '<em><a href="/monat-einzeln/?'. $user.$link_time. '&f=all">&nbsp;</a></em></div>'; 
	}
	
	
	if ( $budget_anzeigen ) {
		// "zeitstrahl" 
		echo '<div class="legende strahl">';
			echo '<span><em>'. $budgetprozent .'% Budgetauslastung '. $benutzer .'</em>&nbsp;</span>';
			echo '<em>'. number_format ( $budget, 0,',','.' ) .'&nbsp;&euro;</em>';
		echo '</div>';
		// css zeitstrahl
		echo '<style>@-webkit-keyframes strahl{from{width:0}to{width:'. $budgetgrafik .'%}}</style>';
	}

	
	// alle ausgaben des monats einzeln
	echo $out_ausgaben;
		
		
	if ( $budget_anzeigen ) {	
		// budget
		echo '<br/><div class="legende"><a href="/budget/?'. strtolower($benutzer) .'&z='. $year .'-'. $month .'">';
			echo 'Budget '. $benutzer;			
			echo '<em>'. number_format ( $budget, 2,',','.' ) .'&nbsp;&euro;</em></a>';
		echo '</div>';
		// summe_ausgaben
		echo '<div class="legende">';
			echo 'Luxusausgaben '. $benutzer;			
			echo '<em>'. number_format ( $summe_ausgaben, 2,',','.' ) .'&nbsp;&euro;</em>';
		echo '</div>';	
		// restbudget o. budget überschritten
		echo '<div class="legende">';
			echo $budgettext;
			echo '<em>'. number_format ( $rest, 2,',','.' ) .'&nbsp;&euro;</em>';
		echo '</div>';
	}
		
		
	// summe bei filter "leben", "gemeinsam", "fixkosten" anzeigen
	if ( $summe_anzeigen ) {
		echo '<br/><div class="legende">';
			echo 'Summe '. $filter_text;
			echo '<em>'. number_format ( $summe_ausgaben, 2,',','.' ) .'&nbsp;&euro;</em>';
		echo '</div>';	
	}
	
	
	// neues budget eingeben
	echo '<a id="newbudget" href="/wp-admin/post-new.php?post_type=budget">neues Budget</a>';
	
	
	// user wechseln
	if ( $filter == '&f=all' ) {		
		echo '<a id="user" href="/monat-einzeln/?'. $other_user.$link_time.$filter .'">Person wechseln</a>';
	}
	
	?>
	<div class="clear"></div>
</div>
<?php


}} //if have_posts() und while have_posts() 





// **********************************************
//  budgetauslastung für budgetübersicht zwischenspeichern
// ********************************************** 
if ( $budget_anzeigen ) { 
	// post_meta als array speichern
	// https://stackoverflow.com/a/946300
	
	// id der seite "budget-auslastung"
	// hierin werden die metafields gespeichert
	$id_budgetauslastung = 273;

	// bisherigen cache holen	
	$cache = get_post_meta( $id_budgetauslastung, $cache_name, true );	
	
	// neuen wert hinzufügen
	$cache = $cache .','. $month .'*'. $summe_ausgaben;
	
	// aus string array machen
	$cache_array = explode( ',', $cache ); 
	
	// array unique
	$cache_array = array_unique($cache_array);
	
	// wieder in string verpacken
	$cache = implode(",", $cache_array);

	// cache wieder speichern
	update_post_meta( $id_budgetauslastung, $cache_name, $cache );
}


get_footer(); ?> 