<?php defined('ABSPATH') or die("No script kiddies please!");
 
// Theme Name:  elsch-ausgaben
// Author:      Michael Schaefer
// Author URI:  https://elsch.net
// Version:     1.0 (2019-01)
// Text Domain:	elschnet_td
// Template Name: monat-summen
// *******************************
if ( !session_id() ) { session_start(); }



// **********************************************
//  aktueller benutzer
// ********************************************** 
$current_user = wp_get_current_user();
$user = strtolower( $current_user->user_firstname ); 


/*	
echo '<pre>';
print_r($args);
echo '</pre>';
*/

$beginn = '2019-01'; //elschtodo: sollte an zentrale stelle



// **********************************************
//  anzeigezeitraum
// **********************************************
if ( isset( $_GET['z'] ) ) { // bestimmter monat
		
	// kein monat in der zukunft
	// kein monat vor festgelegtem beginn 
	if ( ($_GET['z'] > date('Y-m')) || ($_GET['z'] < $beginn)	) {
		$link = substr( $_SERVER['REQUEST_URI'], 0, -10 );
		header('Location:'. site_url($link) );
		exit;
	}
	
	list( $year, $month ) = explode( '-', $_GET['z'] );	
} 
else { // aktueller monat
	$year = current_time('Y');
	$month = current_time('m');
}
$link_time = '&z='. $year .'-'. $month;



// **********************************************
//  monatslinks herstellen
// **********************************************
$last_month = date( 'Y-m', mktime(0,0,0, $month-1, date("d"), $year ));
$next_month = date( 'Y-m', mktime(0,0,0, $month+1, date("d"), $year ));

// kein monat vor festgelegtem beginn 
if ( "$year-$month" <= $beginn ) { unset($last_month); } 	
// kein monat in der zukunft
if ( "$year-$month" >= date('Y-m') ) { unset($next_month); } 



// **********************************************
//  aktuellen user ermitteln
// **********************************************
if ( isset($_GET['michael']) ) {
	// aufruf /monat-summen/?michael
	$benutzer = 'michael';
	$luxus = 'luxus-michael';
	$other_user = 'anja';
}
elseif ( isset($_GET['anja']) ) {
	// aufruf /monat-summen/?anja
	$benutzer = 'anja';
	$luxus = 'luxus-anja';
	$other_user = 'michael';
}
else {
	// aufruf ohne username /monat-einzeln/ 
	if ( $user == 'michael' ) { header('Location:'. site_url( '/monat-summen/?michael'. $link_time.$filter ) ); }
	elseif ($user == 'anja' ) { header('Location:'. site_url( '/monat-summen/?anja'. $link_time.$filter ) ); }
	else { // falscher user, z.b. admin
		$_SESSION['fehler'] = 'Auswertung nicht möglich. Falscher Benutzername. (85)';
		header('Location:'. site_url('/fehler/') );
		exit;	
	}	
	exit;
}


// **********************************************
//  alle ausgaben holen
// **********************************************
$args = array(
    'posts_per_page' => -1,
    'post_type' => 'ausgabe',
	'post_status' => 'any', // auch zukünftige ausgaben anzeigen
    
	// anzeigezeitraum
	'date_query' => array(
        array(
            'year'	=> $year,
			'month' => $month			
        ),
    )
);
$ausgaben = get_posts( $args );


// variablen erstellen
$summe_elschnet = 0;
$summe_michael = 0;
$summe_anja = 0;
$summe_leben = 0;
$summe_gemeinsam = 0;
$summe_fixkosten = 0;

$kreis_legende 		= ''; // erklärung kreisdiagramm
$kategorie_summe	= array(); // array für kategoriesummen
$kategorie_name		= array(); // array für kategorienamen
$kategorie_farbe	= array(); // array für kategoriefarben
$kreis_stuecke		= array(); // tortenstücke
$kreis_prozent		= 0; // für addition prozentwerte


// **********************************************
//  alle ausgaben durchlaufen + summieren
// **********************************************
if ( $ausgaben ) {
	foreach ( $ausgaben as $post ) {
		setup_postdata( $post );
		$betrag = get_the_title();		
		
		
		// ausgabearten
		$ausgabeart = array();
		$aus = get_the_terms( $post->ID, 'ausgabeart' );
		foreach ( $aus as $aa ) {
			$ausgabeart[] = $aa->slug;
		}
		
		
		// währung
		$waehrung = get_the_terms( $post->ID, 'waehrung' );
		$waehrung_id = $waehrung[0]->term_id;	
		// umrechnung fremdwährung
		if ( $waehrung_id != 37 ) { // euro=37
			$ausgabeart[] = 'waehrung';

			$umrechnungskurs = get_field('a_umrechnung');
			$kurs = str_replace(",", ".", $umrechnungskurs);
			$umrechnungskurs = str_replace(".", ",", $umrechnungskurs);
			$betrag = $betrag * $kurs;
		}
		
		
		// betrag runden auf 2 nachkommastellen
		$betrag = round( $betrag, 2 );


		if ( in_array('leben', $ausgabeart) ) {
			$summe_leben = $summe_leben + $betrag;
		}
		elseif ( in_array('gemeinsam', $ausgabeart) ) {
			$summe_gemeinsam = $summe_gemeinsam + $betrag;
		}
		elseif ( in_array('fixkosten', $ausgabeart) ) {
			$summe_fixkosten = $summe_fixkosten + $betrag;
		}
		elseif ( in_array('luxus-anja', $ausgabeart) ) {
			$summe_anja = $summe_anja + $betrag;
		}
		elseif ( in_array('luxus-michael', $ausgabeart) ) {
			$summe_michael = $summe_michael + $betrag;
		}
		elseif ( in_array('elschnet', $ausgabeart) ) {
			$summe_elschnet = $summe_elschnet + $betrag;
		}
		

		
		// wenn luxus, kategorien summieren
		if ( in_array($luxus, $ausgabeart) ) { 
				
			// kategorie
			$kategorie = get_the_terms( $post->ID, 'kategorie' );
			$kategorie_id = $kategorie[0]->term_id;
		
			// kategorie-namen
			$kategorie_name[$kategorie_id] = $kategorie[0]->name;
						
			// kategorie-farben
			$farbe = get_term_meta($kategorie_id,'farbe_kreisdiagramm');
			$kategorie_farbe[$kategorie_id] = $farbe[0];
						
			// summe je kategorie	
			if ( isset($kategorie_summe[$kategorie_id]) ) {
				// kategorie bereits in array vorhanden = addieren
				$kategorie_summe[$kategorie_id] = $kategorie_summe[$kategorie_id] + $betrag;
			} else {
				// "neue" kategorie
				$kategorie_summe[$kategorie_id] = $betrag;
			}
		}
		
	}  	
	wp_reset_postdata();
	
	
	// luxus anja
	if ( $benutzer == 'anja' ) { 
		$icon = 'i1'; $class = 'luxus'; 
		$kreis_headline = '<div class="legende">Luxusausgaben Anja';
			$kreis_headline .= '<em>'. number_format ( $summe_anja, 2,',','.' ) .' &euro;</em>';
		$kreis_headline .= '</div>';		
	} else { $icon = 'i0'; }	
	$out_ausgaben .= '<div class="ausgabe summe '. $class .'">';
		$out_ausgaben .= '<a href="/monat-einzeln/?anja'. $link_time .'&f=la">';
		$out_ausgaben .= '<div class="icon '. $icon .'">Luxusausgaben Anja<strong>';
		$out_ausgaben .= number_format ( $summe_anja, 2,',','.' ) . ' &euro;</strong></div></a>';
	$out_ausgaben .= '</div>';
	// elschtodo: cache schreiben
	
	// luxus michael
	if ( $benutzer == 'michael' ) {  
		$icon = 'i1'; $class = 'luxus'; 
		$kreis_headline = '<div class="legende">Luxusausgaben Michael';
			$kreis_headline .= '<em>'. number_format ( $summe_michael, 2,',','.' ) .' &euro;</em>';
		$kreis_headline .= '</div>';		
	} else { $icon = 'i0'; $class = ''; }	
	$out_ausgaben .= '<div class="ausgabe summe '. $class .'">';
		$out_ausgaben .= '<a href="/monat-einzeln/?michael'. $link_time .'&f=lm">';
		$out_ausgaben .= '<div class="icon '. $icon .'">Luxusausgaben Michael<strong>';
		$out_ausgaben .= number_format ( $summe_michael, 2,',','.' ) . ' &euro;</strong></div></a>';
	$out_ausgaben .= '</div>';
	// elschtodo: cache schreiben
	
	// elschnet
	$icon = 'ie';
	$out_ausgaben .= '<div class="ausgabe summe">';
		$out_ausgaben .= '<a href="/monat-einzeln/?'. $user . $link_time .'&f=en">';
		$out_ausgaben .= '<div class="icon '. $icon .'">elschnet / gemeinsame Steuer<strong>';
		$out_ausgaben .= number_format ( $summe_elschnet, 2,',','.' ) . ' &euro;</strong></div></a>';
	$out_ausgaben .= '</div>';
	
	// gemeinsam
	$icon = 'ig';
	$out_ausgaben .= '<div class="ausgabe summe">';
		$out_ausgaben .= '<a href="/monat-einzeln/?'. $user . $link_time .'&f=ge">';
		$out_ausgaben .= '<div class="icon '. $icon .'">Gemeinsame Ausgaben<strong>';
		$out_ausgaben .= number_format ( $summe_gemeinsam, 2,',','.' ) . ' &euro;</strong></div></a>';
	$out_ausgaben .= '</div>';
	
	// fixkosten
	$icon = 'if';
	$out_ausgaben .= '<div class="ausgabe summe">';
		$out_ausgaben .= '<a href="/monat-einzeln/?'. $user . $link_time .'&f=fi">';
		$out_ausgaben .= '<div class="icon '. $icon .'">Fixkosten<strong>';
		$out_ausgaben .= number_format ( $summe_fixkosten, 2,',','.' ) . ' &euro;</strong></div></a>';
	$out_ausgaben .= '</div>';
	
	// leben
	$icon = 'il';
	$out_ausgaben .= '<div class="ausgabe summe">';
		$out_ausgaben .= '<a href="/monat-einzeln/?'. $user . $link_time .'&f=le">';
		$out_ausgaben .= '<div class="icon '. $icon .'">Lebensunterhalt<strong>';
		$out_ausgaben .= number_format ( $summe_leben, 2,',','.' ) . ' &euro;</strong></div></a>';
	$out_ausgaben .= '</div>';
	
	// gesamt
	$icon = 'is';
	$summen = $summe_leben + $summe_gemeinsam + $summe_fixkosten + $summe_michael + $summe_anja;
	$out_ausgaben .= '<br/><div class="ausgabe summe">';
		$out_ausgaben .= '<div class="icon '. $icon .'">Gesamtsumme '. date_i18n('F Y', strtotime( $month .'/01-'. $year ) ) .'<strong>';
		$out_ausgaben .= number_format ( $summen, 2,',','.' ) . ' &euro;</strong></div>';
	$out_ausgaben .= '</div>';
	
} 

else {
	$out_ausgaben .= '<div class="ausgabe">';
		$out_ausgaben .= '<div class="icon">Keine Ausgaben gefunden</div></a>';
	$out_ausgaben .= '</div>';
}



// **********************************************
//  kreisdiagramm
// **********************************************
if ( count($kategorie_summe) > 1 ) {
	// 100 prozent
	$kreis_hundert = array_sum( $kategorie_summe );

	// höchster wert zuerst
	arsort( $kategorie_summe ); 

		
	// tortenstücke und legende
	foreach ($kategorie_summe as $key => $wert) {
		
		// farbe der kategorie
		$farbe = $kategorie_farbe[$key];
		
		// addition prozentwerte
		$kreis_prozent = $kreis_prozent + round( ( $wert/$kreis_hundert ) * 100, 1 );
			
		// css diagramm: farbe 0, farbe prozentwert(addiert)
		$kreis_stuecke[] = "$farbe 0";
		$kreis_stuecke[] = "$farbe $kreis_prozent%";
			
		$kreis_legende .= '<div class="legende" style="background:'. $farbe .'">';
			$kreis_legende .= $kategorie_name[$key] . ' ('. number_format( round( ( $wert/$kreis_hundert ) * 100, 1 ), 1,',','.' ) .'%)';
			$kreis_legende .= '<em>'. number_format ( $wert, 2,',','.' ) .' €</em>';
		$kreis_legende .= '</div>';

	}

	// wenn mehr als ein element:
	// erstes element des array löschen
	// letztes element des array löschen
	if ( count( $kreis_stuecke ) > 2 ) {
		array_pop( $kreis_stuecke ); 
		array_shift( $kreis_stuecke ); 
	}
}






// **********************************************
//  seitenkopf
// **********************************************
$headline = '<h1>Summen '. date_i18n('F Y', strtotime( $month .'/01-'. $year ) ) .'</h1>';
$GLOBALS['aktive_nav'] = 3;
get_header(); 
$inc = get_template_directory() .'/parts/header.php';
if ( !@include( $inc ) ) { elsch_include( $inc ); } 



// **********************************************
//  inhalt
// ********************************************** 
if ( have_posts() ) { while ( have_posts() ) { the_post(); ?>

<div class="content-body"><?php

	// monats-navigation
	if ( $last_month ) { echo '<a id="nav1" href="/monat-summen/?'. $benutzer .'&z='. $last_month .'">vorheriger Monat</a>'; }
	if ( $next_month ) { echo '<a id="nav2" href="/monat-summen/?'. $benutzer .'&z='. $next_month .'">nächster Monat</a>'; }
	echo '<div class="clear"></div>';

	
	// alle ausgaben des monats einzeln
	echo $out_ausgaben;	

	
	// link budget-übersicht	
	echo '<br/><div class="ausgabe summe">';
		echo '<a href="/budget-auslastung/?'. $benutzer .'&z='. $year .'">';
		echo '<div class="icon i1">Budget-Übersicht '. ucfirst($benutzer) .' '. $year .'</div></a>';
	echo '</div>';
	
	
	// user wechseln
	echo '<a id="user" href="/monat-summen/?'. $other_user.$link_time .'">Person wechseln</a>';
	echo '<div class="clear"></div>';
	
	
	// kreisdiagramm ausgeben
	if ( $kreis_hundert > 0 ) {	
		echo $kreis_headline;
		echo '<div class="kreisdiagramm"><div class="pie" style="background: conic-gradient(';
			echo implode( ',', $kreis_stuecke );
		echo ');"></div></div>';
		echo $kreis_legende;
	}
	
	?>
	<div class="clear"></div>
</div>
<?php


}} //if have_posts() und while have_posts()



// **********************************************
//  javascript im footer laden
// ********************************************** 
if ( $kreis_hundert > 0 ) {
	function kreisdiagramm() { 
		echo '<script src="'. get_template_directory_uri() .'/assets/js/kreisdiagramm.min.js"></script>'; 
		
		/*
		echo '<script>';
		echo '$(document).ready(function() {';
		echo '});';
		echo '</script>';
		*/
	}
	add_action( 'wp_footer', 'kreisdiagramm', 100 );
}

get_footer(); ?> 