<?php defined('ABSPATH') or die("No script kiddies please!");
 
// Theme Name:  elsch-ausgaben
// Author:      Michael Schaefer
// Author URI:  https://elsch.net
// Version:     1.0 (2018-12)
// Text Domain:	elschnet_td
// *******************************


// **********************************************
//  theme: version, name, is-dev
// ********************************************** 
$theme_version 	= '1.5';
$theme_name		= 'orte'; 	// keine sonderzeichen!
#define('WP_DEBUG', true); 		// überschreiben der einstellung aus wp-config.php
								// elschtodo: ausschalten im live-betrieb! 

 
// **********************************************
//  this theme specials
// **********************************************
$dir = dirname( __FILE__ ) . '/vendor/';
include_once( $dir . 'theme-functions.php' );



// **********************************************
//  setup
// ********************************************** 
add_action( 'after_setup_theme', 'elschnet_setup', 0 );
function elschnet_setup() {
	// pfad der theme-dateien
	$dir = dirname( __FILE__ ) . '/vendor/elschnet/';

	// mehrsprachigkeit
	load_theme_textdomain( 'elschnet_td', get_template_directory() . '/languages' );

	
	// theme support
	add_theme_support( 'automatic-feed-links' ); 	// feed-links im header - bleibt so, auch wenn feeds abgeschaltet werden
	add_theme_support( 'html5', array( 'comment-list', 'comment-form', 'search-form', 'gallery', 'caption'  ) );
	add_theme_support( 'post-thumbnails' ); 		// beitragsbilder 	
	add_theme_support( 'title-tag' ); 				// wordpress macht title

	
	// bilder, bilder-galerien, lazyload
	include_once( $dir . 'setup_images.php' );
	elschnet_register_image_sizes(); // bildgrößen festlegen
	add_filter( 'img_caption_shortcode', 'elschnet_caption_shortcode', 10, 3 ); 	// bildbeschreibung	
	add_filter( 'image_make_intermediate_size', 'elschnet_bilder_schaerfen', 900); 	// bilder schärfen	
	#
	add_filter( 'use_default_gallery_style', '__return_false' ); 		// keine inline-stylesheets 
	#add_filter( 'post_gallery', 'elschnet_custom_gallery', 10, 2 ); 	// aussehen einer galerie
	#add_filter( 'media_view_settings', 'elsch_gallery_defaults' ); 		// galerie-standardwerte ändern	
	#
	#add_filter( 'the_content', 'elschnet_add_image_load', 99 ); 		// lazyload bilder
	#add_filter( 'post_thumbnail_html', 'elschnet_add_image_load', 11 ); // lazyload bilder
	#add_filter( 'get_avatar', 'elschnet_add_image_load', 11 ); 		// lazyload bilder
	#add_filter( 'the_content', 'elschnet_add_video_load' ); 			// lazyload video
	#add_action( 'wp_footer', 'elschnet_add_lazyload_script'); 			// lazyload javascript
	
	
	
	// navigation + menü-walker
	include_once( $dir . 'setup_navigation.php' );
	elschnet_register_menus();
	
	
	
	// widgets
	include_once( $dir . 'setup_widgets.php' );
	add_action( 'widgets_init', 'elschnet_register_sidebars' );		// widget-bereiche definieren
	add_action( 'widgets_init', 'elschnet_load_widget' ); 			// einfaches textwidget
	add_filter( 'widget_text', 'do_shortcode' ); 					// shortcodes in widgets möglich machen 

	
	// customizer anpassen
	#include_once( $dir . 'theme_customizer.php' );
	#add_action('customize_register', 'elschnet_customizer_logo');	//  logo upload im customizer
	
	
	// login styles + customizer
	include_once( $dir . 'backend_login.php' );
	add_action( 'login_enqueue_scripts', 'elschnet_enqueue_login_style' ); 	// login styles	
	add_filter( 'login_errors','elschnet_login_error_message' ); 			// fehlermeldung ändern
	add_action( 'login_head', 'elschnet_loginshake'); 						// blödes wackeln abstellen
	add_action( 'init', 'elschnet_checked_rememberme' ); 					// remember me = checked
	add_filter( 'login_headerurl', 'elschnet_login_logo_url' ); 			// url logo
	add_filter( 'login_headertitle', 'elschnet_login_logo_url_title' ); 	// title logo
	#
	add_action( 'customize_register', 'elschnet_login_customizer' );		// customizer: felder definieren
	add_action( 'login_enqueue_scripts', 'elschnet_login_css' ); 			// css änderungen laut customizer ausgeben
	add_filter( 'login_message', 'elschnet_login_message');					// login message laut customizer ausgeben
	add_filter( 'login_footer', 'elschnet_login_copyright' );				// copyright customizer (nicht) ausgeben
			
		
	// hilfsfunktionen
	include_once( $dir . 'helpers.php' );
	add_filter( 'mce_buttons','elschnet_wysiwyg_editor');					// visueller editor: button nextpage
	#add_action( 'wp_footer', 'elschnet_devmode_warnung' );					// hinweis bei WP_DEBUG=true
	add_filter( 'style_loader_src', 'elschnet_remove_wp_ver', 9999 );		// keine versionsnummern an css+js
	add_filter( 'script_loader_src', 'elschnet_remove_wp_ver', 9999 );		// keine versionsnummern an css+js
	add_filter( 'heartbeat_settings', 'elschnet_heartbeat_settings' ); 		// heartbeat verlangsamen
	
	
	// backend funktionen
	include_once( $dir . 'backend_functions.php' );
	add_action( 'admin_enqueue_scripts', 'elschnet_admin_style' );			// backend.css laden
		
	
	if ( !is_admin() ) {
		
		// scripte laden
		include_once( $dir . 'frontend_scripts.php' );
		add_action( 'wp_enqueue_scripts', 'elschnet_enqueue_script', 20 );
		
		
		// styles laden
		include_once( $dir . 'frontend_styles.php' );
		add_action( 'wp_footer', 'elschnet_enqueue_style' );				// styles im footer lasen
		#add_filter( 'wp_resource_hints', 'elschnet_style_preload', 10,2 );	// externen style vorausladen
		// stylesheet wird im footer geladen
		add_action( 'wp_head', 'elschnet_header_css', 100 );				// criticalpath.css im head laden
		// elschtodo: criticalpath.css erstellen und add_action aktivieren
			
			
		// general template
		include_once( $dir . 'frontend_general.php' );
		#add_filter( 'body_class', 'elschnet_body_class', 10, 2 ); 			// dem body eine weitere klasse geben
		add_action( 'init', 'elschnet_remove_head_stuff', 0 ); 				// head aufräumen
		add_action( 'init', 'elschnet_remove_xmlrpc_stuff', 0 ); 			// xmlrpc deaktivieren
		add_action( 'init', 'elschnet_remove_api_stuff', 0 ); 				// api deaktivieren
		add_shortcode( 'elschnet_email', 'elschnet_emaillink_shortcode' ); 	// email maskieren
		add_action( 'init', 'elschnet_disable_emojicons' ); 				// keine emojis
		add_filter( 'emoji_svg_url', '__return_false' ); 					// keine emojis		
		add_action( 'init', 'elschnet_disable_feeds', 0 ); 					// alle feeds deaktivieren
		// zusätzlich sollte das Plugin "Disable Embeds" installiert werden, um den oembed-code loszuwerden
		
		// breadcrumbs
		include_once( $dir . 'frontend_breadcrumbs.php' );

		
		// tagcloud
		include_once( $dir . 'frontend_tagcloud.php' );
		add_shortcode( 'elschnet_tagcloud', 'elschnet_tagcloud_shortcode' );

				
		// comments
		include_once( $dir . 'frontend_comments.php' );
		add_action( 'comment_form_top', 'elschnet_topic_closes_in' ); //zeit bis schließung der kommentare
				
		
		// posts
		include_once( $dir . 'frontend_posts.php' );
		add_filter( 'excerpt_more', 'elschnet_excerpt_morelink' );		// excerpt-more-link
		add_filter( 'request', 'elschnet_search_request' );				// leere suche verhindern

	}
}

//elschtodo: todos in wp-config.php beachten!
