<?php defined('ABSPATH') or die("No script kiddies please!");
 
// Theme Name:  elsch-ausgaben
// Author:      Michael Schaefer
// Author URI:  https://elsch.net
// Version:     1.0 (2018-12)
// Text Domain:	elschnet_td
// *******************************
// zeigt eine ausgabe an


// **********************************************
//  session starten
// ********************************************** 
if ( !session_id() ) { session_start(); }



// **********************************************
//  aktueller benutzer
// ********************************************** 
$current_user = wp_get_current_user();
$user = strtolower( $current_user->user_firstname ); 



if ( have_posts() ) { while ( have_posts() ) { the_post();



// **********************************************
//  single daten aufbereiten
// **********************************************
$betrag = get_the_title();

// beschreibung
$beschreibung = wp_strip_all_tags( get_the_content() );
if ( empty($beschreibung) ) { $beschreibung = ' &mdash;'; }
	
// kategorie
$kategorie = get_the_terms( $post->ID, 'kategorie' );
$kategorie_name = $kategorie[0]->name;

// zahlart, budget
$ausgabearten = get_the_terms( $post->ID, 'ausgabeart' );
foreach ( $ausgabearten as $ausgabeart ) {
	switch ( $ausgabeart->slug ) {
		case 'elschnet': $aa = 'elschnet'; break;
		case 'luxus-anja': $aa = 'Luxus Anja'; break;			
		case 'luxus-michael': $aa = 'Luxus Michael'; break;			
		case 'gemeinsam': $aa = 'Gemeinsame Ausgaben'; break;			
		case 'leben': $aa = 'Lebensunterhalt'; break;			
		case 'fixkosten': $aa = 'Fixkosten'; break;
			
		case 'bargeld': $zahlart = 'Bargeld'; break;
		case 'karte-konto': $zahlart = 'Karte/Konto'; break;
}	} 

// währung, umrechnung fremdwährung
$waehrung = get_the_terms( $post->ID, 'waehrung' );
$waehrung_id = $waehrung[0]->term_id;
if ( $waehrung_id != 37 ) { // euro=37
	$fremdwaehrung = true;

	$umrechnungskurs = get_field('a_umrechnung');
	$kurs = str_replace(",", ".", $umrechnungskurs);
	
	$umrechnungskurs = number_format ( $umrechnungskurs, 4,',','.' );
	$originalbetrag = number_format ( $betrag, 2,',','.' );	
	$waehrung = $waehrung[0]->name;
	
	$betrag = $betrag * $kurs;
} 




// **********************************************
//  seitenkopf
// **********************************************
$headline = '<h1>Einzelne Ausgabe</h1>';
$GLOBALS['aktive_nav'] = 0;
get_header(); 

$inc = get_template_directory() .'/parts/header.php';
if ( !@include( $inc ) ) { elsch_include( $inc ); } 



// **********************************************
//  inhalt
// ********************************************** 
echo '<div class="content-body single"><div id="ausgabe" class="readonly">';
	
	
	// zurück
	if ( isset( $_GET['stop'] ) ) {
		$abbr_url = '/monat-einzeln/';
		$abbr_txt = 'zurück';
	} else {
		$abbr_url = 'javascript:window.history.back()';
		$abbr_txt = 'zurück';		
	}
	echo '<button class="abbrechen"><a href="'. $abbr_url .'">'. $abbr_txt .'</a></button><br/><br/><br/>';
	
	// datum
	echo '<div class="field g">Datum:<input class="edit" type="text" value="'. get_the_date('d.m.Y') .'" readonly /></div>';
	
	// betrag, runden auf 2 nachkommastellen
	echo '<div class="field">Betrag:<input type="text" value="';
	echo number_format ( round( $betrag, 2 ), 2,',','.' );
	echo ' &euro;" readonly /></div>';

	// fremdwährung
	if ( isset($fremdwaehrung) ) {
		echo '<div class="field">Originalbetrag:<input type="text" value="'. $originalbetrag .'" readonly /></div>';
		echo '<div class="field">Währung:<input type="text" value="'. $waehrung .'" readonly /></div>';
		echo '<div class="field">Umrechnungskurs:<input type="text" value="'. $umrechnungskurs .'" readonly /></div>';
	} 
		
	// zahlart
	echo '<div class="field g">Zahlart:<input type="text" value="'. $zahlart .'" readonly /></div>';
	
	// budget
	echo '<div class="field">Budget:<input type="text" value="'. $aa .'" readonly /></div>';
	
	// kategorie
	echo '<div class="field g">Kategorie:<input type="text" value="'. $kategorie_name .'" readonly /></div>';
	
	// sonderfall: womo-diesel
	if ( $kategorie[0]->term_id == 22 ) {
		
		// nachrechnen, ob die werte stimmen können
		// mehr als 2 cent plus/minus unterschied
		$rechenwert = round( get_field('a_liter') * get_field('a_betragproliter'), 2);
		if ( ( $rechenwert - 0.02 ) > $betrag || ( $rechenwert + 0.02 ) < $betrag ) {	
			
			echo '<div class="field" style="background:tomato">Dieselgate<div class="textarea">Betrag oder Betrag/Liter oder<br/> Liter ist fehlerhaft. Prüfen!<br/>';
			echo '(rechnerisch: '. number_format ( round( $rechenwert, 2 ), 2,',','.' ) .' &euro;)</div></div>';
			
		}
		
		echo '<div class="field g">Kilometerstand: <input type="text" value="'. number_format( get_field('a_kilometerstand'), 0,',','.' ) .' km" readonly /></div>';
		echo '<div class="field g">Liter: <input type="text" value="'. number_format( get_field('a_liter'), 2,',','.' ) .' &#8467;" readonly /></div>';
		echo '<div class="field g">Betrag/Liter: <input type="text" value="'. number_format( get_field('a_betragproliter'), 3,',','.' ) .' &euro;/&#8467;" readonly /></div>';
		echo '<div class="field g">getankt in: <input type="text" value="'. get_field('a_land') .'" readonly /></div>';
	} 
	
	// kurzbeschreibung grau
	echo '<div class="field">Kurzbeschreibung: <div class="textarea">'. $beschreibung .'</div></label></div>';

	

	
	// datum eintragung/änderung + button: bearbeiten
	if ( get_the_modified_date() != get_the_date() ) {
		$singledate = get_the_modified_date('d.m.Y H:i');
		$datetext = 'geändert am: ';
	} else {
		$singledate = get_the_date('d.m.Y H:i');
		$datetext = 'eingetragen am: ';
	}
	echo '<div class="field g">'. $datetext;
	echo '<span id="savedate"><a href="/ausgabe-aendern/?edit='. $post->ID .'">'. $singledate .' - ändern</a></span></div>';
	
	
	// zurück
	echo '<br/><button class="abbrechen"><a href="'. $abbr_url .'">'. $abbr_txt .'</a></button>';

echo '</div></div>';



}} //if have_posts() und while have_posts() 



get_footer(); ?> 