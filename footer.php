<?php defined('ABSPATH') or die("No script kiddies please!");
 
// Theme Name:  elsch-ausgaben
// Author:      Michael Schaefer
// Author URI:  https://elsch.net
// Version:     1.0 (2018-12)
// Text Domain:	elschnet_td
// *******************************



	// **********************************************
	//  mobile navigation
	// ********************************************** ?>
	<footer class="content-footer"><?php
		if ( wp_is_mobile() ) {	
			$inc = get_template_directory() .'/parts/navigation.php';
			if ( !@include( $inc ) ) { elsch_include( $inc ); } 
		} ?>
	</footer>
	
		
	</div></div>
</main> 
</div><?php //.container


wp_footer(); ?>
</body></html>