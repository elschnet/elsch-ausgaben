<?php defined('ABSPATH') or die("No script kiddies please!");
 
// Theme Name:  elsch-ausgaben
// Author:      Michael Schaefer
// Author URI:  https://elsch.net
// Version:     1.0 (2018-12)
// Text Domain:	elschnet_td
// *******************************

// **********************************************
//  INHALT
//
//  kommentare anzeigen
//  lange worte im kommentartext umbrechen
//  zeit bis schließung de kommentare anzeigen
//  kommentar paginierung
//
// **********************************************



// **********************************************
//  kommentare anzeigen
// **********************************************
function elsch_custom_comments( $comment, $args, $depth ) {
	//@http://blog.josemcastaneda.com/2013/05/29/custom-comment/
    $GLOBALS['comment'] = $comment;
	global $wp_customize;
    switch( $comment->comment_type ) {
        
		case 'pingback' :
        case 'trackback' : ?>
            <li <?php comment_class(); ?> id="comment-<?php comment_ID(); ?>">
				<div class="backlink"><p>
					<?php comment_author_link(); ?>
					<?php 
					if ( !isset( $wp_customize ) ) {
						edit_comment_link( 
							__( 'bearbeiten', 'elschnet_td'), 
							'<span title="'. __( 'Ping bearbeiten', 'elschnet_td' ) .'"',
							'</span>' 
						); 
					} ?>
				</p></div>
			</li><?php break;
		
        default : ?>
            <li <?php comment_class(); ?> id="comment-<?php comment_ID(); ?>">
				<div class="comment-meta">
					<span><?php printf( ( '%s' ), wp_kses_post( sprintf( '%s', get_comment_author_link() ) ) ); ?></span> 
					<time <?php comment_time( 'c' ); ?> class="comment-time"><?php 
						// aktuelle kommentare werden in der form "vor x tagen" angezeigt
						// aktuell = jünger als 14 tage = 1209600 sekunden
						if ( current_time( 'timestamp' ) - get_comment_time( 'U' ) < 1209600 ) {
							printf( _x( 'vor %s', '%s = human-readable time difference', 'elschnet_td' ), human_time_diff( get_comment_time( 'U' ), current_time( 'timestamp' ) ) );
						}					
						// älter als 14 tage
						else {							
							echo ' - ';
							printf( esc_html__( '%1$s', 'elschnet_td' ), get_comment_date());
							echo ' - ';
							printf( esc_html__( '%1$s', 'elschnet_td' ), get_comment_time());
						} 
					?></time>
					
					<a href="<?php echo esc_url( get_comment_link( $comment->comment_ID ) ); ?>">#</a>
																			
					<?php 
					// "direkt antworten" -link
					if ( comments_open () ) { 
						comment_reply_link( array_merge( $args, array( 
							'reply_text' => esc_html__( 'Direkt antworten', 'elschnet_td' ), 
							'depth' => $depth, 
							'max_depth' => $args['max_depth'] 
						) ) );
					} ?>
					
					<?php 
					if ( !isset( $wp_customize ) ) {
						edit_comment_link( 
							__( 'bearbeiten', 'elschnet_td'), 
							'<span title="'. __( 'Kommentar bearbeiten', 'elschnet_td' ) .'">',
							'</span>' 
						); 
					} ?>
				</div>
		
				<div class="comment-text"><?php 
					comment_text(); //die funktion elschnet_wrap_comment_text filtert diese ausgabe

					if ( $comment->comment_approved == '0' ) { ?>
						<p class="comment-awaiting-moderation">
							<?php esc_html_e( 'Dein Kommentar muss noch freigegeben werden.', 'elschnet_td' ); ?>
						</p><?php 
					} ?>
				</div>

			</li>
        <?php 
        break;
    }
}



// **********************************************
//  lange worte im kommentartext umbrechen
// **********************************************
// @ http://stackoverflow.com/questions/2609095/hooking-into-comment-text-to-add-surrounding-tag
function elschnet_wrap_comment_text( $content ) {
	return wordwrap( $content, 45, " ", true); 
}
add_filter('comment_text', 'elschnet_wrap_comment_text');




// **********************************************
//  zeit bis schließung de kommentare anzeigen
// **********************************************
// @http://wpengineer.com/2692/inform-user-about-automatic-comment-closing-time/
// erweitert um get_option( 'close_comments_for_old_posts' )
function elschnet_topic_closes_in() {
    global $post;
    if ( $post->comment_status == 'open' && get_option( 'close_comments_for_old_posts' ) ) {
        $close_comments_days_old = get_option( 'close_comments_days_old' );
        $expires = strtotime( "{$post->post_date_gmt} GMT" ) +  $close_comments_days_old * DAY_IN_SECONDS; ?>
        
		<p class="no-comments"><?php 
			printf( 
				__( 'Kommentare zu diesem Beitrag werden in %s geschlossen.', 'elschnet_td' ),  
				human_time_diff( $expires )
			); ?>
		</p> <?php 
		
    }
}



// **********************************************
//  kommentar paginierung
// **********************************************
function elschnet_comments_pagination() {
	
	$args = array(
		'prev_text'          => '&laquo; '. __( 'Older comments' ),
		'next_text'          => __( 'Newer comments' ) .' &raquo;',
	);

	return the_comments_navigation( $args );
	
}
