<?php defined('ABSPATH') or die("No script kiddies please!");
 
// Theme Name:  elsch-ausgaben
// Author:      Michael Schaefer
// Author URI:  https://elsch.net
// Version:     1.0 (2018-12)
// Text Domain:	elschnet_td
// *******************************

// **********************************************
//  INHALT
//
//  admin style laden
//
// **********************************************


// **********************************************
//  admin style laden
// ********************************************** 
function elschnet_admin_style() {
	// load css into the admin pages
	global $theme_name;
	
	$file = '/assets/css/backend.css'; //relativer pfad + datei
	wp_enqueue_style( 
		$theme_name .'-admin', // id
		elschnet_getversioned( $file ), //url des stylesheets
		FALSE,
		FALSE
	); 
}

