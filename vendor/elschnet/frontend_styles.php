<?php defined('ABSPATH') or die("No script kiddies please!");
 
// Theme Name:  elsch-ausgaben
// Author:      Michael Schaefer
// Author URI:  https://elsch.net
// Version:     1.0 (2018-12)
// Text Domain:	elschnet_td
// *******************************

// **********************************************
//  INHALT
//
//  styles laden
//  criticalpath stylesheet im header laden
//
// **********************************************



// **********************************************
//  styles laden
// ********************************************** 
// @http://code.tutsplus.com/tutorials/loading-css-into-wordpress-the-right-way--cms-20402
// angepassed um versionierung
function elschnet_enqueue_style() {
	// load css into the website's front-end
	global $theme_name;
	
	// bei externen styles sollte ein "dns-prefetch" und "preconnect"
	// im head hinzufügt werden. externe styles in function 
	// "elschnet_external_style_preload" eintragen und entsprechenden 
	// add_filter in functions aktivieren
	
	#elschtodo: schriften lokal speichern und laden
	#@https://kittpress.com/das-theme-beschleunigen-am-beispiel-von-twenty-seventeen/#more-103
	/*
	wp_enqueue_style (
		$theme_name .'-google-fonts', // id
		'https://fonts.googleapis.com/css?family=Didact+Gothic', // url des stylesheets
		array(), // eventuelle abhängigkeiten
		null // version 
	);
	wp_enqueue_style(
		'font-awesome', // id
		'https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css', // url des stylesheets
		array(), // eventuelle abhängigkeiten
		null // version 		
	);
	*/
	
	//wp_enqueue_style( 'dashicons' );
	
	/*$file = '/assets/css/fonts.css'; //relativer pfad + datei
	wp_enqueue_style (
		$theme_name .'-fonts', // id
		elschnet_getversioned( $file ), //url des stylesheets
		array(), // eventuelle abhängigkeiten
		FALSE
	);*/
		
	$file = '/style.css'; //relativer pfad + datei
	wp_enqueue_style(
		$theme_name .'-style', // id
		elschnet_getversioned( $file ), //url des stylesheets
		array(), // eventuelle abhängigkeiten
		FALSE
	);
}


// **********************************************
//  externen style vorausladen
// ********************************************** 
// @https://make.wordpress.org/core/2016/07/06/resource-hints-in-4-6/
// bei externen styles sollte ein "dns-prefetch" und "preconnect"
// im head hinzufügt werden
function elschnet_style_preload( $hints, $relation_type ) {
    if ( 'dns-prefetch' === $relation_type ) {
        $hints[] = '//maxcdn.bootstrapcdn.com';
		$hints[] = '//fonts.googleapis.com';
    } else if ( 'preconnect' === $relation_type ) {
        $hints[] = '//maxcdn.bootstrapcdn.com';
		$hints[] = '//fonts.googleapis.com';
    }
 
    return $hints;
}



// **********************************************
//  criticalpath stylesheet im header laden
// **********************************************
function elschnet_header_css() {
	
	// inline-css, keine versionierung notwendig
	$file = '/assets/css/criticalpath.css';
	
	if ( file_exists( get_template_directory() . $file ) ) {
		$criticalpath = @file_get_contents( get_template_directory_uri().'/assets/css/criticalpath.css' );
		if ( $criticalpath ) {	echo '<style id="criticalpath">'. $criticalpath .'</style>'; }
	}
}
