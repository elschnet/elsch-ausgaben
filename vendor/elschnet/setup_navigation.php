<?php defined('ABSPATH') or die("No script kiddies please!");
 
// Theme Name:  elsch-ausgaben
// Author:      Michael Schaefer
// Author URI:  https://elsch.net
// Version:     1.0 (2018-12)
// Text Domain:	elschnet_td
// *******************************

// **********************************************
//  INHALT
//
//  menüs definieren
//  menü walker
//
// **********************************************



// **********************************************
//  menüs definieren 
// **********************************************
function elschnet_register_menus() {	   
	register_nav_menus(	array(
		'main_nav'	 	=> 'MainNavigation',
		'footer_nav' 	=> 'FooterNavigation',
		'admin_nav'	 	=> 'AdminNavigation',
	)	);	
}



// **********************************************
//  menü walker
// **********************************************
function elschnet_remove_menu_classes( $classes ) {
	// wordpress-klassen wahnsinn entschärfen
	// diese klassen werden entfernt
	$remove_classes = array (
		'menu-item',
		'menu-item-type-taxonomy',
		'menu-item-object-category',
		'current-menu-item',
		'menu-item-type-custom',
		'menu-item-object-custom',
		'menu-item-type-post_type',
		'menu-item-object-page',
		'current-post-ancestor',
		'current-menu-parent',
		//'current-post-parent',
		'current_page_item',		
	);
	
	if ( ! is_array( $classes ) ) {
		return $classes;
	}
	
	foreach ( $classes as $key => $class ) {
		
		if ( in_array( $class, $remove_classes ) ) {
			unset ( $classes[ $key ] );
		}
	}
	
	return $classes;
}


// ursprünglich mal von hier: https://dhue.de/wordpress-links-menu-walker/
// erweitert und angepasst elsch 2017/07
class elschnet_walker_nav extends Walker_Nav_Menu {
	
	function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ) {

		//attribute 
		$attributes = ! empty( $item->attr_title ) ? ' title="' . esc_attr( $item->attr_title ) . '"' : '';
		$attributes .= ! empty( $item->target ) ? ' target="' . esc_attr( $item->target ) . '"' : '';
		$attributes .= ! empty( $item->xfn ) ? ' rel="' . esc_attr( $item->xfn ) . '"' : '';
		$attributes .= ! empty( $item->url ) ? ' href="' . esc_attr( $item->url ) . '"' : '';

		//attribute: class
		if ( isset( $item->classes ) ) {
			$class = implode(" ", elschnet_remove_menu_classes( $item->classes ) );
			if ( $item->current && !in_array('active',$item->classes ) ) { $class .= ' active'; } //active hinzufügen
			$class = str_replace('current-post-parent','parent',$class); //current-post-parent durch parent ersetzen
			$attributes .= ! empty( $class ) ? ' class="' . trim ( $class ) . '"' : '';
		}

		//text zwischen den links
		if ( isset( $args->between_links ) ) {
			$output .= ( $item->menu_order > 1 ) ? $args->between_links : '';
		}

		$output .= '<a' . $attributes . '>';
		if ( isset( $args->link_before ) ) { $output .= $args->link_before; }
		$output .= apply_filters( 'the_title', $item->title, $item->ID );
		if ( isset( $args->link_after ) ) { $output .= $args->link_after; }
		$output .= '</a>';

	}

	function end_el( &$output, $item, $depth = 0, $args = array() ) {

		$output .= '';
	}
}
