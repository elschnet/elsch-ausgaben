<?php defined('ABSPATH') or die("No script kiddies please!");
 
// Theme Name:  elsch-ausgaben
// Author:      Michael Schaefer
// Author URI:  https://elsch.net
// Version:     1.0 (2018-12)
// Text Domain:	elschnet_td
// *******************************

// **********************************************
//  INHALT
//
//  body klasse zuweisen
//  theme autor link
//  head aufräumen 
//  xmlrpc deaktivieren
//  api deaktivieren
//  email-adressen maskieren
//  keine emojis
//  alle feeds deaktivieren
//
// **********************************************
 
 
 
// **********************************************
//  body klasse zuweisen
// **********************************************
function elschnet_body_class( $classes, $class ) {
	
	if ( !in_array( 'elschnet-body', $classes ) ) {
		$classes[] = 'elschnet-body';
	}
	
	return $classes;
}



// **********************************************
//  head aufräumen 
// **********************************************
function elschnet_remove_head_stuff() {
	remove_action ('wp_head', 'wlwmanifest_link');
	remove_action ('wp_head', 'wp_generator');

	// kein shortlink, keine prev/next...
	remove_action('wp_head', 'index_rel_link');	
	remove_action('wp_head', 'parent_post_rel_link', 10, 0);
	remove_action('wp_head', 'start_post_rel_link', 10, 0);
	remove_action('wp_head', 'wp_shortlink_wp_head', 10, 0);
	remove_action('wp_head', 'wp_shortlink_header', 10, 0);
	remove_action('wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);	
	
	//search everything script im head entfernen
	//@ https://wordpress.org/support/topic/unnecessary-script-added-to
	remove_action('wp_head', 'se_global_head');
}



// **********************************************
//  xmlrpc deaktivieren
// **********************************************
function elschnet_remove_xmlrpc_stuff() {	
	remove_action ('wp_head', 'rsd_link');
	add_filter( 'xmlrpc_enabled', '__return_false' );
}



// **********************************************
//  api deaktivieren
// **********************************************
function elschnet_remove_api_stuff() {
	// @ http://websupporter.net/blog/de/so-schaltest-du-die-wordpress-rest-api-ab/
	function only_allow_logged_in_rest_access( $access ) {
		if( ! is_user_logged_in() ) {
			return new WP_Error( 'rest_cannot_access', 'Du kannst nur eingeloggt die REST API nutzen.', array( 'status' => rest_authorization_required_code() ) );
		}
		return $access;
	}
	add_filter( 'rest_authentication_errors', 'only_allow_logged_in_rest_access' );
	
	remove_action( 'wp_head', 'rest_output_link_wp_head', 10 );
	remove_action( 'template_redirect', 'rest_output_link_header', 11 );
	remove_action( 'xmlrpc_rsd_apis', 'rest_output_rsd' );
}




// **********************************************
//  email-adressen maskieren
// **********************************************
// function emaillink auch auf seiten und in artikeln nutzbar machen
// aufruf mit z.B: [email adresse="hallo@elsch.net"]
function elschnet_emaillink( $adresse, $anzeigetext=false ) {
	// emailadresse in ascii verschlüsseln
	// @http://www.php-resource.de/tutorials/read/42/1/
	
	$mail = '';
	for( $i=0; $i < strlen($adresse); $i++ ) { 
		$mail .= '&#'. ord( $adresse[$i] ).';'; 
	} 
	
	// noscript erstellen ( user@domain.de )
	$domain = strstr($adresse, '@'); 	// bleibt "@domain.de" übrig
	$pos = strpos($adresse, '@'); 		// position des @-zeichens
	$user = substr($adresse, 0, $pos); 	// "user", vor dem @-zeichen
	$domain = substr($domain, 1); 		// bleibt "domain.de" übrig, ohne @-zeichen
	$toplevel = strstr($domain, '.'); 	// bleibt ".de" übrig
	$pos = strpos($domain, '.'); 		// position des punkts
	$domain = substr($domain, 0, $pos); // "domain", reiner domainname
	$toplevel = substr($toplevel, 1); 	// "de", top-level-domain
	
	if ( !$anzeigetext ) { 
		$anzeigetext = $mail;
	}
	
	$emaillink  = '<script type="text/javascript">';		
		$emaillink .= 'document.write("\<a href=\"&#109;&#97;&#105;&#108;&#116;&#111;&#58;';
		$emaillink .= $mail .'\" class=\"emaillink\">'. $anzeigetext .'<\/a>");'; 
	$emaillink .= '</script><noscript>';
		$emaillink .= '<span class="emaillink">'. $user .'<em>&copy;</em>';
		$emaillink .= $domain .'<em>&bull;</em>'. $toplevel .'</span>';
	$emaillink .= '</noscript>';
	
	return $emaillink; 
} 

// shortcode email	
function elschnet_emaillink_shortcode( $mail ) {
	// $mail = array( 'adresse'=>'', 'text'=>'' );
	
	$adresse = $mail['adresse'];
	$anzeigetext = '';
	
	if ( isset ( $mail['text'] ) ) { 
		$anzeigetext = $mail['text']; 
	}
	
	$str = elschnet_emaillink( $adresse, $anzeigetext ); 
	
	return $str;
}



// **********************************************
//  keine emojis
// **********************************************
// @http://wordpress.stackexchange.com/a/185578
// @https://plugins.trac.wordpress.org/changeset/1142480/classic-smilies
function elschnet_disable_emojicons() {
	// all actions related to emojis
	remove_action( 'wp_head', 'print_emoji_detection_script', 7 ); 
 	remove_action( 'admin_print_scripts', 'print_emoji_detection_script' ); 
	remove_action( 'wp_print_styles', 'print_emoji_styles' );
	remove_action( 'admin_print_styles', 'print_emoji_styles' ); 
	remove_filter( 'the_content_feed', 'wp_staticize_emoji' ); 
	remove_filter( 'comment_text_rss', 'wp_staticize_emoji' );   
	remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );    
	
	// filter to remove TinyMCE emojis
	add_filter( 'tiny_mce_plugins', 'elschnet_disable_emojicons_tinymce' );
}
function elschnet_disable_emojicons_tinymce( $plugins ) {
	if ( is_array( $plugins ) ) {
		return array_diff( $plugins, array( 'wpemoji' ) );
	} else {
		return array();
	}
}



// **********************************************
//  alle feeds deaktivieren
// **********************************************
function elschnet_disable_feeds() {
	
	function elschnet_disable_feed() {
		wp_die( 
			'<strong>'. get_bloginfo('name') .'</strong><br/>'.
			__('Kein Feed verfügbar. Hier geht es zur <a href="'. 
				get_bloginfo('url') .'">Startseite</a>.', 'elschnet_td') );
	}

	add_action('do_feed', 'elschnet_disable_feed', 1);
	add_action('do_feed_rdf', 'elschnet_disable_feed', 1);
	add_action('do_feed_rss', 'elschnet_disable_feed', 1);
	add_action('do_feed_rss2', 'elschnet_disable_feed', 1);
	add_action('do_feed_atom', 'elschnet_disable_feed', 1);
	add_action('do_feed_rss2_comments', 'elschnet_disable_feed', 1);
	add_action('do_feed_atom_comments', 'elschnet_disable_feed', 1);
	
	remove_action('wp_head', 'feed_links', 2);
	remove_action('wp_head', 'feed_links_extra', 3);
}