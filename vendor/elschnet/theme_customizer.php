<?php defined('ABSPATH') or die("No script kiddies please!");
 
// Theme Name:  elsch-ausgaben
// Author:      Michael Schaefer
// Author URI:  https://elsch.net
// Version:     1.0 (2018-12)
// Text Domain:	elschnet_td
// *******************************

// **********************************************
//  INHALT
//
//  logo upload im customizer
//
// **********************************************



// **********************************************
//  logo upload im customizer
// **********************************************
// @https://getflywheel.com/layout/how-to-add-options-to-the-wordpress-customizer/
function elschnet_customizer_logo( $wp_customize ) {
		
	$wp_customize->add_setting( 'elschnet_customizer_logo' );

	$wp_customize->add_control( new WP_Customize_Image_Control( 
		$wp_customize, 'elschnet_customizer_logo', array(
			'label' 		=> __('Logo hochladen', 'elschnet_td'),
			'description' 	=> __('Das Logo wird in der Sidebar angezeigt und automatisch mit der Startseite verlinkt. Maximale Breite: 280px', 'elschnet_td'),
			'section' 		=> 'title_tagline',
			'settings' 		=> 'elschnet_customizer_logo',
	) ) );
}



