<?php defined('ABSPATH') or die("No script kiddies please!");
 
// Theme Name:  elsch-ausgaben
// Author:      Michael Schaefer
// Author URI:  https://elsch.net
// Version:     1.0 (2018-12)
// Text Domain:	elschnet_td
// *******************************


// **********************************************
//  scripte laden
// ********************************************** 
function elschnet_enqueue_script() {
	global $theme_name;
	
	// eingebautes jquery entfernen
	wp_deregister_script( 'jquery' ); // remove standard jquery
	wp_deregister_script( 'jquery-migrate' ); // remove jquery-migrate
	
	
	// theme in entwicklung oder live?
	$wp_debug = defined( 'WP_DEBUG' ) && WP_DEBUG;
	
	// seite noch in entwicklung
	if ( $wp_debug ) {
		
		// mein jquery laden
		$file = '/_js_nicht_komprimiert/jquery-3.3.1.min.js'; //url des scriptdatei
		wp_enqueue_script( 
			'jquery', //id	
			// id darf nicht anders als "jquery" lauten, sonst funktioniert customizer->widgets nicht
			elschnet_getversioned( $file ),
			FALSE, // eventuelle abhängigkeiten
			null, //version
			true //im footer laden
		); 
		/*
		$file = '/_js_nicht_komprimiert/markerclusterer.js'; //relativer pfad + datei
		wp_enqueue_script( 
			$theme_name .'-markerclusterer', //id
			elschnet_getversioned( $file ), //url des scriptdatei
			array( 'jquery' ), // eventuelle abhängigkeiten
			FALSE,
			true //im footer laden
		);
		*/
		$file = '/_js_nicht_komprimiert/jquery.modal.min.js'; //relativer pfad + datei
		wp_enqueue_script( 
			$theme_name .'-modal', //id
			elschnet_getversioned( $file ), //url des scriptdatei
			array( 'jquery' ), // eventuelle abhängigkeiten
			FALSE,
			true //im footer laden
		);
		
		$file = '/_js_nicht_komprimiert/elschnet.js'; //relativer pfad + datei
		wp_enqueue_script(
			$theme_name .'-script', //id
			elschnet_getversioned( $file ), //url des scriptdatei
			array( 'jquery' ), // eventuelle abhängigkeiten
			FALSE,
			true //im footer laden
		);
	}
	
	// seite ist produktiv
	else {
			
		$file = '/assets/js/elschnet.min.js'; //relativer pfad + datei
		wp_enqueue_script(
			$theme_name .'-script', //id
			elschnet_getversioned( $file ), //url des scriptdatei
			FALSE, // eventuelle abhängigkeiten
			FALSE,
			true //im footer laden
		);
		
	}
}

