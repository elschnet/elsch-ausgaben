<?php defined('ABSPATH') or die("No script kiddies please!");
 
// Theme Name:  elsch-ausgaben
// Author:      Michael Schaefer
// Author URI:  https://elsch.net
// Version:     1.0 (2018-12)
// Text Domain:	elschnet_td
// *******************************

// **********************************************
//  tagcloud
// **********************************************
function elschnet_tagcloud( $args=FALSE ) {
	
	// standards, falls nicht definiert wurde
	if ( !$args ) {
		$args = array(
			'smallest'                  => 1.5, 
			'largest'                   => 4,
			'unit'                      => 'rem', 
			'number'                    => 45, 
			'title'                     => _e('Die häufigsten Stichworte:', 'elschnet_td'),  
			'taxonomy' 					=> array( 'post_tag', 'zutaten' ),
		); 	
	}
	
	// konstanten, überschreiben ggf. übergebene werte
	else {		
		$args['echo'] = FALSE;
		$args['separator'] = '##';
		$args['format'] = 'flat';
		$args['link'] = 'view';		
	}

	$tagcloud = explode("##", wp_tag_cloud( $args ) );
		
	$tags = '';
	foreach ($tagcloud as $tag) {
		$tags .= $tag .' '; 
	}

	$out  = '<div class="tagcloud">';
		if ( isset( $title ) ) { $out .= '<p>'. $title .'</p>'; }
		$out .= $tags;
	$out .= '</div>';
	
	return $out;
}


// shortcode tagcloud
function elschnet_tagcloud_shortcode( $atts ) {
	extract( shortcode_atts(
		array(
			'order' => 'rand',
			'number' => '45',
		), $atts )
	);
	return elschnet_tagcloud( $order, $number );
}