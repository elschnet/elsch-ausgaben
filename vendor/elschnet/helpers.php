<?php defined('ABSPATH') or die("No script kiddies please!");
 
// Theme Name:  elsch-ausgaben
// Author:      Michael Schaefer
// Author URI:  https://elsch.net
// Version:     1.0 (2018-12)
// Text Domain:	elschnet_td
// *******************************

// **********************************************
//  INHALT
//
//  visueller editor: +nextpage
//  hinweis bei WP_DEBUG=true
//  keine versionsnummern an css+js
//  versionsnummern anhand timestamp
//  heartbeat verlangsamen
//  elschsign in codeansicht
//  theme-info + resourcen
//
// **********************************************	



// **********************************************
//  visueller editor: +nextpage
// **********************************************
// @https://wordpress.stackexchange.com/a/102171
function elschnet_wysiwyg_editor( $mce_buttons ) {
    
	// add <!-- next-page --> button to tinymce
	$pos = array_search('wp_more',$mce_buttons,true);
    
	if ($pos !== false) {
        $tmp_buttons = array_slice($mce_buttons, 0, $pos+1);
        $tmp_buttons[] = 'wp_page';
        $mce_buttons = array_merge($tmp_buttons, array_slice($mce_buttons, $pos+1));
    }
	
    return $mce_buttons;
}


// **********************************************
//  hinweis bei WP_DEBUG=true
// **********************************************
function elschnet_devmode_warnung( ) {
	
	// theme in entwicklung oder live?
	$wp_debug = defined( 'WP_DEBUG' ) && WP_DEBUG;
	
	//nur admin anzeigen
	if ( $wp_debug && current_user_can('manage_options') ) { 
	
		// div container
		$warn  = '<div id="devmode_warnung">';
			$warn .= '<a href="#" onclick="$(\'#devmode_warnung\').hide();">';
				$warn .= 'WP_DEBUG = true';
			$warn .= '</a>';
		$warn .= '</div>';
		
		// style
		$warn .= '<style>';
			$warn .= '#devmode_warnung {';
				$warn .= 'position:fixed;';
				$warn .= 'bottom:50px;';
				$warn .= 'left:50px;';
				$warn .= 'padding:10px;';
				$warn .= 'background:yellow;';
			$warn .= '}';
			$warn .= '#devmode_warnung a {';
				$warn .= 'color:#000';
			$warn .= '}';
		$warn .= '</style>';
		
		echo $warn;
	}
}



// **********************************************
//  keine versionsnummern an css+js
// **********************************************
function elschnet_remove_wp_ver( $src ) {
	
    if ( strpos( $src, 'ver=' ) ) {
        $src = remove_query_arg( 'ver', $src );
	} 
	
    return $src;
}


// **********************************************
//  versionsnummern anhand timestamp
// **********************************************
function elschnet_getversioned( $file ) {
	// idee, stark modifiziert 
	// @https://gist.github.com/HechtMediaArts/51ebb155217f55cfff17ad1eb0a6dc98
	global $theme_version;	
	
	// es gibt die datei auf dem server NICHT
	if ( !file_exists( get_template_directory() . $file ) ) {
		return get_template_directory_uri() . $file;
		// ausgabe, um 404 sichtbar zu machen
	}
	
	// versionierung = letzten 6 zahlen des timestamp (speicherdatum)
	$version  = $theme_version .'.';
	$version .= substr( filemtime( get_template_directory() . $file ), -6 ); 			
	
	// nur zahlen!
	$version = str_replace('.', '', $version);
	
	// position von '.js' in file - aus (1) wird (2) 
	// (1) '/assets/js/elschnet.min.js' oder '/assets/js/elschnet.js'
	// (2) '/assets/js/elschnet.min.nnnnn.js' oder '/assets/js/elschnet.nnnnn.js'	
	$pos = strpos( $file, '.js');
	if ( $pos===FALSE) {
		$pos = strpos( $file, '.css');
	}
	
	// string an pos zerschneiden
	$begin = substr($file, 0, $pos+1);
	$end = substr($file, $pos+1);
	
	// komplette url ausgeben
	$out  = get_template_directory_uri();
	$out .= $begin . $version .'.'. $end;
	return $out;
}



// **********************************************
//  heartbeat verlangsamen
// **********************************************
// im normalfall wird alle 15 sekunden ein herzschlag
// ausgeführt, hier wird der wert auf den maximalwert 
// 60 sekunden erhöht
// @https://bueltge.de/wordpress-puls-heartbeat-api/2590/
function elschnet_heartbeat_settings( $settings = array() ) {

	$settings['interval'] = 60;

	return $settings;
}



// **********************************************
//  elschsign in codeansicht
// **********************************************	
function elschnet_elschsign() {

	$asci  = "<!--";
	$asci .= "\n\n";
	$asci .= "           888                   888\n           888                   888\n           888                   888\n   .d88b.  888 .d8888b   .d8888b 88888b.\n  d8P  Y8b 888 88K      d88P\"    888 \"88b\n  88888888 888 \"Y8888b. 888      888  888\n  Y8b.     888      X88 Y88b.    888  888\n   \"Y8888  888  88888P'  \"Y8888P 888  888";
	$asci .= "\n\n\n";
	$asci .= "    design + programmierung\n";
	$asci .= "    micha ELSCH äfer\n";
	$asci .= "    https://elsch.net\n";
	$asci .= "\n";
	$asci .= "-->\n";

	echo $asci;	
}



// **********************************************
//  theme-info + resourcen
// **********************************************
function elschnet_get_theme_info() {
	
	// theme in entwicklung oder live?
	$wp_debug = defined( 'WP_DEBUG' ) && WP_DEBUG;
		
	// nur admin zeigen, wenn in entwicklung
	if ( $wp_debug && current_user_can('manage_options') ) { 
		
		$theme_data 		= wp_get_theme( get_template() );

		$theme  = '<div class="elschnet-themeinfo">';
			$theme .= '<strong>Theme-Info</strong><br/>';
			$theme .= '<dl>';
				$theme .= '<dt>Theme:</dt><dd>'. $theme_data->get( 'Name' ) .'</dd>';
				$theme .= '<dt>ThemeURI:</dt><dd>'. $theme_data->get( 'ThemeURI' ) .'</dd>';
				$theme .= '<dt>Version:</dt><dd>'. $theme_data->get( 'Version' ) .'</dd>';
				$theme .= '<dt>TextDomain:</dt><dd>'. $theme_data->get( 'TextDomain' ) .'</dd>';
				$theme .= '<dt>Author:</dt><dd>'. $theme_data->get( 'Author' ) .'</dd>';
				$theme .= '<dt>AuthorURI:</dt><dd>'. $theme_data->get( 'AuthorURI' ) .'</dd>';
			$theme .= '</dl>';
		$theme .= '</div>';
		
		$recource = sprintf(
			'<div class="elschnet-themeinfo"><strong>Query Memory Parameters</strong><br/><dt>&nbsp;</dt><dd>%d queries in %.4f seconds using %.1fMB memory, gesamt %4$sB</dd></div>',
			get_num_queries(),
			str_replace(',', '.', timer_stop( 0, 4 ) ),
			memory_get_peak_usage() / 1024 / 1024,
			ini_get('memory_limit')
		);	
		

		return $theme . $recource;	
	}
}

// **********************************************
//  theme-info + resourcen
// **********************************************
function elschnet_empty_sidebar( $sidebar, $class='box1' ) {	
	// gibt eine meldung zurück, wenn eine sidebar noch keinen inhalt hat
	// meldung erscheint nur für administratoren
	
	$msg = '';
	
	if ( !is_active_sidebar( $sidebar ) ) {
				
		if ( $class ) { $msg .= '<div class="'. $class .'">'; }
			$msg .= '<code>'. $sidebar .'</code> ';
		if ( $class ) { $msg .= '</div>'; }
		
	}
		
	if ( current_user_can('manage_options') ) {
		return $msg;
	}

}

