<?php defined('ABSPATH') or die("No script kiddies please!");
 
// Theme Name:  elsch-ausgaben
// Author:      Michael Schaefer
// Author URI:  https://elsch.net
// Version:     1.0 (2018-12)
// Text Domain:	elschnet_td
// *******************************

 

// **********************************************
//	social share links
// **********************************************
// @https://goo.gl/pfGq1H

function elschnet_share_links( Array $args = array() ){
	$default_args = array(
		'before'        => '<div class="social-share">',
		'after'         => '</div>',
		'before_link'   => '',
		'after_link'    => '',
		'link'          => '<a href="%1$s" title="%2$s"><i class="fa %3$s"></i></a>',
		'networks'      => array(
			array(
				'name'	=> 'google+',
				'link'	=> '//plusone.google.com/_/+1/confirm?hl=de&url=%s',
				'class'	=> 'fa-google-plus'
			),
			array(
				'name'	=> 'facebook',
				'link'	=> '//www.facebook.com/sharer.php?u=%s',
				'class'	=> 'fa-facebook'
			),
			array(
				'name'	=> 'twitter',
				'link'	=> '//twitter.com/share?url=%s',
				'class'	=> 'fa-twitter'
			),
		)
	);
	$rtn = apply_filters( 'pre_t30_get_social_share_links', FALSE, $args, $default_args );
	if ( $rtn !== FALSE )
		return $rtn;
	$args = wp_parse_args( $args, $default_args );
	$args = apply_filters( 't30_get_social_share_links_args', $args );
	$the_permalink = get_permalink();
	$markup = $args[ 'before' ];
	foreach ( $args[ 'networks' ] as $network ) {
		$link   = sprintf( $network[ 'link' ], $the_permalink );
		$title  = sprintf(
			_x( 'Share on %s', 'The Share-Link in t30_get_social_share_links', 't30' ),
			ucfirst( $network[ 'name' ] )
		);
		$class = $network[ 'class' ];
		$markup .= $args[ 'before_link' ];
		$markup .= sprintf(
			$args[ 'link' ],
			$link,
			$title,
			$class
		);
		$markup .= $args[ 'after_link' ];
	}
	$markup .= $args[ 'after' ];
	
	echo $markup;
}