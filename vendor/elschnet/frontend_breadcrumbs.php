<?php defined('ABSPATH') or die("No script kiddies please!");
 
// Theme Name:  elsch-ausgaben
// Author:      Michael Schaefer
// Author URI:  https://elsch.net
// Version:     1.0 (2018-12)
// Text Domain:	elschnet_td
// *******************************

 

// **********************************************
//	breadcrumbs
// **********************************************
// @https://goo.gl/pfGq1H

function elschnet_get_breadcrumbs( Array $args = array() ) {
	global $paged, $post;
	
	$default_args = array(
		'before'            => '<nav id="site-breadcrumbs" class="clearfix" xmlns:v="http://rdf.data-vocabulary.org/#">',
		'after'             => '</nav>',
		'standard'          => '<span typeof="v:Breadcrumb">%s</span>',
		'current'           => '<span typeof="v:Breadcrumb" class="current-breadcrumb">%s</span>',
		'link'              => '<a href="%s" rel="v:url"><span property="v:title">%s</span></a>&nbsp;/&nbsp;',
		'show_home_link'    => true,
		'home_text'         => _x( 'Start', 'Breadcrumb Home Text', 'elschnet_td' )
	);
	
	
	// pre-filter
	$rtn = apply_filters( 'pre_t30_get_breadcrumbs', FALSE, $args, $default_args );
	if ( $rtn !== FALSE )
		return $rtn;
	// merging the args and arg-filter
	$args = wp_parse_args( $args, $default_args );
	$args = apply_filters( 't30_get_breadcrumbs_args', $args );

	
	// init the breadcrumb-array
	$breadcrumbs = array();
	// filling up the breadcrumbs, when we're not on the home- or front-page
	if ( !is_front_page() ) {
		
		// taxonomy archives
		if ( is_tax() || is_category() || is_tag() ) {  
			
			// on custom tax, category or tag pages we've the same logic
			$term       = get_queried_object();
			$term_id    = $term->term_id;
			
			// fetching the parents
			$parent_id = (int)$term->parent;
			if ( $parent_id !== 0 ) {
				while ( $parent_id !== 0 ){
					$parent_term = get_term( $parent_id, $term->taxonomy );
					if ( is_wp_error( $parent_term ) )
						break;
					
					// insert on first position
					array_unshift(
						$breadcrumbs,
						array(
							'title'     => $parent_term->name,
							'link'      => get_term_link( $parent_term->term_id, $parent_term->taxonomy )
						)
					);
					$parent_id = (int)$parent_term->parent;
				}
			}
			
			
			// einzelne Zutat
			if ( is_tax( 'zutaten' ) ) {
				$breadcrumbs[] = array(
					'title'     => 'Womoküche',
					'link'      => '/womokueche/'
				);
			}
			
			
			// einzelnes Land
			if ( is_tax( 'land' ) ) {
				$breadcrumbs[] = array(
					'title'     => 'Länder',
					'link'      => '/laender/'
				);
			}
			
			
			// adding the current term
			$breadcrumbs[] = array(
				'title'     => $term->name,
				'link'      => get_term_link( $term )
			);
		}
		
		// day-archive
		else if ( is_day() ) {           
			$year   = get_the_time( 'Y' );
			$month  = get_the_time( 'm' );
			$day    = get_the_time( 'd' );
			// year link
			$breadcrumbs[] = array(
				'link'  => get_year_link( $year ),
				'title' => get_the_time( 'Y' )
			);
			// month link
			$breadcrumbs[] = array(
				'link'  => get_month_link( $year, $month ),
				'title' => get_the_time( 'F' )
			);
			// day link
			$breadcrumbs[] = array(
				'title'     => $day,
			    'link'      => get_day_link( $day, $month, $year )
			);
		}
		
		// month-archive
		else if ( is_month() ) {         
			$year   = get_the_time( 'Y' );
			$month  = get_the_time( 'm' );
			// year link
			$breadcrumbs[] = array(
				'link'  => get_year_link( $year ),
				'title' => $year
			);
			
			// month link
			$breadcrumbs[] = array(
				'title'     => get_the_time( 'F' ), // month-name
				'link'      => get_month_link( $year, $month ),
			);
		}
		
		// year-archive
		else if ( is_year() ) {			
			$year   = get_the_time( 'Y' );
			$breadcrumbs[] = array(
				'title'     => $year,
				'link'      => get_year_link( $year )
			);
		}
		
		// attachment-page
		else if ( is_attachment() ) {  
			
			if ( $post->post_parent ) {
				
				$breadcrumbs[] = array(
					'title'     => get_the_title( $post->post_parent ),
					'link'      => get_permalink( $post->post_parent )
				);
			}
		
			$breadcrumbs[] = array(
				'title'     => get_the_title(),
			    'link'      => get_permalink()
			);
		}
		
		
		// post-type archive
		else if ( is_post_type_archive() ) {
			
			// fahrtenbuch, ausgabe, anschaffung, stellplatz
			// mit adminlink
			if ( is_post_type_archive( 
				array( 'fahrtenbuch', 'ausgabe', 'anschaffung', 'stellplatz' ) ) ) {
			
				$breadcrumbs[] = array(
					'title'     => 'MAXplus',
					'link'      => home_url( 'maxplus' )
				);	
			}
			
			
			$cpturl = parse_url( get_permalink() );
			$cpturl = explode( '/', $cpturl['path'] );	
			$breadcrumbs[] = array(
				'title'     => ucfirst( get_post_type() ),
			    'link'      => '/'. $cpturl[1] .'/' 
			);
		
		}

		
		// single-page
		else if ( is_singular()  ) {        
			
			// fetching the hierarchical taxonomy to the current post_type()
			$filter = array(
				'hierarchical'      => TRUE,
				'show_in_nav_menus' => TRUE
			);
			$the_post_type  = get_post_type();
			$taxonomies     = get_object_taxonomies(
				$the_post_type,
				'objects'
			);
			$taxonomies     = wp_list_filter(
				$taxonomies,
				$filter
			);
			$taxonomies = array_values( $taxonomies );
			
			
			// single in cpt womokueche
			if ( is_singular( 'womokueche' ) ) {
				$breadcrumbs[] = array(
					'title' => 'Womoküche',
					'link'  => '/womokueche/'
				);
			}
			
			// rezeptsuche ist child von womokueche!
			if ( is_page( 'rezeptsuche' ) ) {
				$breadcrumbs[] = array(
					'title' => 'Womoküche',
					'link'  => '/womokueche/'
				);
			}
				
				
			// checking for taxonomies in this post_type
			if ( !empty( $taxonomies ) ) {
				
				// get the first taxonomy
				$taxonomy   = $taxonomies[0]->name;
				
				// get the post id
				$post_id    = get_the_ID();
				
				// get all terms
				$terms =  wp_get_post_terms(
					$post_id,
					$taxonomy
				);
				
				if ( !is_wp_error( $terms ) && ! empty( $terms ) ) {
					// get all ancestor-terms
					$ancestors = get_ancestors(
						$terms[0]->term_id,
						$taxonomy
					);
					
					if( !is_wp_error( $ancestors ) && !empty( $ancestors ) ) {
						foreach( $ancestors as $term_id ) {
							$term = get_term( $term_id, $taxonomy );
							$term_link = get_term_link(
								$term->term_id,
								$taxonomy
							);
							$breadcrumb = array(
								'title' => $term->name,
								'link'  => $term_link
							);
							array_unshift(
								$breadcrumbs,
								$breadcrumb
							);
						}
					}
					$term_link = get_term_link(
						$terms[0]->term_id,
						$taxonomy
					);
					$breadcrumbs[] = array(
						'title' => $terms[0]->name,
						'link'  => $term_link
					);
				}
			}
			
			// Parents von Seiten anzeigen
			if ( is_page() && $post->post_parent ) {
				$breadcrumbs[] = array(
					'title' => get_the_title( $post->post_parent ),
					'link'  => get_permalink( $post->post_parent )
				);	
			}
			
			// last but not least, adding the current single-site
			$breadcrumbs[] = array(
				'title'     => get_the_title(),
			    'link'      => get_permalink()
			);
		}
		
		// blog (bei statischer front-page)
		else if ( !is_front_page() && is_home() ) {			
			$page_for_posts = get_option( 'page_for_posts' );
			
			$breadcrumbs[] = array(
				'title'     => get_the_title( $page_for_posts ),
			    'link'      => get_permalink( $page_for_posts )
			);
		}
			
		// search
		else if ( is_search() ) {    
			$title = __( 'Suche', 'elschnet_td' );
			$breadcrumbs[] = array(
				'title'     => $title,
			    'link'      => get_search_link()
			);
		}
		
		// author page
		else if ( is_author() ) {    
			global $author;
			$user_data = get_userdata( $author );
			$title = sprintf(
				_x( 'Autor: %s', 'breadcrumb nav item', 'elschnet_td' ),
				$user_data->display_name
			);
			$breadcrumbs[] = array(
				'title'     => $title,
			    'link'      => get_author_posts_url( $user_data->ID, $user_data->user_nicename )
			);
		}
		
		// 404 page
		else if ( is_404() ) {       
			$breadcrumbs[] = array(
				'title'     => _x( 'Fehler 404', 'breadcrumb nav item', 'elschnet_td' )
			);
		}
	}

	// adding the home_link if we activated it
	if ( (bool)$args[ 'show_home_link' ] && !is_front_page() ){
		$breadcrumb = array(
			'title' => $args[ 'home_text' ],
			'link'  => home_url( '/' )
		);
		array_unshift(
			$breadcrumbs,
			$breadcrumb
		);
	}
	
	// last but no least...adding the Page-Number to Breadcrumb
	if( is_paged() ){
		
		if ( is_front_page() ) {
			$breadcrumbs[] = array(
				'title' => $args[ 'home_text' ],
				'link'  => home_url( '/' )
			);
		}
		
		$title = sprintf(
			__( 'Seite %s', 'elschnet_td' ),
			$paged
		);
		$breadcrumbs[] = array(
			'title'     => $title,
			'link'      => get_pagenum_link( $paged )
		);
	}
	
	
	// building the markup
	$markup = $args[ 'before' ];	
	$breadcrumb_count = count( $breadcrumbs ) - 1;
	
	foreach( $breadcrumbs as $k => $breadcrumb ) {
		// the last one is the current one!
		if( $k === $breadcrumb_count ){
			$markup .= sprintf(
				$args[ 'current' ],
				$breadcrumb[ 'title' ]
			);
		}
		else if( isset( $breadcrumb[ 'link' ] ) ){
			$link = sprintf(
				$args[ 'link' ],
				$breadcrumb[ 'link' ],
				$breadcrumb[ 'title' ]
			);
			$markup .= sprintf(
				$args[ 'standard' ],
				$link
			);
		}
	}	
	$markup .= $args[ 'after' ];
	
	
	return apply_filters( 'elschnet_get_breadcrumbs', $markup, $args, $breadcrumbs );

}