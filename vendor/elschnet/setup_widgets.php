<?php defined('ABSPATH') or die("No script kiddies please!");
 
// Theme Name:  elsch-ausgaben
// Author:      Michael Schaefer
// Author URI:  https://elsch.net
// Version:     1.0 (2018-12)
// Text Domain:	elschnet_td
// *******************************

// **********************************************
//  INHALT
//
//  bearbeiten-link für widgets, menüs...
//  widget-bereiche definieren
//  einfaches Textwidget [elschnet textonly]
//  get_dynamic_sidebar + #jahr# umwandeln
//
// **********************************************


// **********************************************
//  bearbeiten-link für widgets, menüs...
// **********************************************
function elschnet_widget_bearbeiten( $name, $type='widget' ) {
	//@https://wordpress.stackexchange.com/a/214491
	global $wp_customize;
	
	// link zur anzeigen, wenn user-rolle stimmt und 
	// customizer nicht bereits offen ist
	if ( current_user_can('edit_theme_options') && !isset( $wp_customize ) ) {
		switch ( $type ) {
			case 'nav_menus': 		
				$autofocus = 'panel'; 	
				$title = __('Menü: ', 'elschnet_td'); 
				break;
			case 'title_tagline': 	
				$autofocus = 'section'; 	
				$title = __('Website-Information: ', 'elschnet_td'); 
				break;
			case 'blogdescription'; 
				$autofocus = 'control';	
				$title = __('Website-Information: ', 'elschnet_td'); 
				break;
			case 'elschnet_customizer_logo'; 
				$autofocus = 'control';	
				$title = __('Website-Information: ', 'elschnet_td'); 
				break;
			default:
				$type = 'widgets';
				$autofocus = 'panel'; 	
				$title = __('Widget: ', 'elschnet_td'); 
		}
		
		$bea  = '<span class="edit-link-inline"><a class="post-edit-link" href="';
		$bea .= admin_url( "customize.php?autofocus[$autofocus]=$type" );
		$bea .= '" title="'. $title .'\''. $name .'\' bearbeiten">';
		$bea .= __('bearbeiten', 'elschnet_td') .'</a></span>';
		
		return $bea;
	}	
}


// **********************************************
//  widget-bereiche definieren
// **********************************************
function elschnet_register_sidebars() {
	
	register_sidebar(array(
		'id'			=> 'header',
		'name' 			=> __('Header', 'elschnet_td'),		
		'description' 	=> __('Logo (oder Text) oben links, als Link zur Startseite', 'elschnet_td'),
		'before_widget' => '',
		'after_widget' 	=> '',
		'before_title' 	=> '<span>',
		'after_title' 	=> ' '. elschnet_widget_bearbeiten( __('Header', 'elschnet_td') ). '</span>',
	));

	register_sidebar(array(
		'id'			=> 'sidebar',
		'name' 			=> __('Sidebar Unterseiten', 'elschnet_td'),		
		'description' 	=> __('Inhalt der Sidebar.', 'elschnet_td'),
		'before_widget' => '<div>',
		'after_widget' 	=> '</div>',
		'before_title' 	=> '<p>',
		'after_title' 	=> ' '. elschnet_widget_bearbeiten( __('Sidebar', 'elschnet_td') ) .'</p>',
	));	
		
	register_sidebar(array(
		'id'			=> 'sidebar-startseite',
		'name' 			=> __('Sidebar Startseite', 'elschnet_td'),		
		'description' 	=> __('Sidebar auf der Startseite.', 'elschnet_td'),
		'before_widget' => '<div>',
		'after_widget' 	=> '</div>',
		'before_title' 	=> '<p>',
		'after_title' 	=> ' '. elschnet_widget_bearbeiten( __('Sidebar', 'elschnet_td') ) .'</p>',
	));	
		
	register_sidebar(array(
		'id'			=> 'footer-copyright',
		'name' 			=> __('Text am Seitenende', 'elschnet_td'),
		'description' 	=> __('Raum für Copyright-Vermerk am Seitenende. Wird nur angezeigt, wenn es auch ein Footer-Menü gibt.', 'elschnet_td'),
		'before_widget' => '',
		'after_widget' 	=> '',
		'before_title' 	=> '',
		'after_title' 	=> '',
	));	

}



// **********************************************
//  einfaches Textwidget [elschnet textonly]
// **********************************************
// kräftig modifiziert, ursprünglich von hier:
// @ http://www.wpbeginner.com/wp-tutorials/how-to-create-a-custom-wordpress-widget/
class elschnet_widget extends WP_Widget {

	function __construct() {
		parent::__construct(
		// Base ID of your widget
		'elschnet_textonly', 

		// Widget name will appear in UI
		__('elschnet Copyright', 'elschnet_td'), 

		// Widget description
		array( 'description' => __( 'Für Copyright-Vermerk am Seitenende. Die Jahreszahl bleibt auf Wunsch automatisch aktuell.', 'elschnet_td' ), ) 
		);
	}

	// Creating widget front-end
	// This is where the action happens
	public function widget( $args, $instance ) {
		$title = apply_filters( 'widget_title', $instance['title'] );
		echo $title;
	}
			
	// Widget Backend 
	public function form( $instance ) {
		if ( isset( $instance[ 'title' ] ) ) {
			$title = $instance[ 'title' ];
		}
		else {
			$title = __( '&copy; #jahr# Michael Schäfer elsch.net', 'elschnet_td' );
		}		
		// Widget admin form
		?>
		<p>
		<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Text:' ); ?></label> 
		<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
		<br/><?php _e('Der Ausdruck <strong>#jahr#</strong> wird zur aktuellen Jahrzahl umgewandelt.', 'elschnet_td'); ?>				
		</p>
		<?php 
	}
		
	// Updating widget replacing old instances with new
	public function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
		
		return $instance;
	}
} // Class elschnet_widget ends here
// Register and load the widget
function elschnet_load_widget() { register_widget( 'elschnet_widget' ); }




// **********************************************
//  get_dynamic_sidebar + #jahr#
// **********************************************
// die WP-Funktion dynamic_sidebar brüllt direkt raus,
// um #jahr# ersetzen zu können, brauche ich einen return
if ( !function_exists('get_dynamic_sidebar') ) {
	function get_dynamic_sidebar($index = 1) {
		$sidebar_contents = "";
		ob_start();
		dynamic_sidebar($index);
		$sidebar_contents = ob_get_clean();
		$aktuelles_jahr = date('Y');
		$sidebar_contents = str_replace('#jahr#', $aktuelles_jahr, $sidebar_contents);
		return $sidebar_contents;
	}
}