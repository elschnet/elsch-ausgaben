<?php defined('ABSPATH') or die("No script kiddies please!");
 
// Theme Name:  elsch-ausgaben
// Author:      Michael Schaefer
// Author URI:  https://elsch.net
// Version:     1.0 (2018-12)
// Text Domain:	elschnet_td
// *******************************

// **********************************************
//  INHALT
//
//  bildgrößen festlegen
//  bildbeschreibung anzeigen
//  thumbnails nachschärfen
//  gallery aussehen ändern
//  standards wp-gallery festlegen
//  lazyload images
//  lazyload videos
//  lazyload videos: script im footer
//
// **********************************************



// **********************************************
//  bildgrößen festlegen
// ********************************************** 
// @https://goo.gl/CQCB7g
function elschnet_register_image_sizes() {
	
	$image_sizes = array(
		#'post-thumbnail'	=> array( 'width' => 650, 'height' => 300, 'crop' => TRUE ),
		#'lomo'				=> array( 'width' => 220, 'height' => 180, 'crop' => FALSE ),
	);
	
	$image_sizes = apply_filters( 'elschnet_image_sizes', $image_sizes );
	
	foreach ( $image_sizes as $id => $args ) {
		add_image_size( $id, $args[ 'width' ], $args[ 'height' ], $args[ 'crop' ] );
	}
}


// **********************************************
//  werte eines bildes
// ********************************************** 
// @https://wordpress.stackexchange.com/a/125613
function wp_get_attachment( $attachment_id ) {

	$attachment = get_post( $attachment_id );
	
	return array(
		'alt' => get_post_meta( $attachment->ID, '_wp_attachment_image_alt', true ),
		'caption' => $attachment->post_excerpt,
		'description' => $attachment->post_content,
		'href' => get_permalink( $attachment->ID ),
		'src' => $attachment->guid,
		'title' => $attachment->post_title
	);
}


// **********************************************
//  bildbeschreibung anzeigen
// **********************************************  
// @https://goo.gl/CQCB7g
function elschnet_caption_shortcode( $output, $attr, $content ) {
	
	$output = '<figure class="wp-caption ' . $attr[ 'align' ] . '">';
		$output .= $content;
		$output .= '<figcaption class="wp-caption-text">' . $attr[ 'caption' ] . '</figcaption>';
	$output .= '</figure>';
	
	return $output;
}



// **********************************************
//  thumbnails nachschärfen
// ********************************************** 
// @https://goo.gl/gNLVwv
// beim erzeugen der bildgrößen entstehen unschärfen
function elschnet_bilder_schaerfen( $resized_file ) {

	$image = wp_load_image( $resized_file );
	if ( !is_resource( $image ) ) {
		return new WP_Error( 'error_loading_image', $image, $file );
	}

	$size = @getimagesize( $resized_file );
	
	if ( !$size ) {
		return new WP_Error('invalid_image', __('Could not read image size'), $file);
	}
	
	list($orig_w, $orig_h, $orig_type) = $size;

    switch ( $orig_type ) {
		
        case IMAGETYPE_JPEG:
            $matrix = array(
                array(-1, -1, -1),
                array(-1, 16, -1),
                array(-1, -1, -1),
            );

            $divisor = array_sum(array_map('array_sum', $matrix));
            $offset = 0; 
            imageconvolution($image, $matrix, $divisor, $offset);
            imagejpeg($image, $resized_file,apply_filters( 'jpeg_quality', 90, 'edit_image' ));
            break;
			
        case IMAGETYPE_PNG:
        case IMAGETYPE_GIF:
            return $resized_file;
    }

    return $resized_file;
}  



// **********************************************
//  gallery aussehen ändern
// ********************************************** 
// @https://stackoverflow.com/a/23410472 (stark modifiziert)
function elschnet_custom_gallery( $output, $attr) {
    global $post, $wp_locale;

    static $instance = 0;
    $instance++;

    // We're trusting author input, so let's at least make sure it looks like a valid orderby statement
    if ( isset( $attr['orderby'] ) ) {
        $attr['orderby'] = sanitize_sql_orderby( $attr['orderby'] );
        if ( !$attr['orderby'] )
            unset( $attr['orderby'] );
    }

    extract(shortcode_atts(array(
        'order'      => 'ASC',
        'orderby'    => 'menu_order ID',
        'id'         => $post->ID,
        'itemtag'    => 'picture',
        //'icontag'    => 'div',
        'captiontag' => 'div',
        'columns'    => 3,
        'size'       => 'thumbnail',
        'include'    => '',
        'exclude'    => ''
    ), $attr));

    $id = intval($id);
    if ( 'RAND' == $order )
        $orderby = 'none';

    if ( !empty($include) ) {
        $include = preg_replace( '/[^0-9,]+/', '', $include );
        $_attachments = get_posts( array('include' => $include, 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => $order, 'orderby' => $orderby) );

        $attachments = array();
        foreach ( $_attachments as $key => $val ) {
            $attachments[$val->ID] = $_attachments[$key];
        }
    } elseif ( !empty($exclude) ) {
        $exclude = preg_replace( '/[^0-9,]+/', '', $exclude );
        $attachments = get_children( array('post_parent' => $id, 'exclude' => $exclude, 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => $order, 'orderby' => $orderby) );
    } else {
        $attachments = get_children( array('post_parent' => $id, 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => $order, 'orderby' => $orderby) );
    }

    if ( empty($attachments) )
        return '';

    if ( is_feed() ) {
        $output = "\n";
        foreach ( $attachments as $att_id => $attachment )
            $output .= wp_get_attachment_link($att_id, $size, true) . "\n";
        return $output;
    }
	
	//extra klasse für die lightbox
	//so kann im backend tatsächlich entschieden werden, ob
	//attachment-seite oder lightbox angezeigt wird
	$lightbox_class = '';
	if ( isset($attr['link']) && $attr['link'] == 'file' ) {
		$lightbox_class = ' elsch-lightbox';
	}	

    $itemtag = tag_escape($itemtag);
    $captiontag = tag_escape($captiontag);
    $columns = intval($columns);
    $selector = "gallery-{$instance}";	
	$output = "<div id='$selector' class='gallery{$lightbox_class} gallery-{$columns}col galleryid-{$id}'>";

    $i = 0;
    foreach ( $attachments as $id => $attachment ) {
		
		// es soll keine zwei br hintereinander geben
		$letztezeile = false;
		
		//was soll noch click auf thumbnail angezeigt werden?
		//attachment-seite oder image-datei
		//bei lightbox = image-datei!
		//wird im wp backend bei den gallerie einstellungen ausgewählt
		if ( isset($attr['link']) && $attr['link'] == 'file' ) {			
			$link = wp_get_attachment_link($id, $size, false, false);			
			//statt dem original (full) soll nur die bildgröße large in lightbox erscheinen
			//@https://wpquestions.com/Modify_Gallery_Shortcode_to_open_large_images_instead_of_full/8562
			$values = array_values( wp_get_attachment_image_src( $id, 'large' ) );
			$imgsrc = array_shift( $values );
			$description = wp_get_attachment( $id );
			$title = esc_html( $description['caption'] );
			if ( $description['description'] ) {
				$title .= ': '. esc_html( $description['description'] );
			}
			$pattern = "/href\s*\=\s*[\'\"]?([^\'\"]*)[\'\"]?/i";
			$replace = "href='". $imgsrc ."' aria-label='". $title ."'";
			$link = preg_replace( $pattern, $replace, $link );			
		} 
		elseif ( isset($attr['link']) && $attr['link'] == 'none' ) {
			$link = wp_get_attachment_image($id, $size);
		}
		else {
			$link = wp_get_attachment_link($id, $size, true, false);
		}

        //$output .= "<{$itemtag} class='gallery-item'>";
		$output .= "<{$itemtag}>";
        
		if ( isset($icontag) ) {
			$output .= "
				<{$icontag} class='gallery-icon'>
					$link
				</{$icontag}>";
		} else {
			$output .= $link;
		}
		
		/*
		//if ( $captiontag && trim($attachment->post_excerpt) ) {
		if ( $captiontag ) {
            $output .= "<{$captiontag} class='caption'>";
			$output .= wptexturize($attachment->post_excerpt) .' ';
            $output .= "</{$captiontag}>";
        }*/
		
        $output .= "</{$itemtag}>";
		
        if ( $columns > 0 && ++$i % $columns == 0 ) {
            $output .= '<br style="clear: both" />';
			
			// es soll keine zwei br hintereinander geben
			$letztezeile = true;
		}
    }

	// es soll keine zwei br hintereinander geben
    if ( !$letztezeile ) { $output .= "<br style='clear: both;' />"; }
	
    $output .= "</div>\n";

    return $output;
}



// **********************************************
//  standards wp-gallery festlegen
// ********************************************** 
// Q: https://wordpress.stackexchange.com/a/203320
function elsch_gallery_defaults( $settings ) {
    
	$settings['galleryDefaults']['columns'] = 5;
	$settings['galleryDefaults']['link'] = 'file';
	//$settings['galleryDefaults']['size'] = 'medium';
    
	return $settings;
}



// **********************************************
//  lazyload images
// **********************************************
// @ https://fastwp.de/4747/
function elschnet_add_image_load( $content ) { 
	
	if( is_feed() || is_preview() || ( function_exists( 'is_mobile' ) && is_mobile() ) ) {
		return $content;
	}
	
	if ( false !== strpos( $content, 'data-src' ) ) {
		return $content; 
	}
	
	$placeholder_image = ('data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs='); 
	
	$content = preg_replace( 
		'#<img([^>]+?)src=[\'"]?([^\'"\s>]+)[\'"]?([^>]*)>#', sprintf( '<img${1}src="%s" data-src="${2}"${3}><noscript><img${1}src="${2}"${3}></noscript>', $placeholder_image ), 		$content ); 
	
	return $content; 
} 



// **********************************************
//  lazyload videos
// **********************************************
// @ https://fastwp.de/5995
function elschnet_add_video_load( $content ) { 
	if( is_feed() || is_preview() || ( function_exists( 'is_mobile' ) && is_mobile() ) ) {
		return $content; 
	}
	
	if ( false !== strpos( $content, 'data-src' ) ) {
		return $content; 
	}
	
	$placeholder_image = ('data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs='); 
	
	$content = preg_replace( 
		'#<iframe([^>]+?)src=[\'"]?([^\'"\s>]+)[\'"]?([^>]*)>#', sprintf( '<iframe${1}src="%s" data-src="${2}"${3}><noscript><iframe${1}src="${2}"${3}></noscript>', $placeholder_image ), $content ); 
		
	return $content; 
}


// **********************************************
//  lazyload videos: script im footer
// **********************************************
function elschnet_add_lazyload_script() { 
	echo '<script type="text/javascript">';
	echo 'function lazyload() {';
		
		//images
		echo 'var imgDefer=document.getElementsByTagName("img");';
		echo 'for( var i=0; i<imgDefer.length; i++ ) {';
			echo 'if(imgDefer[i].getAttribute("data-src")) {';
				echo 'imgDefer[i].setAttribute("src",imgDefer[i].getAttribute("data-src"));';
			echo '}';
		echo '}';
		
		// videos
		echo 'var videoDefer=document.getElementsByTagName("iframe");';		
		echo 'for ( var i=0; i<videoDefer.length; i++ ) {';
			echo 'if(videoDefer[i].getAttribute("data-src")) {';
				echo 'videoDefer[i].setAttribute("src",videoDefer[i].getAttribute("data-src"));';
			echo '}';
		echo '}';		
		
	echo '}';	
	echo 'window.onload=lazyload;';
	echo '</script>';
}  
