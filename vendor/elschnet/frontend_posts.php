<?php defined('ABSPATH') or die("No script kiddies please!");
 
// Theme Name:  elsch-ausgaben
// Author:      Michael Schaefer
// Author URI:  https://elsch.net
// Version:     1.0 (2018-12)
// Text Domain:	elschnet_td
// *******************************

// **********************************************
//  INHALT
//
//  beitrags-paginierung
//  link unter excerpt anpassen
//  post-thumbnail
//
// **********************************************


// **********************************************
//  beitrags-paginierung
// **********************************************
function elschnet_get_paginierung( $before=false, $args = array() ) {

	$default = array(
		'mid_size' 	=> 3,
		'prev_text'	=> sprintf('<span title="%s">&laquo;</span>', __('vorherige Seite', 'elschnet_td') ),
		'next_text'	=> sprintf('<span title="%s">&raquo;</span>',	__('nächste Seite', 'elschnet_td') ),
	);
	$args = wp_parse_args( $args, $default );

	$pag  = '<div class="paginierung">';
		if ( !is_paged() ) { $pag .= $before; }
		$pag .= paginate_links( $args );
	$pag .= '</div>';
	
	return $pag;
}



// **********************************************
//  link unter excerpt anpassen
// **********************************************
function elschnet_excerpt_morelink() {
	$morelink = sprintf( '<a href="%1$s#more-%2$s" class="more-link">%3$s</a>',
		esc_url( get_permalink( get_the_ID() ) ),	
		get_the_ID(),
		sprintf( 
			__( '<span class="screen-reader-text">"%s" </span>[&nbsp;weiterlesen&hellip;&nbsp;]', 'elschnet_td' ), 
			get_the_title( get_the_ID() ) 
			)
		);
	return ' '. $morelink;
}


// **********************************************
//  post-thumbnail
// **********************************************
function elschnet_post_thumbnail( $post_id ) {
	
	// in excerpt-ansichten ein eventuell verfügbares icon anzeigen
	$beitrag_excerpt_icon = get_field('beitrag_excerpt_icon');
	if ( $beitrag_excerpt_icon ) {
		$beitrag_excerpt_icon =  wp_get_attachment_image( $beitrag_excerpt_icon, 'thumbnail', '', 
			array( 'class' => 'post-thumbnail' ) );
	}
	if ( is_singular() ) { unset( $beitrag_excerpt_icon ); }
	
	if ( post_password_required() || is_attachment() || 
		 ( !has_post_thumbnail() && !$beitrag_excerpt_icon )) {
		return;
	}
	
	// caption und description des bildes holen
	$description = wp_get_attachment( get_post_thumbnail_id( $post_id ) );

	
	// auf single-seiten als großes bild mit beschriftung
	if ( is_singular() ) { 
					
		$output = '<figure class="wp-caption">';
	
			$output .= get_the_post_thumbnail( null, 'large' ); 

			if ( $description['caption'] || $description['description'] ) {
				$output .= '<figcaption class="wp-caption-text">';
					$output .= esc_html( $description['caption'] );
					if ( $description['caption'] && $description['description'] ) { $output .= ': '; }
					$output .= esc_html( $description['description'] );
				$output .= '</figcaption>';
			}
		
		$output .= '</figure>';			
	} 
	
	
	// sonst als thumbnail ausgeben
	else { 	
		
		$output = '<div class="post-thumbnail">';	
			$output .= '<a href="'. get_the_permalink( $id ) .'">';
				
				
				if ( $beitrag_excerpt_icon ) {
					
					$output .= $beitrag_excerpt_icon;
					
				} else {
				
					$output .= get_the_post_thumbnail( null, 'thumbnail', array( 
							'class' => 'post-thumbnail', 
							'alt' => the_title_attribute( 'echo=0' ),
					) 	); 
					
				}
			
			$output .= '</a>'; 
			$output .= '<div class="caption">'. esc_html( $description['caption'] ) .'</div>';
		$output .= '</div>'; 
		
	}
	
	
	
	return $output;
}



// **********************************************
//  leere suche verhindern
// **********************************************
function elschnet_search_request( $query_vars ) {
    if( isset( $_GET['s'] ) && empty( $_GET['s'] ) ) {
        $query_vars['s'] = "";
		wp_redirect( home_url( "/" ) );
		exit();
    }
    return $query_vars;
}
 


