<?php defined('ABSPATH') or die("No script kiddies please!");
 
// Theme Name:  elsch-ausgaben
// Author:      Michael Schaefer
// Author URI:  https://elsch.net
// Version:     1.0 (2018-12)
// Text Domain:	elschnet_td
// *******************************

// **********************************************
//  INHALT
//
//	options für kategorie-select zurückgeben
//	options für waehrung-select zurückgeben
//	options für zahlart bzw. budget-select zurückgeben
//	adminbar nur für administartoren anzeigen
//	include oder fehlermeldung
//
// **********************************************	


// **********************************************
//	options für kategorie-select zurückgeben
// **********************************************
function kategorie_options( $selected=false, $fixkosten=false, $alle=false ) {
	// wird in index und aendern-loeschen verwendet
	// fixkosten kommt bei "neue fixkosten ausgabe"
	// alle kommt bei "ändern" zum einsatz
	
	
	
	// **********************************************
	// bestimmte kategorien ausschließen
	if ( $fixkosten ) {
		$exclude = array( 7,22,16,2,11,4,17,12,10,20,6,14,13 ); 
		//  7 bus, bahn, bookndrive
		// 22 diesel womo
		// 16 eintritt
		//  2 foto
		// 11 frisör
		//  4 garten, blumen
		// 17 geschenke
		// 12 kleidung
		// 10 kosmetik, drogerie
		// 20 maut
		//  6 restaurant, imbiss, kneipe
		// 14 schreibwaren
		// 13 zeitschriften, bücher		
	} elseif ( $alle ) {
		$exclude = array( 23 );
		// 23 none 
	} else {
		$exclude = array( 23,46,47,48,49 ); 
		// 23 none 
		// 46 kreditrate haus
		// 47 vorsorge
		// 48 versicherung
		// 49 kreditrate womo
	}
	
	
	
	// **********************************************
	// kategorien holen
	$kats = get_terms( array(
		'taxonomy' => 'kategorie',
		'exclude' => $exclude, 
		'hide_empty' => false,
	) );

	
	
	// **********************************************
	// wichtige kategorien oben im select anzeigen
	$wichtige_kats = array( 6,5, );
		//  6 restaurant, imbiss, kneipe
		//  5 lebensmittel
		
	if ( $fixkosten ) {
		$wichtige_kats = array( 46,47,48,49 );
		// 46 kreditrate haus
		// 47 vorsorge
		// 48 versicherung
		// 49 kreditrate womo
	} 
	
	elseif ( $alle ) {
		// bei "ändern" alle alphabetisch anzeigen
		// keine kategorie oben, alle unten
		$wichtige_kats = array();
	}

	
	
	$kat_oben = '';
	$kat_unten = '';
	foreach( $kats as $kat ) {	
		if ( in_array( $kat->term_id, $wichtige_kats ) ) {  // wichtige oben
			$kat_oben .= '<option value="'. $kat->term_id .'"';
			if ( $kat->term_id == $selected ) { $kat_oben .= ' selected'; }
			$kat_oben .= '>'. $kat->name .'</option>';
		} else { // rest unten
			$kat_unten .= '<option value="'. $kat->term_id .'"';
			if ( $kat->term_id == $selected ) { $kat_unten .= ' selected'; }
			$kat_unten .= '>'. $kat->name .'</option>';
		}	
	} 

	// kategorien zusammenbauen
	$kategorien  = '<option value="">Kategorie auswählen *</option>';
	$kategorien .= $kat_oben;
	$kategorien .= '<option value="">--------------</option>';
	$kategorien .= $kat_unten;
	
	
	// bei "ändern" alle alphabetisch anzeigen
	if ( $alle ) { $kategorien = $kat_unten; }
	
	
	return $kategorien;
}


// **********************************************
//	options für waehrung-select zurückgeben
// **********************************************
function waehrung_options( $selected=false ) {
	
	$waehs = get_terms( array(
		'taxonomy' => 'waehrung',
		'exclude' => array( 37 ), // 'euro'
		'hide_empty' => false,
	) );
	
	$waehrungen  = '<option value="37" selected>Euro</option>';

	foreach( $waehs as $waeh ) {	
		$waehrungen .= '<option value="'. $waeh->term_id .'"';
		if ( $waeh->term_id == $selected ) { $waehrungen .= ' selected'; }
		$waehrungen .= '>'. $waeh->name .'</option>';
	} 
		
	return $waehrungen;
}


// **********************************************
//	options für zahlart bzw. budget-select zurückgeben
// **********************************************
function ausgabeart_options( $beschreibung, $selected=false ) {
	
	$ausgabearten = get_terms( array(
		'taxonomy' => 'ausgabeart',
		'hide_empty' => false,
	) );

	$str = '';
	foreach( $ausgabearten as $ausgabeart ) {

		// zahlart oder budget?
		if ( $beschreibung != $ausgabeart->description ) { continue; }
		
		$str .= '<option value="'. $ausgabeart->term_id .'"';
		if ( in_array( $ausgabeart->term_id, $selected ) ) { $str .= ' selected'; }
		$str .= '>'. $ausgabeart->name .'</option>';
	} 
		
	return $str;
}


// **********************************************
//	options für zahlart bzw. budget-select zurückgeben
// **********************************************
function laender_options( $selected=false ) {

	$laender = array('Albanien', 'Belgien', 'Bosnien-Herzegowina', 'Bulgarien', 'Dänemark', 'Estland', 'Finnland', 'Frankreich', 'Griechenland', 'Großbritannien', 'Irland', 'Italien', 'Kosovo', 'Kroatien', 'Lettland', 'Litauen', 'Luxemburg', 'Mazedonien', 'Moldawien', 'Monaco', 'Montenegro', 'Niederlande', 'Nordirland', 'Norwegen', 'Österreich', 'Polen', 'Portugal', 'Rumänien', 'Schweden', 'Schweiz', 'Serbien', 'Slowakei', 'Slowenien', 'Spanien', 'Tschech. Republik', 'Türkei', 'Ungarn', 'Zypern' );
				
	$laender_option  = '<option value="Deutschland">Deutschland</option>';
	$laender_option .= '<option value="">--------------</option>';
				
	foreach ( $laender as $land ) { 
		$laender_option .= '<option value="'. $land .'"';
		if ( $land == $selected ) { $laender_option .= ' selected'; }
		$laender_option .= '>'. $land .'</option>'; 
	}			

	return $laender_option;
}



// **********************************************
//	adminbar nur für administartoren anzeigen
// **********************************************
if ( !current_user_can( 'manage_options' ) ) {
	// ist eigentlich auch über adminimize ausgeblendet, zur sicherheit nochmal
	show_admin_bar( false ); 
}



// **********************************************
//	fehlermeldung bei include
// **********************************************
function elsch_include( $inc ) {
	
	// sollte eine include datei nicht vorhanden sein, 
	// wird der dateiname als fehlermeldung zurückgegeben
	echo '<code>! '. substr ( end ( explode ( '/', $inc ) ), 0, -4 ) .' fehlt</code><br/>'; 
}


?>