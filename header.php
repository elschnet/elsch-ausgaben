<?php defined('ABSPATH') or die("No script kiddies please!");
 
// Theme Name:  elsch-ausgaben
// Author:      Michael Schaefer
// Author URI:  https://elsch.net
// Version:     1.0 (2018-12)
// Text Domain:	elschnet_td
// *******************************

// kleine absicherung
if ( !is_user_logged_in() ) {
	wp_die( 'Schummeln, was?' );
	exit;
}


?>

<!DOCTYPE html>
<html lang="<?php bloginfo('language') ?>">
<?php elschnet_elschsign() ?>
<head>
	<style>html { display: none; } </style>
	<meta charset="<?php bloginfo('charset') ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no, viewport-fit=cover">
	
	<?php // kein gedöhns, nur apple ios ?>	
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
	<meta name="format-detection" content="telephone=no">
	<meta name="apple-mobile-web-app-title" content="Ausgaben">
	<link rel="apple-touch-icon" sizes="180x180" href="/app_ausgaben/apple-touch-icon.png">
	<link rel="manifest" href="/app_ausgaben/manifest.json">
	<link rel="shortcut icon" href="/app_ausgaben/favicon.ico">


	<?php if ( is_search() ) { ?><meta name="robots" content="noindex, nofollow" /><?php } ?>
	<?php if ( is_front_page() ) { ?><meta name="google-site-verification" content="KZZo7D2wt8Na2Xgx073b7k0NJn-y9W-54co7xDY6Nnk" /><?php } ?>
	<?php wp_site_icon(); ?>
	<?php wp_head(); ?>	
	
	<?php 
	//elschtodo: brauchen wir das überhaupt noch mit html: display none??
	//gehört in critical-css 
	?>
	<style>
	.menu-container{margin-left:-1000px;} 
	svg{visibility:hidden;}
	footer a{font-size: 12px;}
	</style>
</head><?php 


// bei seiten den slug als css-klasse einbinden
// bei "/?fixkosten" fixkosten als klasse einbauen
$class = 'elsch-ausgaben';
if ( is_page() ) { $class .= ' page-'. $post->post_name; } 
if ( isset($_GET['fixkosten']) ) { $class .= ' fixkosten'; } ?>
<body <?php body_class($class); ?>><?php


// helper ?>
<input type="checkbox" id="offcanvas-menu" class="toggle" /><?php


// container um den kompletten inhalt ?>
<div class="container"><?php

		
	// *******************************
	// navigation ?>	
	<div class="menu-container"><?php

	
		// button menü schließen ?>
		<label for="offcanvas-menu" class="close-btn">
		<svg width="20" height="20" viewPort="0 0 20 20">
			<line x1="1" y1="20" x2="20" y2="1" stroke="orange" stroke-width="3"/>
			<line x1="1" y1="1" x2="20" y2="20" stroke="orange" stroke-width="3"/>
		</svg>
		</label><?php
		
		
		// menü
		$arguments = array(
			'container'			=> FALSE,
			'menu'				=> 'MainNavigation',
			'menu_class'		=> 'slide',
			'theme_location'	=> 'main_nav',
			'items_wrap'		=> '<nav id="%1$s" class="%2$s" role="navigation"><ul><li>%3$s</li></ul></nav>',
			'depth'				=> 1,
			'between_links'	=> '</li><li>',
			'walker'			=> new elschnet_walker_nav()
		); 
		
		wp_nav_menu( $arguments ); 
		
		
		// filterlinks
		if ( isset($GLOBALS['f_link']) ) {
			echo '<div class="slide filterlinks"><ul>';
				foreach( $GLOBALS['f_link'] as $link ) { echo '<li>'. $link .'</li>'; }
			echo '</ul></div>';
		}
		?>
			
	</div><?php
			
			
			
	// *******************************
	// inhaltscontainer ?>
	<main class="content"> <?php
	
	
		// hintergrund = button menü schließen ?>
		<label for="offcanvas-menu" class="full-screen-close"></label><?php

		// grid start ?>
		<div class="grid">
			<div class="1/1 grid__cell">

		