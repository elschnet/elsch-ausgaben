<?php defined('ABSPATH') or die("No script kiddies please!");
 
// Plugin Name: CPT Ausgaben
// Description: liefert den Custom Post Type "Ausgaben" 
// Author:      Michael Schaefer
// Author URI:  https://elsch.net
// Version:     1.5 (2018-12)
// Text Domain:	elschnet_td
// *******************************


 
// **********************************************
//  INHALT
//
//  custom post type: ausgabe
//  ausgabe in "Auf einen Blick" anzeigen
//  custom taxonomy: ausgabeart (like tags)
//  custom taxonomy: kategorie (like kategorie)
//  custom taxonomy: waehrung (like kategorie)
//  custom post type: budget
//  auto title bei budget
//  archiv /budget/ sortiert nach budget monat 
//  archiv + suchergebnis sortiert nach kategorie
//  homelink in adminbar ändern
//
// **********************************************


// **********************************************
//  custom post type: ausgabe
// **********************************************
function elschnet_ausgaben_posttype() {

	$labels = array(
		'name'                  => _x( 'Ausgaben', 'Post Type General Name', 'elschnet_td' ),
		'singular_name'         => _x( 'Ausgabe', 'Post Type Singular Name', 'elschnet_td' ),
		'menu_name'             => __( 'Ausgaben', 'elschnet_td' ),
		'name_admin_bar'        => __( 'Ausgaben', 'elschnet_td' ),
		'archives'              => __( 'Archiv: Ausgaben', 'elschnet_td' ),
		'all_items'             => __( 'Alle Ausgaben', 'elschnet_td' ),
		'add_new_item'          => __( 'Neue Ausgabe hinzufügen', 'elschnet_td' ),
		'add_new'               => __( 'Erstellen', 'elschnet_td' ),
		'edit_item'             => __( 'Ausgabe bearbeiten', 'elschnet_td' ),
		'search_items'          => __( 'Ausgabe suchen', 'elschnet_td' ),
		'not_found'             => __( 'keine Ausgabe gefunden', 'elschnet_td' ),
		'not_found_in_trash'    => __( 'keine Ausgabe im Papierkorb', 'elschnet_td' ),
		'back_to_items'			=> __( '← Zurück zu Ausgaben', 'elschnet_td' ),
	);
	$args = array(
		'label'                 => __( 'Ausgaben', 'elschnet_td' ),
		'description'           => __( 'Alle Ausgaben', 'elschnet_td' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'author' ),
		'taxonomies' 			=> array( 'kategorie', 'ausgabeart', 'waehrung' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 4,
		'menu_icon'             => 'dashicons-book-alt',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => false,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'post',
		'show_in_rest'          => false,
	);
	register_post_type( 'ausgabe', $args );

}
add_action( 'init', 'elschnet_ausgaben_posttype', 0 );



// **********************************************
//  ausgabe in "Auf einen Blick" anzeigen
// **********************************************
function elschnet_cpt_to_at_a_glance() {
	// Add custom taxonomies and custom post types counts to dashboard
	// https://wordpress.stackexchange.com/a/241665
	$post_types = get_post_types( array( '_builtin' => false ), 'objects' );
	$anzeigen = array('ausgabe'); //welche sollen angezeigt werden
	foreach ( $post_types as $post_type ) {
		if( !in_array( $post_type->name, $anzeigen ) ) { continue; }
		$num_posts = wp_count_posts( $post_type->name );
		$num = number_format_i18n( $num_posts->publish );
		$text = _n( $post_type->labels->singular_name, $post_type->labels->name, $num_posts->publish );
		
		if ( current_user_can( 'edit_posts' ) ) {
			$num = '<li class="post-count"><a href="edit.php?post_type=' . $post_type->name . '">' . $num . ' ' . $text . '</a></li>';
		}
		
		echo $num;
	}
}
add_action( 'dashboard_glance_items', 'elschnet_cpt_to_at_a_glance' );



// **********************************************
//  custom taxonomy: ausgabeart (like tags)
// **********************************************
function elschnet_ausgabeart_taxonomy() {
	// https://generatewp.com/snippet/bqdon3j/

	$labels = array(
		'name'                       => _x( 'Ausgabeart', 'Taxonomy General Name', 'elschnet_td' ),
		'singular_name'              => _x( 'Ausgabeart', 'Taxonomy Singular Name', 'elschnet_td' ),
		'menu_name'                  => __( 'Ausgabeart', 'elschnet_td' ),
		'all_items'                  => __( 'Alle Ausgabearten', 'elschnet_td' ),
		'new_item_name'              => __( 'Ausgabeart', 'elschnet_td' ),
		'add_new_item'               => __( 'Neue Ausgabeart speichern', 'elschnet_td' ),
		'edit_item'                  => __( 'Ausgabeart bearbeiten', 'elschnet_td' ),
		'update_item'                => __( 'Ausgabeart aktualisieren', 'elschnet_td' ),
		'search_items'               => __( 'Ausgabeart suchen', 'elschnet_td' ),
		'not_found'                  => __( 'keine Ausgabeart gefunden', 'elschnet_td' ),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => false,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => false,
		'show_tagcloud'              => true,
		'show_in_rest'               => false,
	);
	register_taxonomy( 'ausgabeart', array( 'ausgabe' ), $args );

}
add_action( 'init', 'elschnet_ausgabeart_taxonomy', 0 );



// **********************************************
//  custom taxonomy: kategorie (like kategorie)
// **********************************************
function elschnet_kategorie_taxonomy() {
	https://generatewp.com/snippet/KDJ3Rbn/

	$labels = array(
		'name'                       => _x( 'Kategorien', 'Taxonomy General Name', 'elschnet_td' ),
		'singular_name'              => _x( 'Kategorie', 'Taxonomy Singular Name', 'elschnet_td' ),
		'menu_name'                  => __( 'Kategorien', 'elschnet_td' ),
		'all_items'                  => __( 'Alle Kategorien', 'elschnet_td' ),
		'parent_item'                => __( 'Übergeordnete Kategorie', 'elschnet_td' ),
		'new_item_name'              => __( 'Kategorie', 'elschnet_td' ),
		'add_new_item'               => __( 'Neue Kategorie speichern', 'elschnet_td' ),
		'edit_item'                  => __( 'Kategorie bearbeiten', 'elschnet_td' ),
		'update_item'                => __( 'Kategorie aktualisieren', 'elschnet_td' ),
		'search_items'               => __( 'Kategorie suchen', 'elschnet_td' ),
		'not_found'                  => __( 'keine Kategorie gefunden', 'elschnet_td' ),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => true,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => false,
		'show_tagcloud'              => true,
		'show_in_rest'               => false,
	);
	register_taxonomy( 'kategorie', array( 'ausgabe' ), $args );

}
add_action( 'init', 'elschnet_kategorie_taxonomy', 0 );



// **********************************************
//  custom taxonomy: waehrung (like kategorie)
// **********************************************
function elschnet_waehrung_taxonomy() {
	https://generatewp.com/snippet/KDJ3Rbn/

	$labels = array(
		'name'                       => _x( 'Währungen', 'Taxonomy General Name', 'elschnet_td' ),
		'singular_name'              => _x( 'Währung', 'Taxonomy Singular Name', 'elschnet_td' ),
		'menu_name'                  => __( 'Währungen', 'elschnet_td' ),
		'all_items'                  => __( 'Alle Währungen', 'elschnet_td' ),
		'parent_item'                => __( 'Übergeordnete Währung', 'elschnet_td' ),
		'new_item_name'              => __( 'Währung', 'elschnet_td' ),
		'add_new_item'               => __( 'Neue Währung speichern', 'elschnet_td' ),
		'edit_item'                  => __( 'Währung bearbeiten', 'elschnet_td' ),
		'update_item'                => __( 'Währung aktualisieren', 'elschnet_td' ),
		'search_items'               => __( 'Währung suchen', 'elschnet_td' ),
		'not_found'                  => __( 'keine Währung gefunden', 'elschnet_td' ),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => true,
		'public'                     => false,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => false,
		'show_tagcloud'              => false,
		'show_in_rest'               => false,
	);
	register_taxonomy( 'waehrung', array( 'ausgabe' ), $args );

}
add_action( 'init', 'elschnet_waehrung_taxonomy', 0 );



// **********************************************
//  custom post type: budget
// **********************************************
function elschnet_budget_posttype() {
	
	$labels = array(
		'name'                  => _x( 'Budgets', 'Post Type General Name', 'elschnet_td' ),
		'singular_name'         => _x( 'Budget', 'Post Type Singular Name', 'elschnet_td' ),
		'menu_name'             => __( 'Budgets', 'elschnet_td' ),
		'name_admin_bar'        => __( 'Budgets', 'elschnet_td' ),
		'archives'              => __( 'Archiv: Budgets', 'elschnet_td' ),
		'all_items'             => __( 'Alle Budgets', 'elschnet_td' ),
		'add_new_item'          => __( 'Neues Budget hinzufügen', 'elschnet_td' ),
		'add_new'               => __( 'Erstellen', 'elschnet_td' ),
		'edit_item'             => __( 'Budget bearbeiten', 'elschnet_td' ),
		'search_items'          => __( 'Budget suchen', 'elschnet_td' ),
		'not_found'             => __( 'kein Budget gefunden', 'elschnet_td' ),
		'not_found_in_trash'    => __( 'kein Budget im Papierkorb', 'elschnet_td' ),
		'back_to_items'			=> __( '← Zurück zu Budgets', 'elschnet_td' ),
	);
	$args = array(
		'label'                 => __( 'Budgets', 'elschnet_td' ),
		'description'           => __( 'Alle Budgets', 'elschnet_td' ),
		'labels'                => $labels,
		'supports'              => array( 'editor', 'author' ),
		'taxonomies' 			=> array(),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 4,
		'menu_icon'             => 'dashicons-chart-bar',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => false,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'post',
		'show_in_rest'          => false,
	);
	register_post_type( 'budget', $args );

}
add_action( 'init', 'elschnet_budget_posttype', 0 );



// **********************************************
//  auto title bei budget
// **********************************************
function elschnet_budgetpost_title_updater( $post_id ) {
	// @https://support.advancedcustomfields.com/forums/topic/createupdate-post-title-from-acf-fields/

	if ( get_post_type() == 'budget' ) {
		
		$my_post = array();
		$my_post['ID'] = $post_id;

		$betrag	= get_field('b_betrag', $post_id);
		$person	= get_field('b_person', $post_id);
		$monat	= get_field('b_monat', $post_id);

		$my_post['post_title'] = "$monat | $person | $betrag";
		$my_post['post_name'] = urlencode ( "$monat-$person-$betrag" );
		
		$prev_value = get_post_meta( $post_id, 'b_monat', true );
		update_post_meta( $post_id, 'b_monat', $monat, $prev_value );

		// Update the post into the database
		wp_update_post( $my_post );
	}

}
// run after ACF saves the $_POST['fields'] data
add_action('acf/save_post', 'elschnet_budgetpost_title_updater', 20);



// **********************************************
//  archiv /budget/ sortiert nach budget monat
// **********************************************
function elschnet_pregetpost_budget( $query ) {
	// @ https://wordpress.stackexchange.com/a/237746
	// @ https://wordpress.stackexchange.com/q/300804
	// @ https://wordpress.stackexchange.com/a/183612

    if ( !is_admin() && is_archive() && $query->query_vars['post_type'] == 'budget' &&  $query->is_main_query() ) {

        $query->set( 'posts_per_page','-1' );
		$query->set( 'orderby', array('meta_value' => 'DESC', 'title' => 'ASC'));
        $query->set( 'meta_key','b_monat' );

    }
}
add_action( 'pre_get_posts', 'elschnet_pregetpost_budget' );


// **********************************************
//  archiv + suchergebnis: alle anzeigen
// **********************************************
function elschnet_posttype_archive( $query ) {

	if( $query->is_main_query() && !is_admin() && ( is_archive() || is_search() ) ) {
		$query->set( 'posts_per_page', '-1' );
		//$query->set( 'orderby', 'title' );
		//$query->set( 'order', 'ASC' );
	}
}
add_action( 'pre_get_posts', 'elschnet_posttype_archive' );



// **********************************************
//  homelink in adminbar ändern
// **********************************************
function elschnet_edit_homelink_adminbar( $wp_admin_bar ) {
	// @ https://wordpress.stackexchange.com/questions/298965/change-admin-bar-visit-site-url

	if ( !current_user_can('manage_options') ) {
		$node = $wp_admin_bar->get_node('view-site');
		$node->href = '/monat-summen/';
		$wp_admin_bar->add_node($node);
	}
}
add_action( 'admin_bar_menu', 'elschnet_edit_homelink_adminbar', 80 );
