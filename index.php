<?php defined('ABSPATH') or die("No script kiddies please!");
 
// Theme Name:  elsch-ausgaben
// Author:      Michael Schaefer
// Author URI:  https://elsch.net
// Version:     1.0 (2018-12)
// Text Domain:	elschnet_td
// *******************************
// datei ist für neueintragung von 
// ausgaben zuständig

if ( !session_id() ) { session_start(); }


// **********************************************
//  Quellen dieses Themes
// ********************************************** 
function quellen() {
	# $var = print_r($what, true);

	# css framework
	# https://github.com/colourgarden/avalanche 
	# http://colourgarden.net/avalanche/
	
	# pure css off-canvas
	# https://github.com/acasaprogramming/pure-css-offcanvas-menu 

	# formulare absichern
	# https://www.tipsandtricks-hq.com/introduction-to-wordpress-nonces-5357
	# https://developer.wordpress.org/reference/functions/wp_nonce_field/#comment-801
	# https://codex.wordpress.org/WordPress_Nonces
	
	# css von select/option
	# https://www.clickstorm.de/blog/dropdown-css3/
	
	# modal
	# http://jquerymodal.com/
	# https://github.com/kylefox/jquery-modal
	
	# manifest
	# https://app-manifest.firebaseapp.com/ (manifest-generator)
	# https://medium.com/appscope/changing-the-ios-status-bar-of-your-progressive-web-app-9fc8fbe8e6ab
	# https://www.creativebloq.com/html5/12-html5-tricks-mobile-81412803

	# svg torten-diagramm
	# https://jbkflex.wordpress.com/2011/07/28/creating-a-svg-pie-chart-html5/
	# https://www.smashingmagazine.com/2015/07/designing-simple-pie-charts-with-css/
	
	# iphone notch
	# https://css-tricks.com/the-notch-and-css/
	
	# css radio-buttons
	# https://codepen.io/samkeddy/pen/PbROLK/
}



// **********************************************
//  aktueller benutzer
// ********************************************** 
$current_user = wp_get_current_user();
$user = strtolower( $current_user->user_firstname ); 


if ( have_posts() ) { 
	
	if ( is_singular( 'budget' ) ) {
		header('Location:'. site_url('/budget/') );
		exit;		
	}

	// **********************************************
	//  seitenkopf
	// **********************************************
	get_header();
	$headline = '<h1>Anleitungen/Vereinbarungen</h1>';
	$GLOBALS['aktive_nav'] = 0;
	$inc = get_template_directory() .'/parts/header.php';
	if ( !@include( $inc ) ) { elsch_include( $inc ); } 


	// **********************************************
	//  inhalt
	// ********************************************** 
	?>
	<div class="content-body"><?php

		while ( have_posts() ) { the_post(); 
			?>
			
			<article>
				<?php				
				if ( is_single() ) {					
					echo '<h2>'. get_the_title() .'</h2>';
					the_content();
					echo '<br/><br/><small>'. get_the_time('d.m.Y') .'</small>';
					echo '<hr><p><a href="javascript:window.history.back()">zurück</a></p>';
				} 
				
				else { 					
					echo '<h2><a href="'. get_the_permalink() .'">'. get_the_title() .'</a></h2>';
					the_excerpt(); 
				}
				
				?>
			</article>
			<?php
			
		} // while have_posts() 
		
		if ( !is_single() ) {
			echo '<br/><br/><hr><p><a href="/wp-admin/post-new.php">neuer Eintrag</a></p>';
		}
		?>
			
	</div>
	<?php

		
	// **********************************************
	//  footer
	// **********************************************
	get_footer();
			
} // if have_posts() 


else { // 404
	// da es ein template 404.php gibt, sollte das nie ausgegeben werden
	$_SESSION['fehler'] = '404';
	header('Location:'. site_url('/fehler/') );
	exit;
}

?>